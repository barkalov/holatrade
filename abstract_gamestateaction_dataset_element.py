import abc
import numpy as np
#### helper-class to put raw stat data into and export as unnormalized layers dims (types, rounds, channels) as input and (channels,) as output
class AbstractGamestateactionDatasetElement:
    def __init__(self):
        self.his_steps_as_i_see = None
        self.my_steps_as_i_see = None
        self.my_values = None
        self.his_values = None
        self.counts = None
        self.semi_ids = None
        self.is_accepteds = None
        self.is_a = None
        self.score_pair = None

    def finalize_via_extend(self, max_rounds = 6):
        extend_from = max(len(self.my_steps_as_i_see), len(self.his_steps_as_i_see))
        assert len(self.my_steps_as_i_see) == len(self.his_steps_as_i_see) # tmp check TODO debug and ensure if it happends (or remove max() if all ok after some time)
        for s in range(extend_from, max_rounds):
            my_empty_step_as_i_see = self.counts * 1
            his_empty_step_as_i_see = np.zeros(self.counts.shape)
            semi_id = 0
            is_accepteds = 0
            self.his_steps_as_i_see.insert(0, his_empty_step_as_i_see)
            self.my_steps_as_i_see.insert(0, my_empty_step_as_i_see)
            self.semi_ids.insert(0, semi_id)
            self.is_accepteds.insert(0, is_accepteds)
