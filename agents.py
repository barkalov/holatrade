import numpy as np
import abc
import copy
#from gamestate_dataset_element import GamestateDatasetElement
from gameaction_dataset_element import GameactionDatasetElement
#from gamereplay_dataset_element import GamereplayDatasetElement
from test import test_guesser, test_oraculer

class AbstractAgent:
  agent_name = 'AbstractAgent'

  def __init__(self, me, counts, values, max_rounds, log, cheat_opponent_values):
    self.me = me
    self.counts = counts
    self.values = values
    self.max_rounds = max_rounds
    self.last_round = max_rounds
    self.log = log
    self.cheat_opponent_values = cheat_opponent_values
    self.total_values_sum = 10
    self.current_round = 0
    self.name = type(self).agent_name

  @abc.abstractmethod
  def internal_offer_implementation(self, o):
    pass

  def internal_offer_implementation_hook(self, o):
    # virtual method to be overriden in ManagedAgent
    return self.internal_offer_implementation(o)

  def internal_offer(self, o):
    self.current_round += 1
    offer_result = self.internal_offer_implementation_hook(o) # payload call is here
    assert np.all(offer_result >= 0)
    assert np.all(offer_result <= self.counts)
    if not (o.sum() != 0 or offer_result.sum() != 0):
      self.log('zero-offer catched')
    #assert (o.sum() != 0 or offer_result.sum() != 0)

    return offer_result * 1

  def offer(self, o):
    if o is None:
      o = np.zeros(self.values.shape, dtype=int)
    offer_result = self.internal_offer(o) # payload call is here
    if np.all(offer_result == o):
      return None
    else:
      return offer_result.astype(int) * 1

  @abc.abstractmethod
  def end_of_game_implementation(self, score_pair):
    pass

  def end_of_game_implementation_hook(self, score_pair):
    # virtual method to be overriden somewhere
    return self.end_of_game_implementation(score_pair)

  def end_of_game(self, score_pair):
    return self.end_of_game_implementation_hook(score_pair)

  @abc.abstractmethod
  def acceptance_implementation(self):
    pass

  def acceptance_implementation_hook(self):
    # virtual method to be overriden somewhere
    return self.acceptance_implementation()

  def acceptance(self):
    return self.acceptance_implementation_hook()

  @abc.abstractmethod
  def timeout_implementation(self):
    pass

  def timeout_implementation_hook(self):
    # virtual method to be overriden somewhere
    return self.timeout_implementation()

  def timeout(self):
    return self.timeout_implementation_hook()



class AbstractSigmaAgent(AbstractAgent):
  agent_name = 'AbstractSigmaAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    #self.sigma = 1 / 20
    self.sigma = 0


  def internal_offer_implementation_hook(self, o):
    if np.random.uniform() < (self.sigma / 2):
      self.log('offer randomize: hard sigma')
      offer_result = np.floor(np.random.uniform(0, self.counts + 1))
    else:
      offer_result = super(AbstractSigmaAgent, self).internal_offer_implementation_hook(o) # payload call is here
      if np.random.uniform() < self.sigma:
        self.log('offer randomize: soft sigma for type 0')
        offer_result[0] = np.floor(np.random.uniform(0, self.counts[0] + 1))
      if np.random.uniform() < self.sigma:
        self.log('offer randomize: soft sigma for type 1')
        offer_result[1] = np.floor(np.random.uniform(0, self.counts[1] + 1))
      if np.random.uniform() < self.sigma:
        self.log('offer randomize: soft sigma for type 2')
        offer_result[2] = np.floor(np.random.uniform(0, self.counts[2] + 1))

    return offer_result * 1


class AbstractStateactionedAgent(AbstractSigmaAgent):
  agent_name = 'AbstractStateactionedAgent'

  def __init__(self, gamereplay_dataset, gamereplay_dataset_id, *args, **kwargs):
    super().__init__(*args, **kwargs)
    is_a = self.me == 0
    #self.gamestate_dataset_element = GamestateDatasetElement()
    #self.gamestate_dataset_element.counts = self.counts
    #self.gamestate_dataset_element.is_a = is_a
    #self.gamestate_dataset_element.my_values = self.values
    #self.gamestate_dataset_element.his_values = self.cheat_opponent_values
    #self.gamestate_dataset_element.my_steps_as_i_see = []
    #self.gamestate_dataset_element.his_steps_as_i_see = []
    #self.gamestate_dataset_element.is_accepteds = []
    #self.gamestate_dataset_element.semi_ids = []

    self.gameaction_dataset_element = GameactionDatasetElement()
    self.gameaction_dataset_element.counts = self.counts
    self.gameaction_dataset_element.is_a = is_a
    self.gameaction_dataset_element.my_values = self.values
    self.gameaction_dataset_element.his_values = self.cheat_opponent_values
    self.gameaction_dataset_element.my_steps_as_i_see = []
    self.gameaction_dataset_element.his_steps_as_i_see = []
    self.gameaction_dataset_element.is_accepteds = []
    self.gameaction_dataset_element.semi_ids = []

    self.my_previous_step_as_i_see = self.counts * 1
    #self.gamereplay_dataset = gamereplay_dataset
    #self.gamereplay_dataset_id = gamereplay_dataset_id
    #self.gamereplay_dataset_element = GamereplayDatasetElement(self.gamereplay_dataset, self.gamereplay_dataset_id)
    #self.gamereplay_dataset_element.start(self.values, self.cheat_opponent_values, self.counts, is_a)

  def internal_offer_implementation_hook(self, o):
    is_a = self.me == 0
    my_step_as_i_see = self.my_previous_step_as_i_see
    his_step_as_i_see = o # o will be zeroes on first round while is_a
    is_state_accepted = False # since it's a new offer comed from opponent
    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based

    #self.gamestate_dataset_element.my_steps_as_i_see.append(my_step_as_i_see)
    #self.gamestate_dataset_element.his_steps_as_i_see.append(his_step_as_i_see)
    #self.gamestate_dataset_element.is_accepteds.append(is_state_accepted * 1)
    #self.gamestate_dataset_element.semi_ids.append(semi_id)

    if semi_id > 0: #skip initial [0, 0, 0] offer recording (tail will be filled with [0, 0, 0] later as mask-values)
      #self.gamereplay_dataset_element.add_incoming_offer(o, is_a)
      pass

    offer_result = super(AbstractStateactionedAgent, self).internal_offer_implementation_hook(o) # payload call is here

    is_action_accepted = np.all(o == offer_result)
    self.gameaction_dataset_element.his_steps_as_i_see.append(o)
    self.gameaction_dataset_element.my_steps_as_i_see.append(offer_result)
    self.gameaction_dataset_element.is_accepteds.append(is_action_accepted * 1)
    self.gameaction_dataset_element.semi_ids.append(semi_id + 1)

    #self.gamereplay_dataset_element.add_outgoing_offer(offer_result, is_a)

    self.my_previous_step_as_i_see = offer_result
    return offer_result * 1

  def acceptance_implementation_hook(self):
    is_a = self.me == 0
    my_step_as_i_see = self.my_previous_step_as_i_see
    his_step_as_i_see = self.my_previous_step_as_i_see
    is_state_accepted = True
    semi_id = self.current_round * 2 + self.me # no -2 since it's a terminal round, and offer() does not fired (and current_round does not iterated++)

    #self.gamestate_dataset_element.my_steps_as_i_see.append(my_step_as_i_see)
    #self.gamestate_dataset_element.his_steps_as_i_see.append(his_step_as_i_see)
    #self.gamestate_dataset_element.is_accepteds.append(is_state_accepted * 1)
    #self.gamestate_dataset_element.semi_ids.append(semi_id)

    #self.gamereplay_dataset_element.add_incoming_offer(his_step_as_i_see, is_a) #terminal

    return super(AbstractStateactionedAgent, self).acceptance_implementation_hook() # payload call is here

  def timeout_implementation_hook(self):
    is_a = self.me == 0
    my_step_as_i_see = self.my_previous_step_as_i_see
    his_step_as_i_see = np.zeros(self.counts.shape)
    is_state_accepted = False
    semi_id = self.current_round * 2 + self.me # no -2 since it's a terminal round, and offer() does not fired (and current_round does not iterated++)

    #self.gamestate_dataset_element.my_steps_as_i_see.append(my_step_as_i_see)
    #self.gamestate_dataset_element.his_steps_as_i_see.append(his_step_as_i_see)
    #self.gamestate_dataset_element.is_accepteds.append(is_state_accepted * 1)
    #self.gamestate_dataset_element.semi_ids.append(semi_id)

    #self.gamereplay_dataset_element.add_incoming_offer(his_step_as_i_see, is_a) #terminal

    return super(AbstractStateactionedAgent, self).timeout_implementation_hook() # payload call is here

  def end_of_game_implementation_hook(self, score_pair):
    is_a = self.me == 0
    #self.gamestate_dataset_element.score_pair = score_pair
    self.gameaction_dataset_element.score_pair = score_pair
    #self.gamestate_dataset_element.finalize_via_extend()
    self.gameaction_dataset_element.finalize_via_extend()

    #self.gamereplay_dataset_element.end(score_pair, is_a) #terminal

    #region test dbg TODO:remove
    # test_guesser_gamereplay_state = self.gamereplay_dataset_element.export_guesser_inputs(is_state = True, is_a = is_a, rounds_played_cap = 10)
    # test_guesser_gamestate, _ = self.gamestate_dataset_element.export_score_guesser_gamestate()
    # test_guesser_gamereplay_action = self.gamereplay_dataset_element.export_guesser_inputs(is_state = False, is_a = is_a, rounds_played_cap = 10)
    # test_guesser_gameaction, _ = self.gameaction_dataset_element.export_score_guesser_gameaction()

    # test_guesser(test_guesser_gamestate, test_guesser_gamereplay_state)
    # test_guesser(test_guesser_gameaction, test_guesser_gamereplay_action)


    # #test_oraculer_gamereplay_state = self.gamereplay_dataset_element.export_oraculer_inputs(is_state = True, is_a = is_a, rounds_played_cap = 10)
    # #test_oraculer_gamestate, _ = self.gamestate_dataset_element.export_metapenalty_oraculer_gamestate()
    # #test_oraculer_gamereplay_action = self.gamereplay_dataset_element.export_oraculer_inputs(is_state = False, is_a = is_a, rounds_played_cap = 10)
    # #test_oraculer_gameaction, _ = self.gameaction_dataset_element.export_metapenalty_oraculer_gameaction()

    # #test_oraculer(test_oraculer_gamestate, test_oraculer_gamereplay_state)
    # #test_oraculer(test_oraculer_gameaction, test_oraculer_gamereplay_action)
    #endregion test dbg TODO:remove
    return super(AbstractStateactionedAgent, self).end_of_game_implementation_hook(score_pair) # payload call is here

# # temporary commented TODO: uncomment
# from gameaction_score_guesser_bba import GameactionScoreGuesser as GameactionScoreGuesserBBA
# from gameaction_score_guesser_bbb import GameactionScoreGuesser as GameactionScoreGuesserBBB
# from gameaction_score_guesser_caa import GameactionScoreGuesser as GameactionScoreGuesserCAA
# from gameaction_score_guesser_cab import GameactionScoreGuesser as GameactionScoreGuesserCAB
# from gameaction_score_guesser_dab import GameactionScoreGuesser as GameactionScoreGuesserDAB
# from gameaction_metapenalty_oraculer_aaa import GameactionMetapenaltyOraculer as GameactionMetapenaltyOraculerAAA
# from gameaction_metapenalty_guesser_aab import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserAAB
# from gameaction_metapenalty_guesser_zaa import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserZAA
# #from gan_for_gameaction_metapenalty_oraculer_aaa import GanForGameactionMetapenaltyOraculer as GanForGameactionMetapenaltyOraculerAAA

# global_gameaction_score_guesser_bba = GameactionScoreGuesserBBA(name = 'gameaction_score_guesser_bba', preload = True)
# global_gameaction_score_guesser_bbb = GameactionScoreGuesserBBB(name = 'gameaction_score_guesser_bbb', preload = True)
# global_gameaction_score_guesser_caa = GameactionScoreGuesserCAA(name = 'gameaction_score_guesser_caa', preload = True)
# global_gameaction_score_guesser_cab = GameactionScoreGuesserCAB(name = 'gameaction_score_guesser_cab', preload = True)
# global_gameaction_score_guesser_dab = GameactionScoreGuesserDAB(name = 'gameaction_score_guesser_dab', preload = True)
# global_gameaction_metapenalty_oraculer_aaa = GameactionMetapenaltyOraculerAAA(name = 'gameaction_metapenalty_oraculer_aaa_fat', preload = True)
# global_gameaction_metapenalty_guesser_aab = GameactionMetapenaltyGuesserAAB(name = 'gameaction_metapenalty_guesser_aab_try', preload = True)
# global_gameaction_metapenalty_guesser_zaa = GameactionMetapenaltyGuesserZAA(name = 'gameaction_metapenalty_guesser_zaa_16', preload = True)
# #global_gan_for_gameaction_metapenalty_oraculer_aaa = GanForGameactionMetapenaltyOraculerAAA(name = 'gan_aaa', metapenalty_oraculer_name = 'gameaction_metapenalty_oraculer_aaa_fat', preload = True)

# dirty hack to not to recreate on each game TODO put in factory, that creates agent classes



class ChappieSGBBAAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieSGBBAAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_my_score = 0

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based #TODO correct me too

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_score_pairs_try = global_gameaction_score_guesser_bba.predict(gameaction_dataset_element_clone)
      denormalized_predicred_score_pairs_try = GameactionDatasetElement.denormalize_score_guesser_gameaction_outputs(predicred_score_pairs_try)
      denormalized_predicred_score_pair_try = denormalized_predicred_score_pairs_try[0]
      denormalized_predicred_my_score_try = denormalized_predicred_score_pair_try[0]
      if denormalized_predicred_my_score_try > best_offer_my_score:
        best_offer_my_score = denormalized_predicred_my_score_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 4 / 5:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result


class ChappieSGBBBAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieSGBBBAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_my_score = 0

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_score_pairs_try = global_gameaction_score_guesser_bbb.predict(gameaction_dataset_element_clone)
      denormalized_predicred_score_pairs_try = GameactionDatasetElement.denormalize_score_guesser_gameaction_outputs(predicred_score_pairs_try)
      denormalized_predicred_score_pair_try = denormalized_predicred_score_pairs_try[0]
      denormalized_predicred_my_score_try = denormalized_predicred_score_pair_try[0]
      if denormalized_predicred_my_score_try > best_offer_my_score:
        best_offer_my_score = denormalized_predicred_my_score_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 4 / 5:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result


class ChappieSGCAAAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieSGCAAAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_my_score = 0

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_score_pairs_try = global_gameaction_score_guesser_caa.predict(gameaction_dataset_element_clone)
      denormalized_predicred_score_pairs_try = GameactionDatasetElement.denormalize_score_guesser_gameaction_outputs(predicred_score_pairs_try)
      denormalized_predicred_score_pair_try = denormalized_predicred_score_pairs_try[0]
      denormalized_predicred_my_score_try = denormalized_predicred_score_pair_try[0]
      if denormalized_predicred_my_score_try > best_offer_my_score:
        best_offer_my_score = denormalized_predicred_my_score_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 4 / 5:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result


class ChappieSGCABAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieSGCABAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_my_score = 0

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_score_pairs_try = global_gameaction_score_guesser_cab.predict(gameaction_dataset_element_clone)
      denormalized_predicred_score_pairs_try = GameactionDatasetElement.denormalize_score_guesser_gameaction_outputs(predicred_score_pairs_try)
      denormalized_predicred_score_pair_try = denormalized_predicred_score_pairs_try[0]
      denormalized_predicred_my_score_try = denormalized_predicred_score_pair_try[0]
      if denormalized_predicred_my_score_try > best_offer_my_score:
        best_offer_my_score = denormalized_predicred_my_score_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 4 / 5:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result


class ChappieSGDABAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieSGDABAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_my_score = 0

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_score_pairs_try = global_gameaction_score_guesser_dab.predict(gameaction_dataset_element_clone)
      denormalized_predicred_score_pairs_try = GameactionDatasetElement.denormalize_score_guesser_gameaction_outputs(predicred_score_pairs_try)
      denormalized_predicred_score_pair_try = denormalized_predicred_score_pairs_try[0]
      denormalized_predicred_my_score_try = denormalized_predicred_score_pair_try[0]
      if denormalized_predicred_my_score_try > best_offer_my_score:
        best_offer_my_score = denormalized_predicred_my_score_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 4 / 5:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result


class ChappieMOAAAAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieMOAAAAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_metapenalty_univalue = 100

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_metapenalty_univalues_try = global_gameaction_metapenalty_oraculer_aaa.predict(gameaction_dataset_element_clone)
      denormalized_predicred_metapenalty_univalues_try = GameactionDatasetElement.denormalize_metapenalty_oraculer_gameaction_outputs(predicred_metapenalty_univalues_try)
      denormalized_predicred_metapenalty_univalue_try = denormalized_predicred_metapenalty_univalues_try[0]
      denormalized_predicred_metapenalty_univalue_try = denormalized_predicred_metapenalty_univalue_try[0]
      if denormalized_predicred_metapenalty_univalue_try < best_offer_metapenalty_univalue:
        best_offer_metapenalty_univalue = denormalized_predicred_metapenalty_univalue_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 9 / 10:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result

class ChappieMGAABAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieMGAABAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_metapenalty_univalue = 100

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_metapenalty_univalues_try = global_gameaction_metapenalty_guesser_aab.predict(gameaction_dataset_element_clone)
      denormalized_predicred_metapenalty_univalues_try = GameactionDatasetElement.denormalize_metapenalty_oraculer_gameaction_outputs(predicred_metapenalty_univalues_try)
      denormalized_predicred_metapenalty_univalue_try = denormalized_predicred_metapenalty_univalues_try[0]
      denormalized_predicred_metapenalty_univalue_try = denormalized_predicred_metapenalty_univalue_try[0]
      if denormalized_predicred_metapenalty_univalue_try < best_offer_metapenalty_univalue:
        best_offer_metapenalty_univalue = denormalized_predicred_metapenalty_univalue_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 9 / 10:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result

class ChappieMGZAAAgent(AbstractStateactionedAgent):
  agent_name = 'ChappieMGZAAAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_metapenalty_univalue = 100

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 50):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:25]
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)

      predicred_metapenalty_univalues_try = global_gameaction_metapenalty_guesser_zaa.predict(gameaction_dataset_element_clone)
      denormalized_predicred_metapenalty_univalues_try = GameactionDatasetElement.denormalize_metapenalty_oraculer_gameaction_outputs(predicred_metapenalty_univalues_try)
      denormalized_predicred_metapenalty_univalue_try = denormalized_predicred_metapenalty_univalues_try[0]
      denormalized_predicred_metapenalty_univalue_try = denormalized_predicred_metapenalty_univalue_try[0]
      if denormalized_predicred_metapenalty_univalue_try < best_offer_metapenalty_univalue:
        best_offer_metapenalty_univalue = denormalized_predicred_metapenalty_univalue_try
        best_offer_result = offer_result_try


    return best_offer_result
  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 9 / 10:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      return best_offer_result

class SimpleAgent(AbstractStateactionedAgent):
  agent_name = 'SimpleAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum / 2:
      return o * 1
    else:
      return self.counts * np.sign(self.values)


class LoosieAgent(AbstractStateactionedAgent):
  agent_name = 'LoosieAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) > 0:
      return o * 1
    else:
      offer_result = np.round(self.counts * np.sign(self.values) * 0.5)
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1


class RobberAgent(AbstractStateactionedAgent):
  agent_name = 'RobberAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def internal_offer_implementation(self, o):
    demand_ratio = 1 - self.current_round / self.max_rounds
    demand = self.counts * demand_ratio
    if np.all(o >= demand * 0.25) and np.sum(o * self.values) >= self.total_values_sum * 0.75:
      return o * 1
    else:
      return self.counts * 1


class RandieAgent(AbstractStateactionedAgent):
  agent_name = 'RandieAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) > (np.random.uniform() * 0.8 + 0.2) * self.total_values_sum:
      return o * 1
    else:
      offer_result = np.round(self.counts * np.sign(self.values)
                  * (np.random.uniform(size=self.values.shape) * 0.8 + 0.2))
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1


class GreedieAgent(AbstractStateactionedAgent):
  agent_name = 'GreedieAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.best_offer = self.values / self.values.max() / 3

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)
    best_offer_sum = np.sum(self.best_offer * self.values)
    if offer_sum > best_offer_sum:
      self.best_offer = o

    mix_ratio = self.current_round / self.max_rounds
    #mix_ratio = mix_ratio ** 2
    if offer_sum >= max(self.total_values_sum / 5, self.total_values_sum * (1 - mix_ratio) + best_offer_sum * mix_ratio):
      return o * 1
    else:
      greedie_offer = self.counts * np.sign(self.values)
      greedie_offer = greedie_offer * \
        (1 - mix_ratio) + self.best_offer * mix_ratio
      greedie_offer = np.min([greedie_offer, self.counts], axis=0)
      offer_result = np.round(greedie_offer)
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1


class Greedie2Agent(AbstractStateactionedAgent):
  agent_name = 'Greedie2Agent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.best_offer = self.values / self.values.max() / 3

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)
    best_offer_sum = np.sum(self.best_offer * self.values)
    if offer_sum > best_offer_sum:
      self.best_offer = o

    mix_ratio = self.current_round / self.max_rounds
    mix_ratio_clamp_ratio = min(
      1, 2 * best_offer_sum / self.total_values_sum)
    mix_ratio *= mix_ratio_clamp_ratio

    if offer_sum >= max(self.total_values_sum / 5, self.total_values_sum * (1 - mix_ratio) + best_offer_sum * mix_ratio):
      return o * 1
    else:
      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 0:
        return o * 1
      #endregion slacky last fight when B

      greedie_offer = self.counts * np.sign(self.values)
      greedie_offer = greedie_offer * \
        (1 - mix_ratio) + self.best_offer * mix_ratio
      greedie_offer = np.min([greedie_offer, self.counts], axis=0)
      offer_result = np.round(greedie_offer)
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1


class Greedie3Agent(AbstractStateactionedAgent):
  agent_name = 'Greedie3Agent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.best_offer = self.values / self.values.max() / 3

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)
    best_offer_sum = np.sum(self.best_offer * self.values)
    if offer_sum > best_offer_sum:
      self.best_offer = o

    mix_ratio = self.current_round / self.max_rounds
    #mix_ratio = mix_ratio ** 2

    if offer_sum >= max(self.total_values_sum / 5, self.total_values_sum * (1 - mix_ratio) + best_offer_sum * mix_ratio):
      return o * 1
    else:

      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 0:
        return o * 1
      #endregion slacky last fight when B

      greedie_offer = self.counts * np.sign(self.values)
      greedie_offer = greedie_offer * \
        (1 - mix_ratio) + self.best_offer * mix_ratio
      greedie_offer = self.counts * \
        (1 - mix_ratio) + greedie_offer * mix_ratio
      greedie_offer = np.min([greedie_offer, self.counts], axis=0)
      offer_result = np.round(greedie_offer)
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1

#class SmartAgent(AbstractStateactionedCheckerAgent):
class SmartAgent(AbstractStateactionedAgent):
  agent_name = 'SmartAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def get_smart_keep_counts(self, keep_ratio):
    bins = np.zeros(self.counts.shape)
    items_pick_plan = int(self.counts.sum() * keep_ratio)
    counts_left = self.counts * 1

    for b in range(0, items_pick_plan):
      counts_left_mask = (counts_left > 0)

      harder_power = 1
      chances = self.values ** harder_power + 1e-2
      chances *= counts_left_mask * 1

      chances /= np.sum(chances)
      cumsum = np.cumsum(chances)

      da_random = np.random.uniform()
      cond1 = (cumsum - chances < da_random)
      cond2 = (cumsum > da_random)
      random_bin = np.logical_and(cond1, cond2)
      bins += random_bin * 1
      counts_left -= random_bin * 1

    return bins

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)

    mix_ratio = self.current_round / self.max_rounds
    mix_ratio = mix_ratio ** 2
    if offer_sum >= self.total_values_sum * (0.67 - mix_ratio * 0.25):
      return o * 1
    else:

      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 0:
        return o * 1
      #endregion slacky last fight when B

      keep_ratio = (1 - mix_ratio) * 0.9 + 0.1

      #region superagressive prelast fight when A
      if self.current_round == self.last_round - 1 and self.me == 0:
        keep_ratio = 0.85
      #endregion superagressive prelast fight when A

      #region agressive last fight when A
      if self.current_round == self.last_round and self.me == 0:
        keep_ratio = 0.67
      #endregion agressive last fight when A

      smart_offer = self.get_smart_keep_counts(keep_ratio)
      offer_result = np.round(smart_offer)
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1


class LinishAgent(AbstractStateactionedAgent):
  agent_name = 'LinishAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)
    mix_ratio = self.current_round / self.max_rounds
    if offer_sum >= max(1, self.total_values_sum * (1 - mix_ratio)):
      return o * 1
    else:
      linish_offer = self.counts * (1 - mix_ratio)
      offer_result = np.round(linish_offer)
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1


class RaiserAgent(AbstractStateactionedAgent):
  agent_name = 'RaiserAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)
    mix_ratio = self.current_round / self.max_rounds
    if offer_sum >= self.total_values_sum * (0.5 + mix_ratio / 3):
      return o * 1
    else:
      raiser_offer = self.counts * \
        np.sign(self.values) * (0.5 + mix_ratio / 2)
      offer_result = np.round(raiser_offer)
      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
      return offer_result * 1


class FairCheaterAgent(AbstractStateactionedAgent):
  agent_name = 'FairCheaterAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def get_smart_keep_counts(self, keep_ratio):
    my_bins = np.zeros(self.counts.shape)
    his_bins = np.zeros(self.counts.shape)
    items_pick_plan = self.counts.sum()
    counts_left = self.counts * 1

    for b in range(0, items_pick_plan):
      counts_left_mask = (counts_left > 0)

      is_my_turn = np.random.uniform() < keep_ratio
      harder_power = 4
      if is_my_turn:
        chances = (self.values ** harder_power + 1e-2) / (self.cheat_opponent_values ** harder_power + 1e-2)
      else:
        chances = (self.cheat_opponent_values ** harder_power + 1e-2) / (self.values ** harder_power + 1e-2)

      chances *= counts_left_mask * 1

      chances /= np.sum(chances)
      cumsum = np.cumsum(chances)

      da_random = np.random.uniform()
      cond1 = (cumsum - chances < da_random)
      cond2 = (cumsum > da_random)
      random_bin = np.logical_and(cond1, cond2)
      if is_my_turn:
        my_bins += random_bin * 1
      else:
        his_bins += random_bin * 1


      counts_left -= random_bin * 1

    return my_bins

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)

    mix_ratio = self.current_round / self.max_rounds

    if offer_sum >= self.total_values_sum * (0.67 - (mix_ratio ** 2) * 0.25):
      return o * 1
    else:
      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 0:
        return o * 1
      #endregion slacky last fight when B

      keep_ratio = 0.4 + 0.2 * (1 - mix_ratio)

      #region superagressive prelast fight when A
      if self.current_round == self.last_round - 1 and self.me == 0:
        keep_ratio = 0.9
      #endregion superagressive prelast fight when A

      #region agressive last fight when A
      if self.current_round == self.last_round and self.me == 0:
        keep_ratio = 0.6
      #endregion agressive last fight when A
      smart_offer_variants = []
      for v in range(0, 50):
        stocho_keep_ratio = keep_ratio + np.random.normal(0, 0.2)
        stocho_keep_ratio = np.clip(stocho_keep_ratio, 0, 1)
        smart_offer = self.get_smart_keep_counts(stocho_keep_ratio)
        smart_offer_variants.append(smart_offer)

      smart_offer_variants = np.array(smart_offer_variants)
      smart_anti_offer_variants = self.counts - smart_offer_variants
      my_profit_variants = (smart_offer_variants * self.values).sum(axis=1)
      his_profit_variants = (smart_anti_offer_variants * self.cheat_opponent_values).sum(axis=1)
      shared_profit_variants = my_profit_variants + his_profit_variants
      stricted_alpha_variants_mask = (my_profit_variants >= his_profit_variants) # binary mask
      stricted_beta_variants_mask = 0.5 + 0.5 * (his_profit_variants >= self.total_values_sum / 2) #float mask
      if np.all(stricted_alpha_variants_mask == False):
        best_variant_id = np.argmax(my_profit_variants)
      else:
        stricted_shared_profit_variants = shared_profit_variants * stricted_alpha_variants_mask * stricted_beta_variants_mask
        best_variant_id = np.argmax(stricted_shared_profit_variants)

      smart_offer = smart_offer_variants[best_variant_id]

      offer_result = np.round(smart_offer)


      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1

      return offer_result * 1




class FairNoisedCheaterAgent(AbstractStateactionedAgent):
  agent_name = 'FairNoisedCheaterAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    values_denormalize_factor = 10 # TODO unhardcode
    #normalized_noise_dev = 0.063
    normalized_noise_dev = 0.1
    noise_dev = normalized_noise_dev * values_denormalize_factor
    self.noised_cheat_opponent_values = self.cheat_opponent_values + np.random.normal(0, noise_dev, size = self.cheat_opponent_values.shape)
    self.name += '_nnd{}'.format(normalized_noise_dev)
  def get_smart_keep_counts(self, keep_ratio):
    my_bins = np.zeros(self.counts.shape)
    his_bins = np.zeros(self.counts.shape)
    items_pick_plan = self.counts.sum()
    counts_left = self.counts * 1

    for b in range(0, items_pick_plan):
      counts_left_mask = (counts_left > 0)

      is_my_turn = np.random.uniform() < keep_ratio
      harder_power = 4
      if is_my_turn:
        chances = (self.values ** harder_power + 1e-2) / (self.noised_cheat_opponent_values ** harder_power + 1e-2)
      else:
        chances = (self.noised_cheat_opponent_values ** harder_power + 1e-2) / (self.values ** harder_power + 1e-2)

      chances *= counts_left_mask * 1

      chances /= np.sum(chances)
      cumsum = np.cumsum(chances)

      da_random = np.random.uniform()
      cond1 = (cumsum - chances < da_random)
      cond2 = (cumsum > da_random)
      random_bin = np.logical_and(cond1, cond2)
      if is_my_turn:
        my_bins += random_bin * 1
      else:
        his_bins += random_bin * 1


      counts_left -= random_bin * 1

    return my_bins

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)

    mix_ratio = self.current_round / self.max_rounds

    if offer_sum >= self.total_values_sum * (0.67 - (mix_ratio ** 2) * 0.25):
      return o * 1
    else:
      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 0:
        return o * 1
      #endregion slacky last fight when B

      keep_ratio = 0.4 + 0.2 * (1 - mix_ratio)

      #region superagressive prelast fight when A
      if self.current_round == self.last_round - 1 and self.me == 0:
        keep_ratio = 0.9
      #endregion superagressive prelast fight when A

      #region agressive last fight when A
      if self.current_round == self.last_round and self.me == 0:
        keep_ratio = 0.6
      #endregion agressive last fight when A
      smart_offer_variants = []
      for v in range(0, 50):
        stocho_keep_ratio = keep_ratio + np.random.normal(0, 0.2)
        stocho_keep_ratio = np.clip(stocho_keep_ratio, 0, 1)
        smart_offer = self.get_smart_keep_counts(stocho_keep_ratio)
        smart_offer_variants.append(smart_offer)

      smart_offer_variants = np.array(smart_offer_variants)
      smart_anti_offer_variants = self.counts - smart_offer_variants
      my_profit_variants = (smart_offer_variants * self.values).sum(axis=1)
      his_profit_variants = (smart_anti_offer_variants * self.noised_cheat_opponent_values).sum(axis=1)
      shared_profit_variants = my_profit_variants + his_profit_variants
      stricted_alpha_variants_mask = (my_profit_variants >= his_profit_variants) # binary mask
      stricted_beta_variants_mask = 0.5 + 0.5 * (his_profit_variants >= self.total_values_sum / 2) #float mask
      if np.all(stricted_alpha_variants_mask == False):
        best_variant_id = np.argmax(my_profit_variants)
      else:
        stricted_shared_profit_variants = shared_profit_variants * stricted_alpha_variants_mask * stricted_beta_variants_mask
        best_variant_id = np.argmax(stricted_shared_profit_variants)

      smart_offer = smart_offer_variants[best_variant_id]

      offer_result = np.round(smart_offer)


      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1

      return offer_result * 1


class RefCheaterAgent(AbstractStateactionedAgent):
  agent_name = 'RefCheaterAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    values_denormalize_factor = 10 # TODO unhardcode
    normalized_noise_dev = 0.063
    noise_dev = normalized_noise_dev * values_denormalize_factor
    self.noised_cheat_opponent_values = self.cheat_opponent_values + np.random.normal(0, noise_dev, size = self.cheat_opponent_values.shape)
    self.noised_cheat_opponent_values = np.clip(self.noised_cheat_opponent_values, 0, 1)
    self.name += '_nnd{}'.format(normalized_noise_dev)

  def get_smart_keep_counts(self, keep_ratio):
    my_bins = np.zeros(self.counts.shape)
    his_bins = np.zeros(self.counts.shape)
    items_pick_plan = self.counts.sum()
    counts_left = self.counts * 1

    for b in range(0, items_pick_plan):
      counts_left_mask = (counts_left > 0)

      #region compensator to rebalance scewed cases (be aware: it works *againts* keep_ratio, and fighting with them, until draw)
      score_factor = (np.sum(my_bins * self.values) + 1.0) / (np.sum(his_bins * self.noised_cheat_opponent_values) + 1.0)
      score_compensator_log = np.log(score_factor) / np.log(100.0)
      #endregion compensator to rebalance scewed cases (be aware: it works *againts* keep_ratio, and fighting with them, until draw)

      is_my_turn = np.random.uniform() < keep_ratio - score_compensator_log
      my_harder_power = 2.5
      his_harder_power = 2.0
      if is_my_turn:
        chances = (self.values ** my_harder_power + 1e-1) / (self.noised_cheat_opponent_values ** his_harder_power + 1e-1)
      else:
        chances = (self.noised_cheat_opponent_values ** his_harder_power + 1e-1) / (self.values ** my_harder_power + 1e-1)

      chances *= counts_left_mask * 1

      chances /= np.sum(chances)
      cumsum = np.cumsum(chances)

      da_random = np.random.uniform()
      cond1 = (cumsum - chances < da_random)
      cond2 = (cumsum > da_random)
      random_bin = np.logical_and(cond1, cond2)

      if is_my_turn:
        if np.sum(random_bin * self.values) > 0:
          my_bins += random_bin * 1
        else:
          his_bins += random_bin * 1
      else:
        his_bins += random_bin * 1

      counts_left -= random_bin * 1

    return my_bins

  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)

    mix_ratio = self.current_round / self.max_rounds

    if offer_sum >= self.total_values_sum * (0.67 - (mix_ratio ** 2) * 0.25):
      return o * 1
    else:
      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 0:
        return o * 1
      #endregion slacky last fight when B

      keep_ratio = 0.4 + 0.2 * (1 - mix_ratio)

      #region superagressive prelast fight when A
      if self.current_round == self.last_round - 1 and self.me == 0:
        keep_ratio = 0.9
      #endregion superagressive prelast fight when A

      #region agressive last fight when A
      if self.current_round == self.last_round and self.me == 0:
        keep_ratio = 0.6
      #endregion agressive last fight when A

      smart_offer_variants = []
      for v in range(0, 50):
        stocho_keep_ratio = keep_ratio + np.random.normal(0, 0.2)
        stocho_keep_ratio = np.clip(stocho_keep_ratio, 0, 1)
        smart_offer = self.get_smart_keep_counts(stocho_keep_ratio)
        smart_offer_variants.append(smart_offer)

      smart_offer_variants = np.array(smart_offer_variants)
      smart_anti_offer_variants = self.counts - smart_offer_variants
      my_profit_variants = (smart_offer_variants * self.values).sum(axis=1)
      his_profit_variants = (smart_anti_offer_variants * self.noised_cheat_opponent_values).sum(axis=1)
      shared_profit_variants = my_profit_variants + his_profit_variants
      stricted_alpha_variants_mask = (my_profit_variants >= his_profit_variants) # binary mask
      stricted_beta_variants_mask = 0.5 + 0.5 * (his_profit_variants >= self.total_values_sum / 2) #float mask
      if np.all(stricted_alpha_variants_mask == False):
        best_variant_id = np.argmax(my_profit_variants)
      else:
        stricted_shared_profit_variants = shared_profit_variants * stricted_alpha_variants_mask * stricted_beta_variants_mask
        best_variant_id = np.argmax(stricted_shared_profit_variants)

      smart_offer = smart_offer_variants[best_variant_id]

      offer_result = np.round(smart_offer)


      if offer_result.sum() == 0:
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1

      return offer_result * 1




class CountieAgent(AbstractStateactionedAgent):
  agent_name = 'CountieAgent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.best_offer = self.values / self.values.max() / 3

  def genetate_countie_offer_result(self):
    mix_ratio = self.current_round / self.max_rounds
    countie_giveaway_plan = self.counts.sum() * 0.3 * (1 - mix_ratio) + self.counts.sum() * 0.7 * mix_ratio
    counts_left = self.counts * 1
    for c in range(0, int(countie_giveaway_plan)):
      unneedness = np.sign(counts_left) * (self.total_values_sum - self.values)
      most_unneed_type = np.argmax(unneedness)
      counts_left[most_unneed_type] -= 1
      total_values_sum_left_threshold = 7 * (1 - mix_ratio) + 5 * mix_ratio
      total_values_sum_left = np.sum(counts_left * self.values)
      if total_values_sum_left < total_values_sum_left_threshold :
        break
    assert np.all(counts_left >= 0)
    return counts_left
  def internal_offer_implementation(self, o):
    offer_sum = np.sum(o * self.values)

    mix_ratio = self.current_round / self.max_rounds

    if offer_sum >= max(self.total_values_sum / 5, 7 * (1 - mix_ratio) + 5 * mix_ratio):
      return o * 1
    else:
      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 2:
        return o * 1
      #endregion slacky last fight when B

      countie_offer_result = self.genetate_countie_offer_result()
      if countie_offer_result.sum() == 0:
        countie_offer_result = np.zeros(self.values.shape)
        countie_offer_result[self.values.argmax()] = 1

      return countie_offer_result
