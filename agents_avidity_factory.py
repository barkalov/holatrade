import numpy as np
import abc
import copy
from perfit import perfit, stocho_perfit
from agents import AbstractStateactionedAgent
def AvidityAgentFactory():

  class AvidityAgent(AbstractStateactionedAgent):
    agent_name = 'AvidityAgent'

    def always_win(self, offer):
      my_values =  self.values
      his_values = self.cheat_opponent_values
      counts = self.counts
      offer = offer * 1 # clone, to not to spoil incoming offer array

      my_total_sum = np.sum(my_values * offer)
      his_total_sum = np.sum(his_values * (counts - offer))
      offer_yet_not_full_mask = (counts - offer) >= 1
      my_values_masked = my_values * (offer_yet_not_full_mask * 1)

      min_win_diff = 1
      while my_total_sum - his_total_sum < min_win_diff and np.max(my_values_masked) > 0:
        offer_agrmax = np.argmax(my_values_masked)
        offer[offer_agrmax] += 1

        my_total_sum = np.sum(my_values * offer)
        his_total_sum = np.sum(his_values * (counts - offer))
        offer_yet_not_full_mask = (counts - offer) >= 1
        my_values_masked = my_values * (offer_yet_not_full_mask * 1)
      return offer



    def generate_float_offer(self, avidity_log):
      # his_values_mean = self.noised_cheat_opponent_values
      # his_values_std_1cnt = np.ones(self.noised_cheat_opponent_values) * 3
      # perfit_ratio_a, perfit_ratio_b = stocho_perfit(self.values, his_values_mean, his_values_std_1cnt, self.counts, avidity_log = avidity_log, epsilon = 1e-1)
      perfit_ratio_a, perfit_ratio_b = perfit(self.values, self.cheat_opponent_values, self.counts, avidity_log = avidity_log, epsilon = 1e-6)
      float_offer_result = perfit_ratio_a * self.counts
      float_offer_result *= np.sign(self.counts) # give up all 0-cost if any
      float_offer_result = self.always_win(float_offer_result)
      return float_offer_result

    def internal_offer_implementation(self, o):
      offer_sum = np.sum(o * self.values)

      semi_ratio = self.current_round / self.max_rounds
      semi_sq_ratio = semi_ratio ** 2
      semi_rewratio = 1 - semi_ratio
      avidity_log = semi_rewratio * 2 + 0.25

      #region superagressive prelast fight when A
      if self.current_round == self.last_round - 1 and self.me == 0:
        avidity_log += np.random.uniform(0, 3)
      #endregion superagressive prelast fight when A

      #region agressive last fight when A
      if self.current_round == self.last_round and self.me == 0:
        avidity_log += np.random.uniform(0, 1)
      #endregion agressive last fight when A

      #region slacky last fight when B
      if self.current_round == self.last_round and self.me == 1 and offer_sum > 2:
        return o * 1
      #endregion slacky last fight when B

      #region safeguard good_incoming_offer_plain_threshold
      if offer_sum >= self.total_values_sum * (0.85 - semi_sq_ratio * 0.2):
        return o * 1
      #endregion safeguard good_incoming_offer_plain_threshold

      avidity_offer = self.generate_float_offer(avidity_log)
      offer_result = np.round(avidity_offer)
      offer_result_sum  = offer_result.sum()
      #region filter&fix outgoing_offer if it's all-zеroes TODO: disable it to record (and to learn then) stupid moves too
      if offer_result_sum == 0:
        print('!!zero-sum offer detected!!')
        offer_result = np.zeros(self.values.shape)
        offer_result[self.values.argmax()] = 1
        offer_result_sum  = offer_result.sum()
      #region filter&fix outgoing_offer if it's all-zеroes TODO: disable it to record (and to learn then) stupid moves too

      #region good_incoming_offer_smart_thershold
      if offer_sum >= offer_result_sum - np.abs(np.random.normal(0, 1)) and offer_sum > 2:
        return o * 1
      #endregion good_incoming_offer_smart_thershold

      return offer_result

  return AvidityAgent