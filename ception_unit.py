import os

import keras
from keras.layers import Input
from keras.layers import BatchNormalization, Activation
from keras.layers.convolutional import Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer

import numpy as np
import json

class CeptionUnit():

  def __init__(self, types_dim, rounds_dim, input_channels_dim, kernel_types_dim, kernel_rounds_dim, channels_dim, upsampling_types_dim_factor, upsampling_rounds_dim_factor, bn_training):
    self.class_name = 'ception'
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim
    self.input_channels_dim = input_channels_dim
    self.kernel_types_dim = kernel_types_dim
    self.kernel_rounds_dim = kernel_rounds_dim
    self.channels_dim = channels_dim
    self.upsampling_types_dim_factor = upsampling_types_dim_factor
    self.upsampling_rounds_dim_factor = upsampling_rounds_dim_factor
    self.bn_training = bn_training    

    self.model = self.build_model()

  def build_model(self):
    model_input = Input(shape=(self.types_dim, self.rounds_dim, self.input_channels_dim))
    x = model_input

    x = Conv2D(self.channels_dim, kernel_size=(self.kernel_types_dim, self.kernel_rounds_dim))(x)
    x = BatchNormalization()(x, training = self.bn_training)
    x = Activation('elu')(x)
    x = UpSampling2D(size=(self.upsampling_types_dim_factor, self.upsampling_rounds_dim_factor))(x)

    model_output = x
    model = Model(model_input, model_output)
    print('====| ception_unit |====')
    model.summary()
    return model

  def export_setup(self):
    conv_weights = self.model.layers[1].get_weights()
    bn_weights = self.model.layers[2].get_weights()
    conv_kernel = conv_weights[0]
    conv_bias = conv_weights[1]
    bn_gamma = bn_weights[0]
    bn_beta = bn_weights[1]
    bn_mean = bn_weights[2]
    bn_variance = bn_weights[3]
    
    setup = {
      'className': self.class_name,
      'typesDim': self.types_dim,
      'roundsDim': self.rounds_dim,
      'inputChannelsDim': self.input_channels_dim,
      'kernelTypesDim': self.kernel_types_dim,
      'kernelRoundsDim': self.kernel_rounds_dim,
      'channelsDim': self.channels_dim,
      'upsamplingTypesDimFactor': self.upsampling_types_dim_factor,
      'upsamplingRoundsDimFactor': self.upsampling_rounds_dim_factor,

      'convKernel': conv_kernel.flatten().tolist(),
      'convBias': conv_bias.flatten().tolist(),
      'bnMean': bn_mean.flatten().tolist(),
      'bnVariance': bn_variance.flatten().tolist(),
      'bnBeta': bn_beta.flatten().tolist(),
      'bnGamma': bn_gamma.flatten().tolist(),
    }
    return setup

  def export_setup_to_json(self, json_path):
    setup = self.export_setup()
    os.makedirs(os.path.dirname(json_path), exist_ok=True)
    with open(json_path, 'w') as outfile:
      json.dump(setup, outfile)
  
  def add_to_stack(self, source, master_source):
    return self.model(source)

if __name__ == '__main__':
  test_input_batch = np.linspace(0, 1, 1 * 3 * 6 * 7)
  test_input_batch = test_input_batch.reshape((1, 3, 6, 7))
  ception_unit = CeptionUnit(3, 6, 7, 3, 3, 4, 2, 2, None)
  ception_unit.export_setup_to_json('./exported_model/ception_test/setup.json')
  test_pred = ception_unit.model.predict(test_input_batch)
  print('test_pred ch 0', test_pred[0,:,:,0])
  print('test_pred ch 1', test_pred[0,:,:,1])
  print('test_pred ch 2', test_pred[0,:,:,2])
  print('test_pred ch 3', test_pred[0,:,:,3])
  print('test_pred shape', test_pred.shape)
  print('test_pred fl', test_pred.flatten())
  print('test_pred fl len', len(test_pred.flatten()))
  print('test_pred sum', test_pred.sum())
  print('end')
