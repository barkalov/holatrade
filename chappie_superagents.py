import numpy as np
import copy

#from gamestate_dataset_element import GamestateDatasetElement
from gameaction_dataset_element import GameactionDatasetElement
#from gamereplay_dataset_element import GamereplayDatasetElement

from agents import AbstractStateactionedAgent

#from gameaction_metapenalty_guesser_zaa import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserZAA
#global_gameaction_metapenalty_guesser_zaa = GameactionMetapenaltyGuesserZAA(name = 'gameaction_metapenalty_guesser_zaa_16', preload = True)

from gameaction_metapenalty_guesser_xaa import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserXAA
global_gameaction_metapenalty_guesser_xaa = GameactionMetapenaltyGuesserXAA(name = 'gameaction_metapenalty_guesser_xaa', preload = True)

#from gameaction_metapenalty_guesser_xab import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserXAB
#global_gameaction_metapenalty_guesser_xab = GameactionMetapenaltyGuesserXAB(name = 'gameaction_metapenalty_guesser_xab', preload = True)

#from gameaction_metapenalty_guesser_xac import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserXAC
#global_gameaction_metapenalty_guesser_xac = GameactionMetapenaltyGuesserXAC(name = 'gameaction_metapenalty_guesser_xac', preload = True)

#from gameaction_metapenalty_guesser_saa import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserSAA
#global_gameaction_metapenalty_guesser_saa = GameactionMetapenaltyGuesserSAA(name = 'gameaction_metapenalty_guesser_saa', preload = True)

#from gameaction_metapenalty_guesser_xba import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserXBA
#global_gameaction_metapenalty_guesser_xba = GameactionMetapenaltyGuesserXBA(name = 'gameaction_metapenalty_guesser_xba_as_xaa_xl', preload = True)

#from gameaction_metapenalty_guesser_uaa import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserUAA
#global_gameaction_metapenalty_guesser_uaa = GameactionMetapenaltyGuesserUAA(name = 'gameaction_metapenalty_guesser_uaa', preload = True)

from gameaction_metapenalty_guesser_ubd import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserUBD
global_gameaction_metapenalty_guesser_ubd = GameactionMetapenaltyGuesserUBD(name = 'gameaction_metapenalty_guesser_ubd', preload = True)

from gameaction_metapenalty_guesser_waa import GameactionMetapenaltyGuesser as GameactionMetapenaltyGuesserWAA
global_gameaction_metapenalty_guesser_waa = GameactionMetapenaltyGuesserWAA(name = 'gameaction_metapenalty_guesser_waa', preload = True)

# dirty hack to not to recreate on each game TODO put in factory, that creates agent classes


class AbstractChappieSuperagent(AbstractStateactionedAgent):
  agent_name = 'AbstractChappieSuperagent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = None

  def genetate_best_offer_result(self, o):
    best_offer_result = np.sign(self.values) * self.counts
    best_offer_metapenalty_univalue = 100

    semi_id = self.current_round * 2 + self.me - 2 # -2 since current_round is 1-based (will be +1 on semi_ids append)

    offer_result_tries = []
    for try_num in range(0, 200):
      offer_result_try = np.random.uniform(low=0, high=self.counts)
      offer_result_try = np.round(offer_result_try).astype(int)
      assert np.all([offer_result_try >= 0]) and np.all(offer_result_try <= self.counts)
      if np.sum(offer_result_try * self.values) > 2:
        offer_result_tries.append(offer_result_try)

    offer_result_tries = np.unique(offer_result_tries, axis=0)
    offer_result_tries = offer_result_tries[:24]
    gameaction_dataset_batch = []
    for offer_result_try in offer_result_tries:
      is_action_accepted = np.all(o == offer_result_try)
      gameaction_dataset_element_clone = copy.deepcopy(self.gameaction_dataset_element)
      gameaction_dataset_element_clone.his_steps_as_i_see.append(o)
      gameaction_dataset_element_clone.my_steps_as_i_see.append(offer_result_try)
      gameaction_dataset_element_clone.is_accepteds.append(is_action_accepted * 1)
      gameaction_dataset_element_clone.semi_ids.append(semi_id + 1)
      gameaction_dataset_batch.append(gameaction_dataset_element_clone)

    predicred_metapenalty_univalues_try_batch = self.guesser.predict_batch(gameaction_dataset_batch)

    for id, predicred_metapenalty_univalues_try in enumerate(predicred_metapenalty_univalues_try_batch):
      offer_result_try = offer_result_tries[id]
      denormalized_predicred_metapenalty_univalues_try = GameactionDatasetElement.denormalize_metapenalty_oraculer_gameaction_outputs(predicred_metapenalty_univalues_try)
      denormalized_predicred_metapenalty_univalue_try = denormalized_predicred_metapenalty_univalues_try[0]
      if denormalized_predicred_metapenalty_univalue_try < best_offer_metapenalty_univalue:
        best_offer_metapenalty_univalue = denormalized_predicred_metapenalty_univalue_try
        best_offer_result = offer_result_try

    return best_offer_result

  def internal_offer_implementation(self, o):
    if np.sum(o * self.values) >= self.total_values_sum * 9 / 10:
      return o * 1
    else:
      best_offer_result = self.genetate_best_offer_result(o)
      if np.sum(o * self.values) >= np.sum(best_offer_result * self.values):
        return o * 1

      return best_offer_result


class ChappieMGZAASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGZAASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_zaa

class ChappieMGXAASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGXAASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_xaa

class ChappieMGXABSuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGXABSuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_xab

class ChappieMGXACSuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGXACSuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_xac

class ChappieMGSAASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGSAASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_saa

class ChappieMGXBASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGXBASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_xba

class ChappieMGXCASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGXCASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_xca

class ChappieMGXDASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGXDASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_xda

class ChappieMGXDASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGXDASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_xda

class ChappieMGUAASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGUAASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_uaa

class ChappieMGUBDSuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGUBDSuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_ubd

class ChappieMGWAASuperagent(AbstractChappieSuperagent):
  agent_name = 'ChappieMGWAASuperagent'
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.guesser = global_gameaction_metapenalty_guesser_waa






