import numpy as np

def chop_guesser_dataset(normalized_game_inputs, normalized_game_outputs, as_gameaction = False):

    rounds_dim = normalized_game_inputs.shape[2]
    chopped_inputs = [[] for i in range(rounds_dim)]
    chopped_outputs = [[] for i in range(rounds_dim)]

    for sample_id in range(0, len(normalized_game_inputs)):

        layers = normalized_game_inputs[sample_id]
        target = normalized_game_outputs[sample_id]

        # mrc is masked round column (types * channels), that can be used to safely fill begin or rounds (after end chopping)
        if as_gameaction:
            mrc_l0 = np.zeros(layers.shape[0]) # [his_steps_as_i_see] <=> [0, 0, 0]
            mrc_l1 = layers[:,0,3] # [my_steps_as_i_see] <=> counts from 0 column (from any actually)
        else: # if as_gamestate
            mrc_l0 = layers[:,0,3] # [my_steps_as_i_see] <=> counts from 0 column (from any actually)
            mrc_l1 = np.zeros(layers.shape[0]) # [his_steps_as_i_see] <=> [0, 0, 0]

        mrc_l2 = layers[:,0,2] # [my_values] <=> my_values from 0 column (from any actually)
        mrc_l3 = layers[:,0,3] # [counts] <=> counts from 0 column (from any actually)
        mrc_l4 = np.zeros(layers.shape[0]) # [semi_ids] <=> [0, 0, 0]
        mrc_l5 = layers[:,0,5] # [is_a] <=> is_a from 0 column (from any actually)

        mrc_l0 = np.expand_dims(mrc_l0, axis=1)
        mrc_l1 = np.expand_dims(mrc_l1, axis=1)
        mrc_l2 = np.expand_dims(mrc_l2, axis=1)
        mrc_l3 = np.expand_dims(mrc_l3, axis=1)
        mrc_l4 = np.expand_dims(mrc_l4, axis=1)
        mrc_l5 = np.expand_dims(mrc_l5, axis=1)

        greatest_semi_id = layers[0,-1:,4] * 9 # * 9 since denormalize #TODO check by hands after semi_id fix
        if as_gameaction:
            rounds_played_count = int((greatest_semi_id + 1) / 2.0)
        else: # if as_gamestate
            rounds_played_count = int((greatest_semi_id + 2) / 2.0)
        assert rounds_played_count > 0 or mrc_l5[0] == 0
        assert rounds_played_count >= 0 or mrc_l5[0] == 1  # rounds_played_count (in gameaction) my be == 0 for B, when A stupidly accepts initial zero-valued virtual offer
        assert rounds_played_count <= rounds_dim

        #print('rounds_played_count', rounds_played_count)


        mrc = np.concatenate([mrc_l0, mrc_l1, mrc_l2, mrc_l3, mrc_l4, mrc_l5], axis=-1)
        mrc_as_layers = np.expand_dims(mrc, axis=1) # (types, channels) comes (types, 1, channels) where 1 is in rounds_dim

        for chop_rounds_count in range(0, rounds_played_count):

            masked_dummy_fill = mrc_as_layers.repeat(chop_rounds_count, axis=1)

            chop_rounds_to = (rounds_dim - chop_rounds_count)
            chopped_layers = layers[:,: chop_rounds_to,:]

            welded_layers = np.concatenate([masked_dummy_fill, chopped_layers], axis=-2)
            rounds_unchopped_count = rounds_played_count - chop_rounds_count
            chopped_inputs[rounds_unchopped_count - 1].append(welded_layers)
            chopped_outputs[rounds_unchopped_count - 1].append(target)


    for r in range(0, rounds_dim):
        chopped_inputs[r] = np.array(chopped_inputs[r])
        chopped_outputs[r] = np.array(chopped_outputs[r])

    return chopped_inputs, chopped_outputs


def chop_oraculer_dataset(normalized_game_inputs, normalized_game_outputs, as_gameaction = False):

    rounds_dim = normalized_game_inputs.shape[2]
    chopped_inputs = [[] for i in range(rounds_dim)]
    chopped_outputs = [[] for i in range(rounds_dim)]

    for sample_id in range(0, len(normalized_game_inputs)):

        layers = normalized_game_inputs[sample_id]
        target = normalized_game_outputs[sample_id]

        # mrc is masked round column (types * channels), that can be used to safely fill begin or rounds (after end chopping)
        if as_gameaction:
            mrc_l0 = np.zeros(layers.shape[0]) # [his_steps_as_i_see] <=> [0, 0, 0]
            mrc_l1 = layers[:,0,4] # [my_steps_as_i_see] <=> counts from 0 column (from any actually)
        else: # if as_gamestate
            mrc_l0 = layers[:,0,4] # [my_steps_as_i_see] <=> counts from 0 column (from any actually)
            mrc_l1 = np.zeros(layers.shape[0]) # [his_steps_as_i_see] <=> [0, 0, 0]

        mrc_l2 = layers[:,0,2] # [my_values] <=> my_values from 0 column (from any actually)
        mrc_l3 = layers[:,0,3] # [his_values] <=> his_values from 0 column (from any actually)
        mrc_l4 = layers[:,0,4] # [counts] <=> counts from 0 column (from any actually)
        mrc_l5 = np.zeros(layers.shape[0]) # [semi_ids] <=> [0, 0, 0]
        mrc_l6 = layers[:,0,6] # [is_a] <=> is_a from 0 column (from any actually)

        mrc_l0 = np.expand_dims(mrc_l0, axis=1)
        mrc_l1 = np.expand_dims(mrc_l1, axis=1)
        mrc_l2 = np.expand_dims(mrc_l2, axis=1)
        mrc_l3 = np.expand_dims(mrc_l3, axis=1)
        mrc_l4 = np.expand_dims(mrc_l4, axis=1)
        mrc_l5 = np.expand_dims(mrc_l5, axis=1)
        mrc_l6 = np.expand_dims(mrc_l6, axis=1)

        greatest_semi_id = layers[0,-1:,5] * 9 # * 9 since denormalize #TODO check by hands after semi_id fix
        if as_gameaction:
            rounds_played_count = int((greatest_semi_id + 1) / 2.0)
        else: # if as_gamestate
            rounds_played_count = int((greatest_semi_id + 2) / 2.0)
        assert rounds_played_count > 0 or mrc_l6[0] == 0
        assert rounds_played_count >= 0 or mrc_l6[0] == 1  # rounds_played_count (in gameaction) my be == 0 for B, when A stupidly accepts initial zero-valued virtual offer
        assert rounds_played_count <= rounds_dim

        #print('rounds_played_count', rounds_played_count)


        mrc = np.concatenate([mrc_l0, mrc_l1, mrc_l2, mrc_l3, mrc_l4, mrc_l5, mrc_l6], axis=-1)
        mrc_as_layers = np.expand_dims(mrc, axis=1) # (types, channels) comes (types, 1, channels) where 1 is in rounds_dim

        for chop_rounds_count in range(0, rounds_played_count):

            masked_dummy_fill = mrc_as_layers.repeat(chop_rounds_count, axis=1)

            chop_rounds_to = (rounds_dim - chop_rounds_count)
            chopped_layers = layers[:,: chop_rounds_to,:]

            welded_layers = np.concatenate([masked_dummy_fill, chopped_layers], axis=-2)
            rounds_unchopped_count = rounds_played_count - chop_rounds_count
            chopped_inputs[rounds_unchopped_count - 1].append(welded_layers)
            chopped_outputs[rounds_unchopped_count - 1].append(target)


    for r in range(0, rounds_dim):
        chopped_inputs[r] = np.array(chopped_inputs[r])
        chopped_outputs[r] = np.array(chopped_outputs[r])

    return chopped_inputs, chopped_outputs