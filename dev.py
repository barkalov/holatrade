
import pathlib
import keras.backend as K
import tensorflow as tf
from keras.callbacks import TensorBoard

import numpy as np


from gameaction_score_guesser_dab import GameactionScoreGuesser

##### Preinit
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
sess = tf.Session(config=config)
set_session(sess)  # set this TensorFlow session as the default session for Keras

score_guesser_name = 'gameaction_score_guesser_dab'
score_guesser = GameactionScoreGuesser(score_guesser_name, preload=True)
score_guesser.poor_supermodel.trainable = False
score_guesser.rich_supermodel.trainable = False # does not need actually, since rich is inside poor

zzz = np.array([[[[0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.2       , 0.        , 0.6       , 0.11111111,          1.        ]],        [[0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.6       , 0.        , 0.6       , 0.        ,          1.        ],         [0.        , 0.2       , 0.        , 0.6       , 0.11111111,          1.        ]],        [[0.        , 1.        , 0.2       , 1.        , 0.        ,          1.        ],         [0.        , 1.        , 0.2       , 1.        , 0.        ,          1.        ],         [0.        , 1.        , 0.2       , 1.        , 0.        ,          1.        ],         [0.        , 1.        , 0.2       , 1.        , 0.        ,          1.        ],         [0.        , 1.        , 0.2       , 1.        , 0.        ,          1.        ],         [0.        , 1.        , 0.2       , 1.        , 0.11111111,          1.        ]]]])
pred_score = score_guesser.poor_supermodel.predict(zzz) * 10
print('pred_score', pred_score)
