import numpy as np

def enrich_dataset(normalized_game_inputs):

    typeunshared_inputs = normalized_game_inputs[:,:,:,0:4] # layers 0 to 3 my, his steps, my_values and counts
    typeunshared_a = np.roll(typeunshared_inputs, 1, axis=1) # axis types
    typeunshared_b = np.roll(typeunshared_inputs, -1, axis=1) # axis types
    typeunshared_sum = (typeunshared_a + typeunshared_b)
    typeunshared_absdiff = np.abs(typeunshared_a - typeunshared_b)
    normalized_enriched_game_inputs = np.concatenate([normalized_game_inputs, typeunshared_sum, typeunshared_absdiff], axis=-1)
    return normalized_enriched_game_inputs