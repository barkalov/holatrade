import os

import keras
from keras.layers import Input, Concatenate
from keras.layers import BatchNormalization, Activation
from keras.layers.convolutional import Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer

import numpy as np
import json

class FinceptionUnit():

  def __init__(self, types_dim, rounds_dim, input_channels_dim, master_input_channels_dim, scale_factor, bn_training):
    self.class_name = 'finception'
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim
    self.input_channels_dim = input_channels_dim
    self.master_input_channels_dim = master_input_channels_dim
    self.scale_factor = scale_factor
    self.bn_training = bn_training    

    self.model = self.build_model()

  def build_model(self):
    model_input = Input(shape=(self.types_dim, self.rounds_dim, self.input_channels_dim))
    master_input = Input(shape=(self.types_dim, self.rounds_dim, self.master_input_channels_dim))
    x = model_input

    # a
    x_t = Conv2D(16 * self.scale_factor, kernel_size=(self.types_dim, 1), padding='valid', name='finception_unit_conv_a')(x)
    x_t = BatchNormalization(name='finception_unit_bn_a')(x_t, training = self.bn_training)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)
  
    x = Concatenate()([x, x_t, master_input])
  
    # b
    x = Conv2D(16 * self.scale_factor, kernel_size=(1, self.rounds_dim), padding='valid', name='finception_unit_conv_b')(x)
    x = BatchNormalization(name='finception_unit_bn_b')(x, training = self.bn_training)
    x = Activation('elu')(x)
  
    x = keras.layers.AveragePooling2D(pool_size=(self.types_dim, 1))(x)

    model_output = x
    model = Model([model_input, master_input], model_output)
    print('====| finception_unit |====')
    model.summary()
    return model

  def export_setup(self):
    conv_a_weights = self.model.layers[1].get_weights()
    bn_a_weights = self.model.layers[2].get_weights()

    conv_b_weights = self.model.layers[7].get_weights()
    bn_b_weights = self.model.layers[8].get_weights()

    # a
    conv_a_kernel = conv_a_weights[0]
    conv_a_bias = conv_a_weights[1]
    bn_a_gamma = bn_a_weights[0]
    bn_a_beta = bn_a_weights[1]
    bn_a_mean = bn_a_weights[2]
    bn_a_variance = bn_a_weights[3]

    # b
    conv_b_kernel = conv_b_weights[0]
    conv_b_bias = conv_b_weights[1]
    bn_b_gamma = bn_b_weights[0]
    bn_b_beta = bn_b_weights[1]
    bn_b_mean = bn_b_weights[2]
    bn_b_variance = bn_b_weights[3]
    
    setup = {
      'className': self.class_name,
      'typesDim': self.types_dim,
      'roundsDim': self.rounds_dim,
      'inputChannelsDim': self.input_channels_dim,
      'masterInputChannelsDim': self.master_input_channels_dim,
      'scaleFactor': self.scale_factor,

      #a
      'convKernelA': conv_a_kernel.flatten().tolist(),
      'convBiasA': conv_a_bias.flatten().tolist(),
      'bnMeanA': bn_a_mean.flatten().tolist(),
      'bnVarianceA': bn_a_variance.flatten().tolist(),
      'bnBetaA': bn_a_beta.flatten().tolist(),
      'bnGammaA': bn_a_gamma.flatten().tolist(),

      #b
      'convKernelB': conv_b_kernel.flatten().tolist(),
      'convBiasB': conv_b_bias.flatten().tolist(),
      'bnMeanB': bn_b_mean.flatten().tolist(),
      'bnVarianceB': bn_b_variance.flatten().tolist(),
      'bnBetaB': bn_b_beta.flatten().tolist(),
      'bnGammaB': bn_b_gamma.flatten().tolist(),
    }
    return setup

  def export_setup_to_json(self, json_path):
    setup = self.export_setup()
    os.makedirs(os.path.dirname(json_path), exist_ok=True)
    with open(json_path, 'w') as outfile:
      json.dump(setup, outfile)
  
  def add_to_stack(self, source, master_source):
    return self.model([source, master_source])

if __name__ == '__main__':
  test_input_batch = np.linspace(0, 1, 1 * 3 * 6 * 11)
  test_input_batch = test_input_batch.reshape((1, 3, 6, 11))
  
  master_test_input_batch = np.linspace(0, 1, 1 * 3 * 6 * 7)
  master_test_input_batch = master_test_input_batch.reshape((1, 3, 6, 7))
  
  finception_unit = FinceptionUnit(3, 6, 11, 7, 3, None)
  finception_unit.export_setup_to_json('./exported_model/finception_test/setup.json')
  test_pred = finception_unit.model.predict([test_input_batch, master_test_input_batch])
  print('test_pred ch 0', test_pred[0,:,:,0])
  print('test_pred ch 1', test_pred[0,:,:,1])
  print('test_pred ch 2', test_pred[0,:,:,2])
  print('test_pred ch 3', test_pred[0,:,:,3])
  print('test_pred shape', test_pred.shape)
  print('test_pred fl', test_pred.flatten())
  print('test_pred fl len', len(test_pred.flatten()))
  print('test_pred sum', test_pred.sum())
  print('end')
