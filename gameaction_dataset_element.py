import numpy as np
import copy

from abstract_gamestateaction_dataset_element import AbstractGamestateactionDatasetElement
#### helper-class to put raw stat data into and export as unnormalized layers dims (types, rounds, channels) as input and (channels,) as output
class VGGameactionDatasetElement(AbstractGamestateactionDatasetElement):
    value_guesser_gameaction_inputs_norm_factors = [5, 5, 10, 5, 9, 1]
    value_guesser_gameaction_outputs_norm_factors = [10]
    @staticmethod
    def normalize_value_guesser_gameaction_inputs(value_guesser_gameaction_inputs):
        return value_guesser_gameaction_inputs / GameactionDatasetElement.value_guesser_gameaction_inputs_norm_factors
    @staticmethod
    def normalize_value_guesser_gameaction_outputs(value_guesser_gameaction_outputs):
        return value_guesser_gameaction_outputs / GameactionDatasetElement.value_guesser_gameaction_outputs_norm_factors
    @staticmethod
    def denormalize_value_guesser_gameaction_inputs(normalized_value_guesser_gameaction_inputs):
        return normalized_value_guesser_gameaction_inputs * GameactionDatasetElement.value_guesser_gameaction_inputs_norm_factors
    @staticmethod
    def denormalize_value_guesser_gameaction_outputs(normalized_value_guesser_gameaction_outputs):
        return normalized_value_guesser_gameaction_outputs * GameactionDatasetElement.value_guesser_gameaction_outputs_norm_factors

    def export_value_guesser_gameaction(self):
        cloned_dataset_element = copy.deepcopy(self)
        cloned_dataset_element.finalize_via_extend()
        l0 = np.expand_dims(cloned_dataset_element.his_steps_as_i_see, 0).T

        l1 = np.expand_dims(cloned_dataset_element.my_steps_as_i_see, 0).T

        l2 = np.expand_dims(cloned_dataset_element.my_values, -1).repeat(6, 1).T
        l2 = np.expand_dims(l2, 0).T

        l3 = np.expand_dims(cloned_dataset_element.counts, -1).repeat(6, 1).T
        l3 = np.expand_dims(l3, 0).T

        l4 = np.expand_dims(cloned_dataset_element.semi_ids, -1).repeat(3, 1)
        l4 = np.expand_dims(l4, 0).T

        l5 = np.expand_dims(cloned_dataset_element.is_a, -1).repeat(6)
        l5 = np.expand_dims(l5, -1).repeat(3, 1)
        l5 = np.expand_dims(l5, 0).T

        target = np.array(cloned_dataset_element.his_values)

        layers = np.concatenate([l0, l1, l2, l3, l4, l5], axis = -1)
        return GameactionDatasetElement.normalize_value_guesser_gameaction_inputs(layers), GameactionDatasetElement.normalize_value_guesser_gameaction_outputs(target)

class SGGameactionDatasetElement(VGGameactionDatasetElement):
    score_guesser_gameaction_inputs_norm_factors = [5, 5, 10, 5, 9, 1]
    score_guesser_gameaction_outputs_norm_factors = [10]
    @staticmethod
    def normalize_score_guesser_gameaction_inputs(score_guesser_gameaction_inputs):
        return score_guesser_gameaction_inputs / GameactionDatasetElement.score_guesser_gameaction_inputs_norm_factors
    @staticmethod
    def normalize_score_guesser_gameaction_outputs(score_guesser_gameaction_outputs):
        return score_guesser_gameaction_outputs / GameactionDatasetElement.score_guesser_gameaction_outputs_norm_factors
    @staticmethod
    def denormalize_score_guesser_gameaction_inputs(normalized_score_guesser_gameaction_inputs):
        return normalized_score_guesser_gameaction_inputs * GameactionDatasetElement.score_guesser_gameaction_inputs_norm_factors
    @staticmethod
    def denormalize_score_guesser_gameaction_outputs(normalized_score_guesser_gameaction_outputs):
        return normalized_score_guesser_gameaction_outputs * GameactionDatasetElement.score_guesser_gameaction_outputs_norm_factors

    def export_score_guesser_gameaction(self):
        cloned_dataset_element = copy.deepcopy(self)
        cloned_dataset_element.finalize_via_extend()
        l0 = np.expand_dims(cloned_dataset_element.his_steps_as_i_see, 0).T

        l1 = np.expand_dims(cloned_dataset_element.my_steps_as_i_see, 0).T

        l2 = np.expand_dims(cloned_dataset_element.my_values, -1).repeat(6, 1).T
        l2 = np.expand_dims(l2, 0).T

        l3 = np.expand_dims(cloned_dataset_element.counts, -1).repeat(6, 1).T
        l3 = np.expand_dims(l3, 0).T

        l4 = np.expand_dims(cloned_dataset_element.semi_ids, -1).repeat(3, 1)
        l4 = np.expand_dims(l4, 0).T

        l5 = np.expand_dims(cloned_dataset_element.is_a, -1).repeat(6)
        l5 = np.expand_dims(l5, -1).repeat(3, 1)
        l5 = np.expand_dims(l5, 0).T

        #region dirty hack to export yet-unfinished games
        if cloned_dataset_element.score_pair is None:
            target = np.array([-1, -1])
        else:
            target = np.array(cloned_dataset_element.score_pair)
        #end region dirty hack to export yet-unfinished games

        layers = np.concatenate([l0, l1, l2, l3, l4, l5], axis = -1)
        return GameactionDatasetElement.normalize_score_guesser_gameaction_inputs(layers), GameactionDatasetElement.normalize_score_guesser_gameaction_outputs(target)

class GameactionDatasetElement(SGGameactionDatasetElement):
    metapenalty_oraculer_gameaction_inputs_norm_factors = [5, 5, 10, 10, 5, 9, 1]
    metapenalty_oraculer_gameaction_outputs_norm_factors = [1]
    @staticmethod
    def normalize_metapenalty_oraculer_gameaction_inputs(metapenalty_oraculer_gameaction_inputs):
        return metapenalty_oraculer_gameaction_inputs / GameactionDatasetElement.metapenalty_oraculer_gameaction_inputs_norm_factors
    @staticmethod
    def normalize_metapenalty_oraculer_gameaction_outputs(metapenalty_oraculer_gameaction_outputs):
        return metapenalty_oraculer_gameaction_outputs / GameactionDatasetElement.metapenalty_oraculer_gameaction_outputs_norm_factors
    @staticmethod
    def denormalize_metapenalty_oraculer_gameaction_inputs(normalized_metapenalty_oraculer_gameaction_inputs):
        return normalized_metapenalty_oraculer_gameaction_inputs * GameactionDatasetElement.metapenalty_oraculer_gameaction_inputs_norm_factors
    @staticmethod
    def denormalize_metapenalty_oraculer_gameaction_outputs(normalized_metapenalty_oraculer_gameaction_outputs):
        return normalized_metapenalty_oraculer_gameaction_outputs * GameactionDatasetElement.metapenalty_oraculer_gameaction_outputs_norm_factors

    def calc_metapenalty(self, my_score_ratio, his_score_ratio, anticipatorily_ratio):
        # m1
        penalty_a = 1 - my_score_ratio
        penalty_b = his_score_ratio
        penalty_c = anticipatorily_ratio
        metapenalty = \
            1.0 * (penalty_a ** 2) + \
            0.2 * (penalty_a * penalty_b) + \
            0.2 * ((penalty_a * penalty_b * penalty_c) ** (2/3)) + \
            0
        return metapenalty / 3

    def calc_metapenalty_m2(self, my_score_ratio, his_score_ratio, anticipatorily_ratio):
        # m2
        penalty_a = 1 - my_score_ratio
        penalty_b = his_score_ratio
        penalty_c = anticipatorily_ratio
        penalty_a_flipsquared = 1 - (1 - penalty_a) ** 2
        metapenalty = \
            1.0 * penalty_a_flipsquared + \
            0.2 * (penalty_a * penalty_b) + \
            0.2 * (penalty_a * penalty_b * penalty_c) + \
            0.2 * max(0, his_score_ratio - my_score_ratio) + \
            2.0 * penalty_b * max(0, 0.5 - my_score_ratio ) + \
            0
        return metapenalty

    def calc_metapenalty_m3(self, my_score_ratio, his_score_ratio, anticipatorily_ratio):
        # m3
        penalty_a = 1 - my_score_ratio
        penalty_b = his_score_ratio
        penalty_c = anticipatorily_ratio
        metapenalty = \
            1.0 * penalty_a + \
            0.2 * (penalty_a * penalty_b) + \
            0.2 * (penalty_a * penalty_b * penalty_c) + \
            0.2 * max(0, his_score_ratio - my_score_ratio) + \
            1.0 * penalty_b * max(0, 0.5 - my_score_ratio ) + \
            0
        return metapenalty

    def export_metapenalty_oraculer_gameaction(self):
        cloned_dataset_element = copy.deepcopy(self)
        cloned_dataset_element.finalize_via_extend()
        l0 = np.expand_dims(cloned_dataset_element.his_steps_as_i_see, 0).T

        l1 = np.expand_dims(cloned_dataset_element.my_steps_as_i_see, 0).T

        l2 = np.expand_dims(cloned_dataset_element.my_values, -1).repeat(6, 1).T
        l2 = np.expand_dims(l2, 0).T

        l3 = np.expand_dims(cloned_dataset_element.his_values, -1).repeat(6, 1).T
        l3 = np.expand_dims(l3, 0).T

        l4 = np.expand_dims(cloned_dataset_element.counts, -1).repeat(6, 1).T
        l4 = np.expand_dims(l4, 0).T

        l5 = np.expand_dims(cloned_dataset_element.semi_ids, -1).repeat(3, 1)
        l5 = np.expand_dims(l5, 0).T

        l6 = np.expand_dims(cloned_dataset_element.is_a, -1).repeat(6)
        l6 = np.expand_dims(l6, -1).repeat(3, 1)
        l6 = np.expand_dims(l6, 0).T

        #region dirty hack to export yet-unfinished games
        if cloned_dataset_element.score_pair is None:
            target = np.array([float('nan')])
        else:
            my_score_ratio = np.array(cloned_dataset_element.score_pair[0]) / 10
            his_score_ratio = np.array(cloned_dataset_element.score_pair[1]) / 10
            anticipatorily_ratio = 1 - np.max(cloned_dataset_element.semi_ids) / 10
            target = self.calc_metapenalty(my_score_ratio, his_score_ratio, anticipatorily_ratio)
        #end region dirty hack to export yet-unfinished games

        layers = np.concatenate([l0, l1, l2, l3, l4, l5, l6], axis = -1)
        return GameactionDatasetElement.normalize_metapenalty_oraculer_gameaction_inputs(layers), GameactionDatasetElement.normalize_metapenalty_oraculer_gameaction_outputs(target)

    def export_metapenalty_oraculer_gameaction_outputs(self):
        cloned_dataset_element = copy.deepcopy(self)
        cloned_dataset_element.finalize_via_extend()

        #region dirty hack to export yet-unfinished games
        if cloned_dataset_element.score_pair is None:
            assert(False)
            target = np.array([float('nan')])
        else:
            my_score_ratio = np.array(cloned_dataset_element.score_pair[0]) / 10
            his_score_ratio = np.array(cloned_dataset_element.score_pair[1]) / 10
            anticipatorily_ratio = 1 - np.max(cloned_dataset_element.semi_ids) / 10
            target = self.calc_metapenalty(my_score_ratio, his_score_ratio, anticipatorily_ratio)
        #end region dirty hack to export yet-unfinished games


        return GameactionDatasetElement.normalize_metapenalty_oraculer_gameaction_outputs(target)
