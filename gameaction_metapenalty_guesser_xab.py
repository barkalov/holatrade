import pathlib

import keras
from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Concatenate
from keras.layers import BatchNormalization, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Conv1D, Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer, Lambda, RepeatVector, Multiply
import matplotlib.pyplot as plt
import keras.backend as K
import tensorflow as tf
from keras.callbacks import TensorBoard

import numpy as np
from chop_dataset import chop_guesser_dataset


from unit_stack import UnitStack
from hyception_unit import HyceptionUnit
from ception_unit import CeptionUnit
from reception_unit import ReceptionUnit
from finception_unit import FinceptionUnit

class GameactionMetapenaltyGuesser():

  def __init__(self, name, batch_size = 128 * 8, preload = False, bn_training = None, lr_div = 10):
    self.name = name
    self.batch_size = batch_size
    self.test_batch_size = 32
    # Input shape
    self.types_dim = 3
    self.rounds_dim = 6

    self.type_round_channels_dim = 2 # his_offer, my_offer
    self.type_channels_dim = 2 # my_values, counts
    self.round_channels_dim = 1 # semi_num
    self.single_channels_dim = 1 # is_a

    self.enrichable_channels_dim = \
      self.type_round_channels_dim + \
      self.type_channels_dim

    self.unenrichable_channels_dim = \
      self.round_channels_dim + \
      self.single_channels_dim

    self.hyper_channels_dim = self.enrichable_channels_dim * 3 + self.unenrichable_channels_dim

    self.metapenalty_dim = 1

    self.do_ratio = 0
    self.bn_training = bn_training
    self.lr_div = lr_div
    self.multiplier_alpha = 3
    self.multiplier_beta = 6

    self.dataset_i = 0 #ring-counter for dataset refresh


    self.ultramodel = self.build_ultramodel()

    optimizer = Adam(lr = 0.01/lr_div)

    self.ultramodel.compile(loss='mse', metrics=['mse', 'mae'], optimizer=optimizer)

    if preload == True:
      self.load_models()


  def build_ultramodel(self):
    scale_factor = 1
    units = [
      HyceptionUnit(types_dim = 3, rounds_dim = 6, type_round_channels_dim = 2, type_channels_dim = 2, round_channels_dim = 1, single_channels_dim = 1),
      CeptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 2 * 3 + 2 * 3 + 1 + 1, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 12, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = self.bn_training),

      ReceptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 12, scale_factor = scale_factor, bn_training = self.bn_training),
      CeptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 16 * scale_factor * 2 + 12, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 18, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = self.bn_training),

      ReceptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 18, scale_factor = scale_factor, bn_training = self.bn_training),
      CeptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 16 * scale_factor * 2 + 18, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 24, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = self.bn_training),

      FinceptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 24, master_input_channels_dim = 2 * 3 + 2 * 3 + 1 + 1, scale_factor = scale_factor, bn_training = self.bn_training),
      CeptionUnit(types_dim = 1, rounds_dim = 1, input_channels_dim = 16 * scale_factor, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 1, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = self.bn_training),
    ]

    self.unit_stack = UnitStack(units, types_dim = 3, rounds_dim = 6, type_round_channels_dim = 2, type_channels_dim = 2, round_channels_dim = 1, single_channels_dim = 1)



    ultramodel_type_round_data_input = Input(shape=(self.types_dim, self.rounds_dim, self.type_round_channels_dim))
    ultramodel_type_data_input = Input(shape=(self.types_dim, self.type_channels_dim))
    ultramodel_round_data_input = Input(shape=(self.rounds_dim, self.round_channels_dim))
    ultramodel_single_data_input = Input(shape=(self.single_channels_dim, ))

    x = self.unit_stack.model([
      ultramodel_type_round_data_input, \
      ultramodel_type_data_input, \
      ultramodel_round_data_input, \
      ultramodel_single_data_input, \
    ])
    x = Flatten()(x)
    ultramodel_output = x

    ultramodel = Model([
      ultramodel_type_round_data_input, \
      ultramodel_type_data_input, \
      ultramodel_round_data_input, \
      ultramodel_single_data_input, \
    ], ultramodel_output)


    print('====| ultramodel |====')
    ultramodel.summary()
    return ultramodel

  def train(self, epochs, update_dataset_interval = 2 * 1000, print_interval = 500, demonstrate_interval=500, save_interval=1000, tb_interval=100, start_epoch = 0):
    tb_log_path = './tb/' + self.name
    pathlib.Path(tb_log_path).mkdir(parents=True, exist_ok=True)
    tb_callback = TensorBoard(log_dir=tb_log_path, histogram_freq=5, batch_size=self.batch_size, write_graph=False)
    tb_callback.set_model(self.ultramodel)

    smooth_loss = np.array([0.0, 0.0, 0.0])
    smooth_test_loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)

    for epoch in range(start_epoch, epochs + start_epoch):
      if epoch % update_dataset_interval == 0:
        self.update_dataset()
      # ---------------------
      #  Train ultramodel
      # ---------------------

      # Test the ultramodel
      test_loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)
      for complete_level in range(0, self.rounds_dim - 1): # -1 because of last chop-bin for actions is always empty
        assert len(self.chopped_normalized_game_inputs[complete_level]) == len(self.chopped_normalized_game_outputs[complete_level])

        idx = np.random.randint(0, self.chopped_normalized_game_inputs[complete_level].shape[0], self.test_batch_size)
        normalized_game_inputs_batch = self.chopped_normalized_game_inputs[complete_level][idx]
        normalized_game_outputs_batch = self.chopped_normalized_game_outputs[complete_level][idx]

        #region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
        dim_permutation = np.random.permutation(self.types_dim)
        normalized_game_inputs_batch = normalized_game_inputs_batch[:,dim_permutation]
        #endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

        type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_batch)

        test_loss[complete_level] = np.array(self.ultramodel.test_on_batch([type_round_data, type_data, round_data, single_data], normalized_game_outputs_batch))
        if test_loss[complete_level][0] < 2e-3 and complete_level == 0:
          print ('stange lo at CL{}: test_loss {} for out {}'.format(complete_level, test_loss[complete_level], normalized_game_outputs_batch))
        if epoch % print_interval == 0:
          print ('{} [CL{} TST] [CUS: {}] [MSE: {}] [MAE: {}]'.format(epoch, complete_level, test_loss[complete_level][0], test_loss[complete_level][1], test_loss[complete_level][2]))

      smooth_test_loss = test_loss * 0.005 + smooth_test_loss * 0.995


      # Train the ultramodel
      loss = np.array([0.0, 0.0, 0.0])
      normalized_game_inputs_batch = []
      normalized_game_outputs_batch = []
      for complete_level in range(0, self.rounds_dim - 1): # -1 because of last chop-bin for actions is always empty
        assert len(self.chopped_normalized_game_inputs[complete_level]) == len(self.chopped_normalized_game_outputs[complete_level])
        cl_batch_size = 1 + int(self.batch_size / (self.rounds_dim - 1)) # -1 because of last chop-bin for actions is always empty
        idx = np.random.randint(0, self.chopped_normalized_game_inputs[complete_level].shape[0], cl_batch_size)
        normalized_game_inputs_batch.append(self.chopped_normalized_game_inputs[complete_level][idx])
        normalized_game_outputs_batch.append(self.chopped_normalized_game_outputs[complete_level][idx])

      normalized_game_inputs_batch = np.concatenate(normalized_game_inputs_batch, axis=0)
      normalized_game_outputs_batch = np.concatenate(normalized_game_outputs_batch, axis=0)


      #region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
      dim_permutation = np.random.permutation(self.types_dim)
      normalized_game_inputs_batch = normalized_game_inputs_batch[:,dim_permutation]
      #endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

      type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_batch)

      loss = np.array(self.ultramodel.train_on_batch([type_round_data, type_data, round_data, single_data], normalized_game_outputs_batch))
      if epoch % print_interval == 0:
        print ('{} [TRN] [CUS: {}] [MSE: {}] [MAE: {}]'.format(epoch, loss[0], loss[1], loss[2]))

      smooth_loss = loss * 0.005 + smooth_loss * 0.995


      if epoch % tb_interval == 0:
        tb_loss_names = []
        tb_loss = []
        #region train
        tb_loss.append(np.array([loss]))
        tb_loss.append(np.array([smooth_loss]))
        #base_loss_names = ['MSE', 'MAE']
        base_loss_names = ['CUS', 'MSE', 'MAE']
        smooth_suffix = ['', '_smooth']
        complete_level_suffix = np.arange(0, self.rounds_dim)
        #train_test = ['TRN', 'TST']
        train_test = ['TRN']
        for tt in train_test:
            for ss in smooth_suffix:
                for bln in base_loss_names:
                    loss_name = '[{}] {}{}'.format(tt, bln, ss)
                    tb_loss_names.append(loss_name)
        #endregion train

        #region test
        tb_loss.append(test_loss)
        tb_loss.append(smooth_test_loss)
        #base_loss_names = ['MSE', 'MAE']
        base_loss_names = ['CUS', 'MSE', 'MAE']
        smooth_suffix = ['', '_smooth']
        complete_level_suffix = np.arange(0, self.rounds_dim)
        #train_test = ['TRN', 'TST']
        train_test = ['TST']
        for tt in train_test:
            for ss in smooth_suffix:
                for cl in complete_level_suffix:
                    for bln in base_loss_names:
                        loss_name = '[CL{} {}] {}{}'.format(cl, tt, bln, ss)
                        tb_loss_names.append(loss_name)
        #endregion test
        tb_loss = np.concatenate(tb_loss, axis = 0).flatten()
        self.write_tb_log(tb_callback, tb_loss_names, tb_loss, epoch)

      if epoch % demonstrate_interval == 0:
        self.demonstrate_progress(epoch)

      if epoch % save_interval == 0:
        self.save_models()
        self.export_setups_to_json()

  def save_models(self):
    pathlib.Path('./saved_model/' + self.name).mkdir(parents=True, exist_ok=True)
    self.ultramodel.save_weights('./saved_model/' + self.name + '/ultramodel.h5')

  def demonstrate_progress(self, epoch):
    nf_5 = 5 # norm_factor TODO denormalize explicitly
    print(' == demo at epoch {} == '.format(epoch))
    #prev_printoptions = np.get_printoptions()
    #np.set_printoptions(precision=2)

    for game_num in range(0, 8):
      for is_a in (True, False):
        letter = 'A' if is_a else 'B'
        unchopped_normalized_game_inputs_one_as_dataset = np.expand_dims(self.normalized_game_inputs[game_num * 2 + (not is_a) * 1], axis = 0)
        unchopped_normalized_game_outputs_one_as_dataset = np.expand_dims(self.normalized_game_outputs[game_num * 2 + (not is_a) * 1], axis = 0)
        normalized_counts = unchopped_normalized_game_inputs_one_as_dataset[0,:,0,3] # TODO: unhardcode 3 channeid
        print('game#{} {} counts is {}, normalized ground-truth is {}'.format(game_num, letter, normalized_counts * nf_5, unchopped_normalized_game_outputs_one_as_dataset))

        chopped_normalized_game_inputs_one_as_dataset, chopped_normalized_game_outputs_one_as_dataset = chop_guesser_dataset(unchopped_normalized_game_inputs_one_as_dataset, unchopped_normalized_game_outputs_one_as_dataset, as_gameaction=True)

        for complete_level in range(0, self.rounds_dim - 1): # -1 because of last chop-bin for actions is always empty
          normalized_game_inputs_one_as_batch = chopped_normalized_game_inputs_one_as_dataset[complete_level]
          normalized_game_outputs_one_as_batch = chopped_normalized_game_outputs_one_as_dataset[complete_level]
          if len(normalized_game_inputs_one_as_batch) == 0:
            print ('early end of game#{}'.format(game_num))
            break
          else:

            type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_one_as_batch)

            normalized_game_predictions_one_as_batch = self.ultramodel.predict([type_round_data, type_data, round_data, single_data])
            ae = np.abs(normalized_game_outputs_one_as_batch - normalized_game_predictions_one_as_batch)
            print ('game#{} {} [CL{}] prediction: {} (target: {}), absolute error: {} (mean {:.1f})'.format(game_num, letter, complete_level, normalized_game_predictions_one_as_batch, normalized_game_outputs_one_as_batch, ae, ae.mean()))

            normalized_game_inputs_none_one_as_batch = normalized_game_inputs_one_as_batch * 1
            normalized_game_inputs_none_one_as_batch[:,:,5,1] = np.zeros((1, 3))

            type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_none_one_as_batch)

            normalized_game_predictions_none_one_as_batch = self.ultramodel.predict([type_round_data, type_data, round_data, single_data])
            print ('game#{} {} [CL{}] none-prediction: {}'.format(game_num, letter, complete_level, normalized_game_predictions_none_one_as_batch))

            normalized_game_inputs_all_one_as_batch = normalized_game_inputs_one_as_batch * 1
            normalized_game_inputs_all_one_as_batch[:,:,5,1] = normalized_game_inputs_all_one_as_batch[:,:,0, 3]

            type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_all_one_as_batch)

            normalized_game_predictions_all_one_as_batch = self.ultramodel.predict([type_round_data, type_data, round_data, single_data])
            print ('game#{} {} [CL{}] all-prediction: {}'.format(game_num, letter, complete_level, normalized_game_predictions_all_one_as_batch))


    #np.set_printoptions(prev_printoptions)

  def load_models(self):
    self.ultramodel.load_weights('./saved_model/' + self.name + '/ultramodel.h5')

  def write_tb_log(self, callback, names, logs, batch_no):
    for name, value in zip(names, logs):
      summary = tf.Summary()
      summary_value = summary.value.add()
      summary_value.simple_value = value
      summary_value.tag = name
      callback.writer.add_summary(summary, batch_no)
      callback.writer.flush()

  def dbg_hyperized_batch_to_multiple_inputs(self, hypercube):
    type_round_data = hypercube[:, :, :, 0:2]
    type_data = hypercube[:, :, 0, 2:4]
    round_data = hypercube[:, 0, :, 4:5]
    single_data = hypercube[:, 0, 0, 5:6]
    return type_round_data, type_data, round_data, single_data

  def export_setups_to_json(self):
    json_path = './exported_model/' + self.name + '/setups.json'
    self.unit_stack.export_setups_to_json(json_path)

  def update_dataset(self):
    tour_names = ['tour_sigma25_xparty_run0_100k', 'tour_sigma25_xparty_run1_100k']
    tour_name = tour_names[self.dataset_i]
    print('time to update dataset, now {}'.format(tour_name))
    normalized_game_inputs_path = 'stat_tournament/' + tour_name + '_to_score_guesser_gameaction_dataset_inputs.npy'
    normalized_game_outputs_path = 'stat_tournament/' + tour_name + '_to_metapenalty_m1_oraculer_gameaction_dataset_outputs.npy'
    self.normalized_game_inputs = np.load(normalized_game_inputs_path)
    self.normalized_game_outputs = np.load(normalized_game_outputs_path)
    print('loaded normalized_game_inputs_path.shape', self.normalized_game_inputs.shape)
    print('loaded normalized_game_outputs_path.shape', self.normalized_game_outputs.shape)
    self.dataset_i += 1
    self.dataset_i %= len(tour_names)

    #region blind mode
    #self.normalized_game_inputs = np.random.uniform(0, 1, self.normalized_game_inputs.shape)
    #endregion blind mode
    assert len(self.normalized_game_inputs) == len(self.normalized_game_outputs)

    self.chopped_normalized_game_inputs, self.chopped_normalized_game_outputs = chop_guesser_dataset(self.normalized_game_inputs, self.normalized_game_outputs, as_gameaction=True)

  def predict(self, gameaction_dataset_element):

    #layers, _ = gameaction_dataset_element.export_metapenalty_guesser_gameaction()
    layers, _ = gameaction_dataset_element.export_score_guesser_gameaction() #because export_metapenalty_guesser_gameaction yet not exist
    normalized_one_as_inputs = np.array([layers])
    #TODO
    ##region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
    #dim_permutation = np.random.permutation(self.types_dim)
    #normalized_game_inputs_batch = normalized_game_inputs_batch[:,dim_permutation]
    ##endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

    type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_one_as_inputs)
    return self.ultramodel.predict([type_round_data, type_data, round_data, single_data])

  def predict_batch(self, gameaction_dataset_batch):
    #print('predict batch len:', len(gameaction_dataset_batch))
    inputs = []
    for gameaction_dataset_element in gameaction_dataset_batch:
      layers, _ = gameaction_dataset_element.export_score_guesser_gameaction() #because export_metapenalty_guesser_gameaction yet not exist
      inputs.append(layers)
    normalized_batch_as_inputs = np.array(inputs)
    #TODO
    ##region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
    #dim_permutation = np.random.permutation(self.types_dim)
    #normalized_game_inputs_batch = normalized_game_inputs_batch[:,dim_permutation]
    ##endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

    type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_batch_as_inputs)
    return self.ultramodel.predict([type_round_data, type_data, round_data, single_data])

if __name__ == '__main__':
  ##### Preinit
  from keras.backend.tensorflow_backend import set_session
  config = tf.ConfigProto()
  config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
  sess = tf.Session(config=config)
  set_session(sess)  # set this TensorFlow session as the default session for Keras

  metapenalty_guesser = GameactionMetapenaltyGuesser(name = 'gameaction_metapenalty_guesser_xab', preload = True, batch_size = 128 * 32, bn_training = False, lr_div = 250)


  # #region cooltest

  # cooltest_type_round_input_batch = np.linspace(0, 1, 1 * 3 * 6 * 2)
  # cooltest_type_round_input_batch = cooltest_type_round_input_batch.reshape((1, 3, 6, 2))

  # cooltest_type_input_batch = np.linspace(0, 1, 3 * 1 * 2)
  # cooltest_type_input_batch = cooltest_type_input_batch.reshape((1, 3, 2))

  # cooltest_round_input_batch = np.linspace(0, 1, 1 * 6 * 1)
  # cooltest_round_input_batch = cooltest_round_input_batch.reshape((1, 6, 1))

  # cooltest_single_input_batch = np.linspace(0, 1, 1 * 1)
  # cooltest_single_input_batch = cooltest_single_input_batch.reshape((1, 1))

  # cooltest_inputs_batch = [
  #   cooltest_type_round_input_batch,
  #   cooltest_type_input_batch,
  #   cooltest_round_input_batch,
  #   cooltest_single_input_batch,
  # ]
  # cooltest_preds = metapenalty_guesser.ultramodel.predict(cooltest_inputs_batch)
  # print('cooltest_preds', cooltest_preds)

  # #endregion cooltest

  metapenalty_guesser.train(epochs=5*1000*1000, start_epoch = 25*1000, update_dataset_interval = 1*1000)
