import pathlib

import keras
from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Concatenate
from keras.layers import BatchNormalization, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Conv1D, Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer, Lambda, RepeatVector, Multiply
import matplotlib.pyplot as plt
import keras.backend as K
import tensorflow as tf
from keras.callbacks import TensorBoard

import numpy as np
from chop_dataset import chop_oraculer_dataset

from layer_hyperizer import Hyperizer


class GameactionMetapenaltyOraculer():

  def __init__(self, name, batch_size = 128 * 8, preload = False):
    self.name = name
    self.batch_size = batch_size
    # Input shape
    self.types_dim = 3
    self.rounds_dim = 6

    self.type_round_channels_dim = 2 # his_offer, my_offer
    self.type_channels_dim = 3 # my_values, his_values, counts
    self.round_channels_dim = 1 # semi_num
    self.single_channels_dim = 1 # is_a

    self.enrichable_channels_dim = \
      self.type_round_channels_dim + \
      self.type_channels_dim

    self.unenrichable_channels_dim = \
      self.round_channels_dim + \
      self.single_channels_dim

    self.hyper_channels_dim = self.enrichable_channels_dim * 3 + self.unenrichable_channels_dim

    self.metapenalty_dim = 1

    self.do_ratio = 0.0
    self.bn_training = False
    self.multiplier_alpha = 3
    self.multiplier_beta = 6

    self.dataset_i = 0 #ring-counter for dataset refresh

    self.supermodel = self.build_supermodel()
    self.hypermodel = self.build_hypermodel(self.supermodel)

    optimizer = Adam(lr = 0.01/1000000)

    self.hypermodel.compile(loss='mse', metrics=['mse', 'mae'], optimizer=optimizer)

    if preload == True:
      self.load_models()

  def build_supermodel(self):
    #regularizer = keras.regularizers.l2(1e-5)
    supermodel_input = Input(shape=(self.types_dim, self.rounds_dim, self.hyper_channels_dim))
    x = supermodel_input

    x = Conv2D(24 * self.multiplier_alpha, kernel_size=(1, 1))(x) # ok with 12
    x = BatchNormalization()(x, training = self.bn_training)
    x = Activation('elu')(x)

    #region reception
    x = Dropout(self.do_ratio)(x)

    x_ti = Conv2D(8 * self.multiplier_beta, kernel_size=(1, 1))(x)
    x_ti = BatchNormalization()(x_ti, training = self.bn_training)
    x_ti = Activation('elu')(x_ti)

    x_t = Conv2D(16 * self.multiplier_beta, kernel_size=(self.types_dim, 1), padding='valid')(x_ti)
    x_t = BatchNormalization()(x_t, training = self.bn_training)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    x_ri = Conv2D(8 * self.multiplier_beta, kernel_size=(1, 1))(x)
    x_ri = BatchNormalization()(x_ri, training = self.bn_training)
    x_ri = Activation('elu')(x_ri)

    x_r = Conv2D(16 * self.multiplier_beta, kernel_size=(1, self.rounds_dim), padding='valid')(x_ri)
    x_r = BatchNormalization()(x_r, training = self.bn_training)
    x_r = Activation('elu')(x_r)
    x_r = UpSampling2D(size=(1, self.rounds_dim))(x_r)

    x = Concatenate()([x, x_t, x_r])
    #endregion reception

    x = Conv2D(36 * self.multiplier_beta, kernel_size=(1, 1))(x)  # ok with 24
    x = BatchNormalization()(x, training = self.bn_training)
    x = Activation('elu')(x)

    #region reception
    x = Dropout(self.do_ratio)(x)

    x_ti = Conv2D(8 * self.multiplier_beta, kernel_size=(1, 1))(x)
    x_ti = BatchNormalization()(x_ti, training = self.bn_training)
    x_ti = Activation('elu')(x_ti)

    x_t = Conv2D(16 * self.multiplier_beta, kernel_size=(self.types_dim, 1), padding='valid')(x_ti)
    x_t = BatchNormalization()(x_t, training = self.bn_training)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    x_ri = Conv2D(8 * self.multiplier_beta, kernel_size=(1, 1))(x)
    x_ri = BatchNormalization()(x_ri, training = self.bn_training)
    x_ri = Activation('elu')(x_ri)

    x_r = Conv2D(16 * self.multiplier_beta, kernel_size=(1, self.rounds_dim), padding='valid')(x_ri)
    x_r = BatchNormalization()(x_r, training = self.bn_training)
    x_r = Activation('elu')(x_r)
    x_r = UpSampling2D(size=(1, self.rounds_dim))(x_r)

    x = Concatenate()([x, x_t, x_r])
    #endregion reception

    x = Conv2D(48 * self.multiplier_beta, kernel_size=(1, 1))(x) # ok with 36
    x = BatchNormalization()(x, training = self.bn_training)
    x = Activation('elu')(x)

    x = Conv2D(24 * self.multiplier_beta, kernel_size=(1, 1))(x) # ok with 36
    x = BatchNormalization()(x, training = self.bn_training)
    x = Activation('elu')(x)

    #region finception
    x = Dropout(self.do_ratio)(x)

    x_t = Conv2D(16 * self.multiplier_beta, kernel_size=(self.types_dim, 1), padding='valid')(x)
    x_t = BatchNormalization()(x_t, training = self.bn_training)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    #region diception in finception
    x = Concatenate()([x, x_t, supermodel_input])
    #endregion diception in finception

    x = Conv2D(16 * self.multiplier_alpha, kernel_size=(1, self.rounds_dim), padding='valid')(x)
    x = BatchNormalization()(x, training = self.bn_training)
    x = Activation('elu')(x)
    #endregion finception

    x = Conv2D(self.metapenalty_dim, kernel_size=(1, 1), padding='valid')(x)
    x = Activation('elu')(x)

    x = keras.layers.AveragePooling2D(pool_size=(self.types_dim, 1))(x)
    x = Flatten()(x)

    supermodel_output = x
    supermodel = Model(supermodel_input, supermodel_output)
    print('====| supermodel |====')
    supermodel.summary()
    return supermodel


  def build_hypermodel(self, supermodel):
    hypermodel_type_round_data_input = Input(shape=(self.types_dim, self.rounds_dim, self.type_round_channels_dim))
    hypermodel_type_data_input = Input(shape=(self.types_dim, self.type_channels_dim))
    hypermodel_round_data_input = Input(shape=(self.rounds_dim, self.round_channels_dim))
    hypermodel_single_data_input = Input(shape=(self.single_channels_dim, ))

    x = Hyperizer(self.types_dim, self.rounds_dim, is_do_enrich = True, is_oldschool = True)([
      hypermodel_type_round_data_input, \
      hypermodel_type_data_input, \
      hypermodel_round_data_input, \
      hypermodel_single_data_input, \
    ])

    x = supermodel(x)

    hypermodel_output = x

    hypermodel = Model([
      hypermodel_type_round_data_input, \
      hypermodel_type_data_input, \
      hypermodel_round_data_input, \
      hypermodel_single_data_input, \
    ], hypermodel_output)

    print('====| hypermodel |====')
    hypermodel.summary()
    return hypermodel

  def train(self, epochs, update_dataset_interval = 5 * 1000, print_interval = 500, demonstrate_interval=500, save_interval=1000, tb_interval=100, start_epoch = 0):
    tb_log_path = './tb/' + self.name
    pathlib.Path(tb_log_path).mkdir(parents=True, exist_ok=True)
    tb_callback = TensorBoard(log_dir=tb_log_path, histogram_freq=5, batch_size=self.batch_size, write_graph=False)
    tb_callback.set_model(self.hypermodel)

    smooth_loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)

    for epoch in range(start_epoch, epochs + start_epoch):
      if epoch % update_dataset_interval == 0:
        self.update_dataset()
      # ---------------------
      #  Train hypermodel
      # ---------------------

      # Train the hypermodel (wants decoder to mistake images as real)
      loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)
      for complete_level in range(0, self.rounds_dim - 1): # -1 because of last chop-bin for actions is always empty
        assert len(self.chopped_normalized_game_inputs[complete_level]) == len(self.chopped_normalized_game_outputs[complete_level])

        idx = np.random.randint(0, self.chopped_normalized_game_inputs[complete_level].shape[0], self.batch_size)
        normalized_game_inputs_batch = self.chopped_normalized_game_inputs[complete_level][idx]
        normalized_game_outputs_batch = self.chopped_normalized_game_outputs[complete_level][idx]

        #region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
        dim_permutation = np.random.permutation(self.types_dim)
        normalized_game_inputs_batch = normalized_game_inputs_batch[:,dim_permutation]
        #endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

        type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_batch)

        loss[complete_level] = np.array(self.hypermodel.train_on_batch([type_round_data, type_data, round_data, single_data], normalized_game_outputs_batch))
        if loss[complete_level][0] < 2e-3 and complete_level == 0:
          print ('stange lo at CL{}: loss {} for out {}'.format(complete_level, loss[complete_level], normalized_game_outputs_batch))
        if epoch % print_interval == 0:
          #print ('{} [CL{} TRN] [mse loss: {}] [mae metric: {}]'.format(epoch, complete_level, loss[complete_level][0], loss[complete_level][1]))
          print ('{} [CL{} TRN] [CUS: {}] [MSE: {}] [MAE: {}]'.format(epoch, complete_level, loss[complete_level][0], loss[complete_level][1], loss[complete_level][2]))

      smooth_loss = loss * 0.005 + smooth_loss * 0.995


      if epoch % tb_interval == 0:
        tb_loss = np.array([loss, smooth_loss]).flatten()
        #base_loss_names = ['MSE', 'MAE']
        base_loss_names = ['CUS', 'MSE', 'MAE']
        smooth_suffix = ['', '_smooth']
        complete_level_suffix = np.arange(0, self.rounds_dim)
        #train_test = ['TRN', 'TST']
        train_test = ['TRN']
        tb_loss_names = []
        for tt in train_test:
            for ss in smooth_suffix:
                for cl in complete_level_suffix:
                    for bln in base_loss_names:
                        loss_name = '[CL{} {}] {}{}'.format(cl, tt, bln, ss)
                        tb_loss_names.append(loss_name)

        self.write_tb_log(tb_callback, tb_loss_names, tb_loss, epoch)

      if epoch % demonstrate_interval == 0:
        self.demonstrate_progress(epoch)

      if epoch % save_interval == 0:
        self.save_models()

  def save_models(self):
    pathlib.Path('./saved_model/' + self.name).mkdir(parents=True, exist_ok=True)
    self.supermodel.save_weights('./saved_model/' + self.name + '/supermodel.h5')

  def demonstrate_progress(self, epoch):
    nf_5 = 5 # norm_factor TODO denormalize explicitly
    print(' == demo at epoch {} == '.format(epoch))
    #prev_printoptions = np.get_printoptions()
    #np.set_printoptions(precision=2)

    for game_num in range(0, 8):
      for is_a in (True, False):
        letter = 'A' if is_a else 'B'
        unchopped_normalized_game_inputs_one_as_dataset = np.expand_dims(self.normalized_game_inputs[game_num * 2 + (not is_a) * 1], axis = 0)
        unchopped_normalized_game_outputs_one_as_dataset = np.expand_dims(self.normalized_game_outputs[game_num * 2 + (not is_a) * 1], axis = 0)
        normalized_counts = unchopped_normalized_game_inputs_one_as_dataset[0,:,0,4] # TODO: unhardcode 4 channeid
        print('game#{} {} counts is {}, normalized ground-truth is {}'.format(game_num, letter, normalized_counts * nf_5, unchopped_normalized_game_outputs_one_as_dataset))

        chopped_normalized_game_inputs_one_as_dataset, chopped_normalized_game_outputs_one_as_dataset = chop_oraculer_dataset(unchopped_normalized_game_inputs_one_as_dataset, unchopped_normalized_game_outputs_one_as_dataset, as_gameaction=True)

        for complete_level in range(0, self.rounds_dim - 1): # -1 because of last chop-bin for actions is always empty
          normalized_game_inputs_one_as_batch = chopped_normalized_game_inputs_one_as_dataset[complete_level]
          normalized_game_outputs_one_as_batch = chopped_normalized_game_outputs_one_as_dataset[complete_level]
          if len(normalized_game_inputs_one_as_batch) == 0:
            print ('early end of game#{}'.format(game_num))
            break
          else:

            type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_one_as_batch)

            normalized_game_predictions_one_as_batch = self.hypermodel.predict([type_round_data, type_data, round_data, single_data])
            ae = np.abs(normalized_game_outputs_one_as_batch - normalized_game_predictions_one_as_batch)
            print ('game#{} {} [CL{}] prediction: {} (target: {}), absolute error: {} (mean {:.1f})'.format(game_num, letter, complete_level, normalized_game_predictions_one_as_batch, normalized_game_outputs_one_as_batch, ae, ae.mean()))

            normalized_game_inputs_none_one_as_batch = normalized_game_inputs_one_as_batch * 1
            normalized_game_inputs_none_one_as_batch[:,:,5,1] = np.zeros((1, 3))

            type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_none_one_as_batch)

            normalized_game_predictions_none_one_as_batch = self.hypermodel.predict([type_round_data, type_data, round_data, single_data])
            print ('game#{} {} [CL{}] none-prediction: {}'.format(game_num, letter, complete_level, normalized_game_predictions_none_one_as_batch))

            normalized_game_inputs_all_one_as_batch = normalized_game_inputs_one_as_batch * 1
            normalized_game_inputs_all_one_as_batch[:,:,5,1] = normalized_game_inputs_all_one_as_batch[:,:,0, 4]

            type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_game_inputs_all_one_as_batch)

            normalized_game_predictions_all_one_as_batch = self.hypermodel.predict([type_round_data, type_data, round_data, single_data])
            print ('game#{} {} [CL{}] all-prediction: {}'.format(game_num, letter, complete_level, normalized_game_predictions_all_one_as_batch))


    #np.set_printoptions(prev_printoptions)

  def load_models(self):
    self.supermodel.load_weights('./saved_model/' + self.name + '/supermodel.h5')

  def write_tb_log(self, callback, names, logs, batch_no):
    for name, value in zip(names, logs):
      summary = tf.Summary()
      summary_value = summary.value.add()
      summary_value.simple_value = value
      summary_value.tag = name
      callback.writer.add_summary(summary, batch_no)
      callback.writer.flush()

  def dbg_hyperized_batch_to_multiple_inputs(self, hypercube):
    type_round_data = hypercube[:, :, :, 0:2]
    type_data = hypercube[:, :, 0, 2:5]
    round_data = hypercube[:, 0, :, 5:6]
    single_data = hypercube[:, 0, 0, 6:7]
    return type_round_data, type_data, round_data, single_data


  def update_dataset(self):
    tour_names = ['tour_sigma10_nochappies0_500k', 'tour_sigma10_nochappies1_500k', 'tour_sigma20_nochappies0_500k']
    tour_name = tour_names[self.dataset_i]
    print('time to update dataset, now {}'.format(tour_name))
    normalized_game_inputs_path = 'stat_tournament/' + tour_name + '_to_metapenalty_m1_oraculer_gameaction_dataset_inputs.npy'
    normalized_game_outputs_path = 'stat_tournament/' + tour_name + '_to_metapenalty_m1_oraculer_gameaction_dataset_outputs.npy'
    self.normalized_game_inputs = np.load(normalized_game_inputs_path)
    self.normalized_game_outputs = np.load(normalized_game_outputs_path)
    print('loaded normalized_game_inputs_path.shape', self.normalized_game_inputs.shape)
    print('loaded normalized_game_outputs_path.shape', self.normalized_game_outputs.shape)
    self.dataset_i += 1
    self.dataset_i %= len(tour_names)

    #region blind mode
    #self.normalized_game_inputs = np.random.uniform(0, 1, self.normalized_game_inputs.shape)
    #endregion blind mode
    assert len(self.normalized_game_inputs) == len(self.normalized_game_outputs)

    self.chopped_normalized_game_inputs, self.chopped_normalized_game_outputs = chop_oraculer_dataset(self.normalized_game_inputs, self.normalized_game_outputs, as_gameaction=True)

  def predict(self, gameaction_dataset_element):

    layers, _ = gameaction_dataset_element.export_metapenalty_oraculer_gameaction()
    normalized_one_as_inputs = np.array([layers])
    #TODO
    ##region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
    #dim_permutation = np.random.permutation(self.types_dim)
    #normalized_game_inputs_batch = normalized_game_inputs_batch[:,dim_permutation]
    ##endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

    type_round_data, type_data, round_data, single_data = self.dbg_hyperized_batch_to_multiple_inputs(normalized_one_as_inputs)
    return self.hypermodel.predict([type_round_data, type_data, round_data, single_data])

if __name__ == '__main__':
  ##### Preinit
  from keras.backend.tensorflow_backend import set_session
  config = tf.ConfigProto()
  config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
  sess = tf.Session(config=config)
  set_session(sess)  # set this TensorFlow session as the default session for Keras

  metapenalty_oraculer = GameactionMetapenaltyOraculer(name = 'gameaction_metapenalty_oraculer_aaa_fat_try_hyp03', preload = True, batch_size=128)
  metapenalty_oraculer.train(epochs=5*1000*1000, start_epoch = 130*1000)
