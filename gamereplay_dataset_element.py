import numpy as np
import copy



types_dim = 3
rounds_dim = 6


import abc
import numpy as np
import copy
class GamereplayDataset:
  def __init__(self, types_dim, rounds_dim, games_dim):
    self.types_dim = 3
    self.rounds_dim = 6
    self.is_finalized = False
    self.a_steps_as_a_see = [None] * games_dim
    self.b_steps_as_b_see = [None] * games_dim
    self.a_values = [None] * games_dim
    self.b_values = [None] * games_dim
    self.counts = [None] * games_dim
    self.score_pair = [None] * games_dim
  def finalize(self):
    assert not self.is_finalized
    self.is_finalized = True
    self.a_steps_as_a_see = np.array(self.a_steps_as_a_see)
    self.b_steps_as_b_see = np.array(self.b_steps_as_b_see)
    self.a_values = np.array(self.a_values)
    self.b_values = np.array(self.b_values)
    self.counts = np.array(self.counts)
    self.score_pair = np.array(self.score_pair)
  def save(self, path):
    assert self.is_finalized
    np.save(path, [self])
  @staticmethod
  def load(path):
    array_with_dataset = np.load(path)
    return array_with_dataset[0]

class AbstractGamereplayDatasetElement:
  def __init__(self, dataset, id):
    self.dataset = dataset
    self.id = id
    self.dataset.a_steps_as_a_see[self.id] = []
    self.dataset.b_steps_as_b_see[self.id] = []
    self.dataset.a_values[self.id] = []
    self.dataset.b_values[self.id] = []
    self.dataset.counts[self.id] = []
    self.dataset.score_pair[self.id] = []

  #region agent API
  def start(self, my_values, his_values, counts, is_a):
    assert not self.dataset.is_finalized
    if is_a:
      self.dataset.a_values[self.id] = my_values
      self.dataset.b_values[self.id] = his_values
    else: #b
      self.dataset.a_values[self.id] = his_values
      self.dataset.b_values[self.id] = my_values
    self.dataset.counts[self.id] = counts

  def add_incoming_offer(self, offer, is_a):
    assert not self.dataset.is_finalized
    if is_a:
      self.dataset.b_steps_as_b_see[self.id].append(self.dataset.counts[self.id] - offer)
    else:
      self.dataset.a_steps_as_a_see[self.id].append(self.dataset.counts[self.id] - offer)

  def add_outgoing_offer(self, offer, is_a):
    assert not self.dataset.is_finalized
    if is_a:
      self.dataset.a_steps_as_a_see[self.id].append(offer)
    else:
      self.dataset.b_steps_as_b_see[self.id].append(offer)

  def end(self, score_pair, is_a):
    assert not self.dataset.is_finalized
    if is_a:
      self.dataset.score_pair[self.id] = score_pair
    else:
      self.dataset.score_pair[self.id] = score_pair[::-1]


  #endregion agent API

class GamereplayDatasetElement(AbstractGamereplayDatasetElement):
  #region normalize/denormalize
  guesser_inputs_type_round_norm_factors = [5, 5]
  guesser_inputs_type_norm_factors = [10, 5]
  guesser_inputs_round_norm_factors = [9]
  guesser_inputs_single_norm_factors = [1]
  his_values_outputs_norm_factors = [10]

  @staticmethod
  def normalize_guesser_inputs(type_round_data, type_data, round_data, single_data):
    type_round_data = type_round_data / GamereplayDatasetElement.guesser_inputs_type_round_norm_factors
    type_data = type_data / GamereplayDatasetElement.guesser_inputs_type_norm_factors
    round_data = round_data / GamereplayDatasetElement.guesser_inputs_round_norm_factors
    single_data = single_data / GamereplayDatasetElement.guesser_inputs_single_norm_factors
    return type_round_data, type_data, round_data, single_data

  @staticmethod
  def normalize_his_values_outputs(his_values_outputs):
    return his_values_outputs / GamereplayDatasetElement.his_values_outputs_norm_factors

  @staticmethod
  def denormalize_guesser_inputs(normalized_type_round_data, normalized_type_data, normalized_round_data, normalized_single_data):
    type_round_data = normalized_type_round_data * GamereplayDatasetElement.guesser_inputs_type_round_norm_factors
    type_data = normalized_type_data * GamereplayDatasetElement.guesser_inputs_type_norm_factors
    round_data = normalized_round_data * GamereplayDatasetElement.guesser_inputs_round_norm_factors
    single_data = normalized_single_data * GamereplayDatasetElement.guesser_inputs_single_norm_factors
    return type_round_data, type_data, round_data, single_data

  @staticmethod
  def denormalize_his_values_outputs(normalized_his_values_outputs):
    return normalized_his_values_outputs * GamereplayDatasetElement.his_values_outputs_norm_factors
  #endregion normalize/denormalize


  def prepare_export_guesser_inputs(self, is_state, is_a, rounds_played_cap):
    #region prepare data
    if is_a:
      my_values = self.dataset.a_values[self.id]
    else: #b
      my_values = self.dataset.b_values[self.id]

    if is_state:
      if is_a:
        my_steps_as_i_see = np.array(self.dataset.a_steps_as_a_see[self.id])
        his_steps_as_i_see = self.dataset.counts[self.id] - np.array(self.dataset.b_steps_as_b_see[self.id])
      else: #b
        my_steps_as_i_see = np.array(self.dataset.b_steps_as_b_see[self.id])
        my_empty_steps_as_i_see = np.expand_dims(self.dataset.counts[self.id], 0) # one step as steps
        if len(my_steps_as_i_see) > 0:
          my_steps_as_i_see = np.concatenate([my_empty_steps_as_i_see, my_steps_as_i_see], axis=0) # add masked empty step at begining
        else:
          my_steps_as_i_see = my_empty_steps_as_i_see # only empty step
        his_steps_as_i_see = self.dataset.counts[self.id] - np.array(self.dataset.a_steps_as_a_see[self.id])
    else: #action
      if is_a:
        his_steps_as_i_see = np.array(self.dataset.counts[self.id]) - np.array(self.dataset.b_steps_as_b_see[self.id])
        his_empty_steps_as_i_see = np.expand_dims(np.zeros(self.dataset.counts[self.id].shape), 0) # one step as steps
        if len(his_steps_as_i_see) > 0:
          his_steps_as_i_see = np.concatenate([his_empty_steps_as_i_see, his_steps_as_i_see], axis=0) # add masked empty step at begining
        else:
          his_steps_as_i_see = his_empty_steps_as_i_see # only empty step
        my_steps_as_i_see = np.array(self.dataset.a_steps_as_a_see[self.id])
      else: #b
        his_steps_as_i_see = self.dataset.counts[self.id] - np.array(self.dataset.a_steps_as_a_see[self.id])
        my_steps_as_i_see = np.array(self.dataset.b_steps_as_b_see[self.id])

    #region fix empty shape
    if len(my_steps_as_i_see)==0:
      my_steps_as_i_see = np.zeros((0,3))
    if len(his_steps_as_i_see)==0:
      his_steps_as_i_see = np.zeros((0,3))
    #endregion fix empty shape

    rounds_played = min(rounds_played_cap, len(my_steps_as_i_see), len(his_steps_as_i_see))

    if is_state:
      if is_a:
        semi_ids_r2_shift = 2
      else: #b
        semi_ids_r2_shift = 1
    else: #action
      if is_a:
        semi_ids_r2_shift = 1
      else: #b
        semi_ids_r2_shift = 2

    #endregion prepare data

    return my_steps_as_i_see, \
           his_steps_as_i_see, \
           my_values, \
           semi_ids_r2_shift, \
           rounds_played


  def pack_export_guesser_inputs(self, is_state, is_a, my_steps_as_i_see, his_steps_as_i_see, my_values, semi_ids_r2_shift, rounds_played):

    semi_ids = np.arange(0 + semi_ids_r2_shift, rounds_played * 2 + semi_ids_r2_shift, 2) # TODO check

    my_steps_as_i_see = my_steps_as_i_see[:rounds_played]
    his_steps_as_i_see = his_steps_as_i_see[:rounds_played]

    my_steps_as_i_see_t = np.array(my_steps_as_i_see).T
    his_steps_as_i_see_t = np.array(his_steps_as_i_see).T

    my_steps_as_i_see_layer = np.expand_dims(my_steps_as_i_see_t, -1)
    his_steps_as_i_see_layer = np.expand_dims(his_steps_as_i_see_t, -1)
    my_values_layer = np.expand_dims(my_values, -1)
    counts_layer = np.expand_dims(self.dataset.counts[self.id], -1)
    semi_ids_layer = np.expand_dims(semi_ids, -1)
    is_a_layer = np.array([is_a * 1])

    if is_state:
      type_round_data = np.concatenate([my_steps_as_i_see_layer, his_steps_as_i_see_layer, ], axis=-1)
      type_round_empty_template = np.concatenate([counts_layer, np.zeros(counts_layer.shape)], axis=-1) #my_steps, #his_steps
    else: #action
      type_round_data = np.concatenate([his_steps_as_i_see_layer, my_steps_as_i_see_layer], axis=-1)
      type_round_empty_template = np.concatenate([np.zeros(counts_layer.shape), counts_layer], axis=-1) #his_steps, #my_steps
    type_data = np.concatenate([my_values_layer, counts_layer], axis=-1)
    round_data = semi_ids_layer
    round_empty_template = np.array([0])
    single_data = is_a_layer


    #region exrend to full width
    empty_rounds_dim = self.dataset.rounds_dim - rounds_played
    type_round_empty_template = np.expand_dims(type_round_empty_template, 1)
    type_round_empty_template = type_round_empty_template.repeat(empty_rounds_dim, axis = 1)

    round_empty_template = np.expand_dims(round_empty_template, 0)
    round_empty_template = round_empty_template.repeat(empty_rounds_dim, axis = 0)

    type_round_data = np.concatenate([type_round_empty_template, type_round_data], axis=1)
    round_data = np.concatenate([round_empty_template, round_data], axis=0)
    #endregion exrend to full width

    return type_round_data, \
           type_data, \
           round_data, \
           single_data


  def export_guesser_inputs(self, is_state, is_a, rounds_played_cap):
    my_steps_as_i_see, \
    his_steps_as_i_see, \
    my_values, \
    semi_ids_r2_shift, \
    rounds_played = self.prepare_export_guesser_inputs(is_state, is_a, rounds_played_cap)

    type_round_data, \
    type_data, \
    round_data, \
    single_data = self.pack_export_guesser_inputs( \
      is_state, \
      is_a, \
      my_steps_as_i_see, \
      his_steps_as_i_see, \
      my_values, \
      semi_ids_r2_shift, \
      rounds_played \
    )
    return GamereplayDatasetElement.normalize_guesser_inputs(type_round_data, type_data, round_data, single_data)
