
import numpy as np
import copy

from abstract_gamestateaction_dataset_element import AbstractGamestateactionDatasetElement
#### helper-class to put raw stat data into and export as unnormalized layers dims (types, rounds, channels) as input and (channels,) as output
class VGGamestateDatasetElement(AbstractGamestateactionDatasetElement):
    value_guesser_gamestate_inputs_norm_factors = [5, 5, 10, 5, 9, 1]
    value_guesser_gamestate_outputs_norm_factors = [10]
    @staticmethod
    def normalize_value_guesser_gamestate_inputs(value_guesser_gamestate_inputs):
        return value_guesser_gamestate_inputs / GamestateDatasetElement.value_guesser_gamestate_inputs_norm_factors
    @staticmethod
    def normalize_value_guesser_gamestate_outputs(value_guesser_gamestate_outputs):
        return value_guesser_gamestate_outputs / GamestateDatasetElement.value_guesser_gamestate_outputs_norm_factors
    @staticmethod
    def denormalize_value_guesser_gamestate_inputs(normalized_value_guesser_gamestate_inputs):
        return normalized_value_guesser_gamestate_inputs * GamestateDatasetElement.value_guesser_gamestate_inputs_norm_factors
    @staticmethod
    def denormalize_value_guesser_gamestate_outputs(normalized_value_guesser_gamestate_outputs):
        return normalized_value_guesser_gamestate_outputs * GamestateDatasetElement.value_guesser_gamestate_outputs_norm_factors

    def export_value_guesser_gamestate(self):
        cloned_dataset_element = copy.deepcopy(self)
        cloned_dataset_element.finalize_via_extend()
        l0 = np.expand_dims(cloned_dataset_element.my_steps_as_i_see, 0).T

        l1 = np.expand_dims(cloned_dataset_element.his_steps_as_i_see, 0).T

        l2 = np.expand_dims(cloned_dataset_element.my_values, -1).repeat(6, 1).T
        l2 = np.expand_dims(l2, 0).T

        l3 = np.expand_dims(cloned_dataset_element.counts, -1).repeat(6, 1).T
        l3 = np.expand_dims(l3, 0).T

        l4 = np.expand_dims(cloned_dataset_element.semi_ids, -1).repeat(3, 1)
        l4 = np.expand_dims(l4, 0).T

        l5 = np.expand_dims(cloned_dataset_element.is_a, -1).repeat(6)
        l5 = np.expand_dims(l5, -1).repeat(3, 1)
        l5 = np.expand_dims(l5, 0).T

        target = np.array(cloned_dataset_element.his_values)

        layers = np.concatenate([l0, l1, l2, l3, l4, l5], axis = -1)
        return GamestateDatasetElement.normalize_value_guesser_gamestate_inputs(layers), GamestateDatasetElement.normalize_value_guesser_gamestate_outputs(target)

class GamestateDatasetElement(VGGamestateDatasetElement):
    score_guesser_gamestate_inputs_norm_factors = [5, 5, 10, 5, 9, 1]
    score_guesser_gamestate_outputs_norm_factors = [10]
    @staticmethod
    def normalize_score_guesser_gamestate_inputs(score_guesser_gamestate_inputs):
        return score_guesser_gamestate_inputs / GamestateDatasetElement.score_guesser_gamestate_inputs_norm_factors
    @staticmethod
    def normalize_score_guesser_gamestate_outputs(score_guesser_gamestate_outputs):
        return score_guesser_gamestate_outputs / GamestateDatasetElement.score_guesser_gamestate_outputs_norm_factors
    @staticmethod
    def denormalize_score_guesser_gamestate_inputs(normalized_score_guesser_gamestate_inputs):
        return normalized_score_guesser_gamestate_inputs * GamestateDatasetElement.score_guesser_gamestate_inputs_norm_factors
    @staticmethod
    def denormalize_score_guesser_gamestate_outputs(normalized_score_guesser_gamestate_outputs):
        return normalized_score_guesser_gamestate_outputs * GamestateDatasetElement.score_guesser_gamestate_outputs_norm_factors

    def export_score_guesser_gamestate(self):
        cloned_dataset_element = copy.deepcopy(self)
        cloned_dataset_element.finalize_via_extend()
        l0 = np.expand_dims(cloned_dataset_element.my_steps_as_i_see, 0).T

        l1 = np.expand_dims(cloned_dataset_element.his_steps_as_i_see, 0).T

        l2 = np.expand_dims(cloned_dataset_element.my_values, -1).repeat(6, 1).T
        l2 = np.expand_dims(l2, 0).T

        l3 = np.expand_dims(cloned_dataset_element.counts, -1).repeat(6, 1).T
        l3 = np.expand_dims(l3, 0).T

        l4 = np.expand_dims(cloned_dataset_element.semi_ids, -1).repeat(3, 1)
        l4 = np.expand_dims(l4, 0).T

        l5 = np.expand_dims(cloned_dataset_element.is_a, -1).repeat(6)
        l5 = np.expand_dims(l5, -1).repeat(3, 1)
        l5 = np.expand_dims(l5, 0).T

        target = np.array(cloned_dataset_element.score_pair)

        layers = np.concatenate([l0, l1, l2, l3, l4, l5], axis = -1)
        return GamestateDatasetElement.normalize_score_guesser_gamestate_inputs(layers), GamestateDatasetElement.normalize_score_guesser_gamestate_outputs(target)
