import pathlib

import keras
from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Concatenate
from keras.layers import BatchNormalization, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Conv1D, Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer, Lambda, RepeatVector, Multiply
import matplotlib.pyplot as plt
import keras.backend as K
import tensorflow as tf
from keras.callbacks import TensorBoard

import numpy as np

from chop_dataset import chop_guesser_dataset
from enrich_dataset import enrich_dataset
##### Preinit
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
sess = tf.Session(config=config)
set_session(sess)  # set this TensorFlow session as the default session for Keras

class GamestateValueGuesser():

  def __init__(self, name, preload = False):
    self.name = name
    # Input shape
    self.types_dim = 3
    self.rounds_dim = 6
    self.channels_dim = 6 + 4 * 2 # 6 classic channel + 4 enrich (each is 2 output: sum and absdiff between type-neighourhoods (width are kind of swap-invariant))

    self.loss_aux_input = None
    self.supermodel = self.build_supermodel()
    self.loss_aux_input = self.supermodel.input
    def custom_loss(y_true, y_pred):
      counts_channel_id = 3
      counts = self.loss_aux_input[:, :, 0, counts_channel_id]

      denorm_10 = 10.0 # TODO denomalize via unhardcoded coefs
      denorm_5 = 5.0 # TODO denomalize via unhardcoded coefs
      denormalized_target_total_values_sums = 10.0
      denormalized_predicted_values = y_pred * denorm_10
      denormalized_counts = counts * denorm_5
      denormalized_predicted_total_values_sums = K.sum(denormalized_counts * denormalized_predicted_values, axis=-1) # 0 round, since counts_channel is rounds shared

      misum = K.abs(denormalized_predicted_total_values_sums - denormalized_target_total_values_sums) / denormalized_target_total_values_sums
      smisum = K.square(misum)

      misdig = K.abs((((denormalized_predicted_values % 1) + 1.5) % 1) - 0.5) # how each digit differs from rounded one, eg 7.1 => 0.1 penalty, 7.5 => 0.5 penalty, 7.9 => 0.1 penalty, 7.0 => 0.0 penalty
      smisdig = K.square(misdig)
      msmisdig = K.mean(smisdig, axis=-1)

      mse = K.mean(K.square(y_pred - y_true), axis=-1)

      msce = K.mean(K.square((y_pred - y_true) * counts * 3), axis=-1)

      return mse + msce + smisum / 3 + msmisdig / 33


    optimizer = Adam(lr = 0.01/1)
    #optimizer = Adam(lr = 0.01/2) #step 2

    self.supermodel.compile(loss='mse', metrics=['mse', 'mae'], optimizer=optimizer)
    #self.supermodel.compile(loss=custom_loss, metrics=['mse', 'mae'], optimizer=optimizer)
    if preload == True:
      self.load_models()

  def build_supermodel(self):
    #regularizer = keras.regularizers.l2(1e-5)

    supermodel_input = Input(shape=(self.types_dim, self.rounds_dim, self.channels_dim))
    x = supermodel_input

    x = Conv2D(24, kernel_size=(1, 1))(x) # ok with 12
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    #region reception
    x_ti = Conv2D(8, kernel_size=(1, 1))(x)
    x_ti = BatchNormalization()(x_ti)
    x_ti = Activation('elu')(x_ti)

    x_t = Conv2D(16, kernel_size=(self.types_dim, 1), padding='valid')(x_ti)
    x_t = BatchNormalization()(x_t)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    x_ri = Conv2D(8, kernel_size=(1, 1))(x)
    x_ri = BatchNormalization()(x_ri)
    x_ri = Activation('elu')(x_ri)

    x_r = Conv2D(16, kernel_size=(1, self.rounds_dim), padding='valid')(x_ri)
    x_r = BatchNormalization()(x_r)
    x_r = Activation('elu')(x_r)
    x_r = UpSampling2D(size=(1, self.rounds_dim))(x_r)

    x = Concatenate()([x, x_t, x_r])
    #endregion reception

    x = Conv2D(36, kernel_size=(1, 1))(x)  # ok with 24
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    #region reception
    x_ti = Conv2D(8, kernel_size=(1, 1))(x)
    x_ti = BatchNormalization()(x_ti)
    x_ti = Activation('elu')(x_ti)

    x_t = Conv2D(16, kernel_size=(self.types_dim, 1), padding='valid')(x_ti)
    x_t = BatchNormalization()(x_t)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    x_ri = Conv2D(8, kernel_size=(1, 1))(x)
    x_ri = BatchNormalization()(x_ri)
    x_ri = Activation('elu')(x_ri)

    x_r = Conv2D(16, kernel_size=(1, self.rounds_dim), padding='valid')(x_ri)
    x_r = BatchNormalization()(x_r)
    x_r = Activation('elu')(x_r)
    x_r = UpSampling2D(size=(1, self.rounds_dim))(x_r)

    x = Concatenate()([x, x_t, x_r])
    #endregion reception

    x = Conv2D(48, kernel_size=(1, 1))(x) # ok with 36
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    x = Conv2D(24, kernel_size=(1, 1))(x) # ok with 36
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    #region finception
    x_t = Conv2D(16, kernel_size=(self.types_dim, 1), padding='valid')(x)
    x_t = BatchNormalization()(x_t)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    #region diception in finception
    x = Concatenate()([x, x_t, supermodel_input])
    #endregion diception in finception

    x = Conv2D(16, kernel_size=(1, self.rounds_dim), padding='valid')(x)
    x = BatchNormalization()(x)
    x = Activation('elu')(x)
    #endregion finception

    x = Conv2D(1, kernel_size=(1, 1), padding='valid')(x)
    x = Activation('elu')(x)

    x = Flatten()(x)

    supermodel_output = x
    supermodel = Model(supermodel_input, supermodel_output)
    print('====| supermodel |====')
    supermodel.summary()
    return supermodel


  def train(self, epochs, batch_size=128, print_interval = 500, demonstrate_interval=500, save_interval=5000, tb_interval=100, start_epoch = 0):
    tb_log_path = './tb/' + self.name
    pathlib.Path(tb_log_path).mkdir(parents=True, exist_ok=True)
    tb_callback = TensorBoard(log_dir=tb_log_path, histogram_freq=5, batch_size=batch_size, write_graph=False)
    tb_callback.set_model(self.supermodel)

    smooth_loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)

    for epoch in range(start_epoch, epochs + start_epoch):


      # ---------------------
      #  Train supermodel
      # ---------------------

      # Train the supermodel (wants decoder to mistake images as real)
      loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)
      for complete_level in range(0, self.rounds_dim):
        assert len(self.chopped_normalized_enriched_game_inputs[complete_level]) == len(self.chopped_normalized_game_outputs[complete_level])

        idx = np.random.randint(0, self.chopped_normalized_enriched_game_inputs[complete_level].shape[0], batch_size)
        normalized_enriched_game_inputs_batch = self.chopped_normalized_enriched_game_inputs[complete_level][idx]
        normalized_game_outputs_batch = self.chopped_normalized_game_outputs[complete_level][idx]

        #region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
        dim_permutation = np.random.permutation(self.types_dim)
        normalized_enriched_game_inputs_batch = normalized_enriched_game_inputs_batch[:,dim_permutation]
        normalized_game_outputs_batch = normalized_game_outputs_batch[:,dim_permutation]
        #endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

        loss[complete_level] = np.array(self.supermodel.train_on_batch(normalized_enriched_game_inputs_batch, normalized_game_outputs_batch))

        if epoch % print_interval == 0:
          #print ('{} [CL{} TRN] [mse loss: {}] [mae metric: {}]'.format(epoch, complete_level, loss[complete_level][0], loss[complete_level][1]))
          print ('{} [CL{} TRN] [CUS: {}] [MSE: {}] [MAE: {}]'.format(epoch, complete_level, loss[complete_level][0], loss[complete_level][1], loss[complete_level][2]))

      smooth_loss = loss * 0.005 + smooth_loss * 0.995




      if epoch % tb_interval == 0:
        tb_loss = np.array([loss, smooth_loss]).flatten()
        #base_loss_names = ['MSE', 'MAE']
        base_loss_names = ['CUS', 'MSE', 'MAE']
        smooth_suffix = ['', '_smooth']
        complete_level_suffix = np.arange(0, self.rounds_dim)
        #train_test = ['TRN', 'TST']
        train_test = ['TRN']
        tb_loss_names = []
        for tt in train_test:
            for ss in smooth_suffix:
                for cl in complete_level_suffix:
                    for bln in base_loss_names:
                        loss_name = '[CL{} {}] {}{}'.format(cl, tt, bln, ss)
                        tb_loss_names.append(loss_name)

        self.write_tb_log(tb_callback, tb_loss_names, tb_loss, epoch)

      if epoch % demonstrate_interval == 0:
        self.demonstrate_progress(epoch)

      if epoch % save_interval == 0:
        self.save_models()


  def save_models(self):
    pathlib.Path('./saved_model/' + self.name).mkdir(parents=True, exist_ok=True)
    self.supermodel.save_weights('./saved_model/' + self.name + '/supermodel.h5')

  def demonstrate_progress(self, epoch):
    nf_10 = 10 # norm_factor TODO denormalize explicitly
    nf_5 = 5 # norm_factor TODO denormalize explicitly
    print(' == demo at epoch {} == '.format(epoch))
    prev_printoptions = np.get_printoptions()
    np.set_printoptions(precision=2)

    for game_num in range(0, 8):
      for is_a in (True, False):
        letter = 'A' if is_a else 'B'
        unchopped_normalized_game_inputs_one_as_dataset = np.expand_dims(self.normalized_game_inputs[game_num * 2 + (not is_a) * 1], axis = 0)
        unchopped_normalized_game_outputs_one_as_dataset = np.expand_dims(self.normalized_game_outputs[game_num * 2 + (not is_a) * 1], axis = 0)
        normalized_counts = unchopped_normalized_game_inputs_one_as_dataset[0,:,0,3]
        print('game#{} {} counts is {}, ground-truth is {}'.format(game_num, letter, normalized_counts * nf_5, unchopped_normalized_game_outputs_one_as_dataset * nf_10))

        chopped_normalized_game_inputs_one_as_dataset, chopped_normalized_game_outputs_one_as_dataset = chop_guesser_dataset(unchopped_normalized_game_inputs_one_as_dataset, unchopped_normalized_game_outputs_one_as_dataset, as_gameaction=False)

        for complete_level in range(0, self.rounds_dim):
          normalized_game_inputs_one_as_batch = chopped_normalized_game_inputs_one_as_dataset[complete_level]
          normalized_game_outputs_one_as_batch = chopped_normalized_game_outputs_one_as_dataset[complete_level]
          if len(normalized_game_inputs_one_as_batch) == 0:
            print ('early end of game#{}'.format(game_num))
            break
          else:
            normalized_enriched_game_inputs_one_as_batch = enrich_dataset(normalized_game_inputs_one_as_batch)
            normalized_game_predictions_one_as_batch = self.supermodel.predict(normalized_enriched_game_inputs_one_as_batch)

            ae = np.abs(normalized_game_outputs_one_as_batch - normalized_game_predictions_one_as_batch)
            total_values_sum = np.sum(normalized_game_predictions_one_as_batch * nf_10 * normalized_counts * nf_5) # one nf_5 for counts, one more nf_10 for normalized_game_predictions
            print ('game#{} {} [CL{}] prediction: {} (sum {:.1f}), absolute error: {} (mean {:.1f})'.format(game_num, letter, complete_level, normalized_game_predictions_one_as_batch * nf_10, total_values_sum, ae * nf_10, ae.mean() * nf_10))
    np.set_printoptions(prev_printoptions)

  def load_models(self):
    self.supermodel.load_weights('./saved_model/' + self.name + '/supermodel.h5')

  def write_tb_log(self, callback, names, logs, batch_no):
    for name, value in zip(names, logs):
      summary = tf.Summary()
      summary_value = summary.value.add()
      summary_value.simple_value = value
      summary_value.tag = name
      callback.writer.add_summary(summary, batch_no)
      callback.writer.flush()

  def create_raw_dataset(self):
    tour_name = 'v2_huge_tour_50k'

    normalized_game_inputs_path = 'stat_tournament/' + tour_name + '_to_value_guesser_gamestate_dataset_inputs.npy'
    normalized_game_outputs_path = 'stat_tournament/' + tour_name + '_to_value_guesser_gamestate_dataset_outputs.npy'
    self.normalized_game_inputs = np.load(normalized_game_inputs_path)
    self.normalized_game_outputs = np.load(normalized_game_outputs_path)
    #region blind mode
    #self.normalized_game_inputs = np.random.uniform(0, 1, self.normalized_game_inputs.shape)
    #endregion blind mode
    assert len(self.normalized_game_inputs) == len(self.normalized_game_outputs)

    self.chopped_normalized_game_inputs, self.chopped_normalized_game_outputs = chop_guesser_dataset(self.normalized_game_inputs, self.normalized_game_outputs, as_gameaction=False)

    self.chopped_normalized_enriched_game_inputs = []
    for complete_level in range(0, len(self.chopped_normalized_game_inputs)):
      normalized_enriched_game_inputs = enrich_dataset(self.chopped_normalized_game_inputs[complete_level])
      self.chopped_normalized_enriched_game_inputs.append(normalized_enriched_game_inputs)


if __name__ == '__main__':
  value_guesser = GamestateValueGuesser(name = 'gamestate_value_guesser_ba_try', preload = False)
  value_guesser.create_raw_dataset()
  value_guesser.train(epochs=5*1000*1000, start_epoch = 0*1000)
