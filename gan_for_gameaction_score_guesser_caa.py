import pathlib

import keras
from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Concatenate
from keras.layers import BatchNormalization, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Conv1D, Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer, Lambda, RepeatVector, Multiply
import matplotlib.pyplot as plt
import keras.backend as K
import tensorflow as tf
from keras.callbacks import TensorBoard

import numpy as np

from chop_dataset import chop_guesser_dataset
from enrich_dataset import enrich_dataset

from gameaction_score_guesser_caa import GameactionScoreGuesser

##### Preinit
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
sess = tf.Session(config=config)
set_session(sess)  # set this TensorFlow session as the default session for Keras


class GanForGameactionScoreGuesser():

  def __init__(self, name, score_guesser_name, batch_size=128, preload = False):
    self.name = name + '_for_' + score_guesser_name
    self.batch_size = batch_size
    # Input shape
    self.types_dim = 3
    self.rounds_dim = 6
    self.channels_dim = 6 + 4 * 2 # 6 classic channel + 4 enrich (each is 2 output: sum and absdiff between type-neighourhoods (width are kind of swap-invariant))
    self.scores_dim = 2

    #self.loss_aux_input = None

    self.generator = self.build_generator()

    self.score_guesser = GameactionScoreGuesser(score_guesser_name, preload=True)
    self.score_guesser.supermodel.trainable = False

    self.supermodel = self.build_supermodel(self.generator, self.score_guesser.supermodel)



    #self.loss_aux_input = self.supermodel.input

    optimizer = Adam(lr = 0.01/1)
    def weighted_mse(y_true, y_pred):
      return K.mean(K.square(y_pred - y_true) * [1.8, 0.1], axis=-1)

    #self.supermodel.compile(loss='mse', metrics=['mse', 'mae'], optimizer=optimizer)
    self.supermodel.compile(loss=weighted_mse, metrics=['mse', 'mae'], optimizer=optimizer)

    if preload == True:
      self.load_models()

  def build_supermodel(self, generator, score_guesser):
    supermodel_input = Input(shape=(self.types_dim, self.rounds_dim, self.channels_dim))
    x = supermodel_input

    #region azaza
    def kill_my_last_step(supermodel_input):
        mask = np.zeros(shape=(self.batch_size, self.types_dim, self.rounds_dim, self.channels_dim))
        mask[:, :, 5, 1 + 6 * 0 + 4 * 0] = np.ones(shape=(self.batch_size, self.types_dim))
        mask[:, :, 5, 1 + 6 * 1 + 4 * 0] = np.ones(shape=(self.batch_size, self.types_dim))
        mask[:, :, 5, 1 + 6 * 1 + 4 * 1] = np.ones(shape=(self.batch_size, self.types_dim))
        i = supermodel_input * (1.0 - mask)
        r = K.random_normal(K.shape(supermodel_input)) * mask
        x = i + r
        return x

    def weld_generated_last_step(inputs):
        supermodel_input = inputs[0]
        generated_last_step_sigmoid = inputs[1]
        counts_per_types = supermodel_input[:, :, 0, 3] #get from 0 round as any, since counts is rounds-shared
        generated_last_step = generated_last_step_sigmoid * counts_per_types

        #region enrich via tensorflow
        mask_step = np.zeros(shape=(self.batch_size, self.types_dim, self.rounds_dim, self.channels_dim))
        mask_step[:, :, 5, 1 + 6 * 0 + 4 * 0] = np.ones(shape=(self.batch_size, self.types_dim))
        mask_sum = np.zeros(shape=(self.batch_size, self.types_dim, self.rounds_dim, self.channels_dim))
        mask_sum[:, :, 5, 1 + 6 * 1 + 4 * 0] = np.ones(shape=(self.batch_size, self.types_dim))
        mask_absdiff = np.zeros(shape=(self.batch_size, self.types_dim, self.rounds_dim, self.channels_dim))
        mask_absdiff[:, :, 5, 1 + 6 * 1 + 4 * 1] = np.ones(shape=(self.batch_size, self.types_dim))
        mask_any = np.any([mask_step, mask_sum, mask_absdiff])

        generated_last_step_a = tf.manip.roll(generated_last_step,shift=1, axis=1) # axis types
        generated_last_step_b = tf.manip.roll(generated_last_step,shift=-1, axis=1) # axis types

        generated_last_step_sum = (generated_last_step_a + generated_last_step_b)
        generated_last_step_absdiff = np.abs(generated_last_step_a - generated_last_step_b)

        step = K.expand_dims(generated_last_step, axis=-1)
        step = K.repeat_elements(step, rep=self.rounds_dim,axis=-1)
        step = K.expand_dims(step, axis=-1)
        step = K.repeat_elements(step, rep=self.channels_dim,axis=-1)

        step_sum = K.expand_dims(generated_last_step_sum, axis=-1)
        step_sum = K.repeat_elements(step_sum, rep=self.rounds_dim,axis=-1)
        step_sum = K.expand_dims(step_sum, axis=-1)
        step_sum = K.repeat_elements(step_sum, rep=self.channels_dim,axis=-1)

        step_absdiff = K.expand_dims(generated_last_step_absdiff, axis=-1)
        step_absdiff = K.repeat_elements(step_absdiff, rep=self.rounds_dim,axis=-1)
        step_absdiff = K.expand_dims(step_absdiff, axis=-1)
        step_absdiff = K.repeat_elements(step_absdiff, rep=self.channels_dim,axis=-1)

        i = supermodel_input * (1 - mask_any)
        s = step * mask_step + step_sum * mask_sum + step_absdiff * mask_absdiff
        x = i + s
        #endregion enrich via tensorflow
        return x
    #endregion azaza

    supermodel_input_with_killed_last_step = Lambda(kill_my_last_step)(supermodel_input)
    generator_output = generator(supermodel_input_with_killed_last_step)
    generated_gameaction = Lambda(weld_generated_last_step)([supermodel_input, generator_output])
    x = score_guesser(generated_gameaction)

    supermodel_output = x
    supermodel = Model(supermodel_input, supermodel_output)
    print('====| supermodel |====')
    supermodel.summary()
    return supermodel





  def build_generator(self):
    #regularizer = keras.regularizers.l2(1e-5)
    multiplier_alpha = 1
    multiplier_beta = 1
    generator_input = Input(shape=(self.types_dim, self.rounds_dim, self.channels_dim))
    x = generator_input

    x = Conv2D(24 * multiplier_alpha, kernel_size=(1, 1))(x) # ok with 12
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    #region reception
    x_ti = Conv2D(8 * multiplier_beta, kernel_size=(1, 1))(x)
    x_ti = BatchNormalization()(x_ti)
    x_ti = Activation('elu')(x_ti)

    x_t = Conv2D(16 * multiplier_beta, kernel_size=(self.types_dim, 1), padding='valid')(x_ti)
    x_t = BatchNormalization()(x_t)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    x_ri = Conv2D(8 * multiplier_beta, kernel_size=(1, 1))(x)
    x_ri = BatchNormalization()(x_ri)
    x_ri = Activation('elu')(x_ri)

    x_r = Conv2D(16 * multiplier_beta, kernel_size=(1, self.rounds_dim), padding='valid')(x_ri)
    x_r = BatchNormalization()(x_r)
    x_r = Activation('elu')(x_r)
    x_r = UpSampling2D(size=(1, self.rounds_dim))(x_r)

    x = Concatenate()([x, x_t, x_r])
    #endregion reception

    x = Conv2D(36 * multiplier_beta, kernel_size=(1, 1))(x)  # ok with 24
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    #region reception
    x_ti = Conv2D(8 * multiplier_beta, kernel_size=(1, 1))(x)
    x_ti = BatchNormalization()(x_ti)
    x_ti = Activation('elu')(x_ti)

    x_t = Conv2D(16 * multiplier_beta, kernel_size=(self.types_dim, 1), padding='valid')(x_ti)
    x_t = BatchNormalization()(x_t)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    x_ri = Conv2D(8 * multiplier_beta, kernel_size=(1, 1))(x)
    x_ri = BatchNormalization()(x_ri)
    x_ri = Activation('elu')(x_ri)

    x_r = Conv2D(16 * multiplier_beta, kernel_size=(1, self.rounds_dim), padding='valid')(x_ri)
    x_r = BatchNormalization()(x_r)
    x_r = Activation('elu')(x_r)
    x_r = UpSampling2D(size=(1, self.rounds_dim))(x_r)

    x = Concatenate()([x, x_t, x_r])
    #endregion reception

    x = Conv2D(48 * multiplier_beta, kernel_size=(1, 1))(x) # ok with 36
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    x = Conv2D(24 * multiplier_beta, kernel_size=(1, 1))(x) # ok with 36
    x = BatchNormalization()(x)
    x = Activation('elu')(x)

    #region finception
    x_t = Conv2D(16 * multiplier_beta, kernel_size=(self.types_dim, 1), padding='valid')(x)
    x_t = BatchNormalization()(x_t)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    #region diception in finception
    x = Concatenate()([x, x_t, generator_input])
    #endregion diception in finception

    x = Conv2D(16 * multiplier_alpha, kernel_size=(1, self.rounds_dim), padding='valid')(x)
    x = BatchNormalization()(x)
    x = Activation('elu')(x)
    #endregion finception

    x = Conv2D(1, kernel_size=(1, 1), padding='valid')(x)
    x = Activation('sigmoid')(x)

    x = Flatten()(x)

    generator_output = x
    generator = Model(generator_input, generator_output)
    print('====| generator |====')
    generator.summary()
    return generator


  def train(self, epochs, print_interval = 500, demonstrate_interval=500, save_interval=1000, tb_interval=100, start_epoch = 0):
    tb_log_path = './tb/' + self.name
    pathlib.Path(tb_log_path).mkdir(parents=True, exist_ok=True)
    tb_callback = TensorBoard(log_dir=tb_log_path, histogram_freq=5, batch_size=self.batch_size, write_graph=False)
    tb_callback.set_model(self.supermodel)



    smooth_loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)

    for epoch in range(start_epoch, epochs + start_epoch):


      # ---------------------
      #  Train supermodel
      # ---------------------

      # Train the supermodel (wants decoder to mistake images as real)
      loss = np.array([[0.0, 0.0, 0.0]]).repeat(self.rounds_dim, axis=0)
      for complete_level in range(0, self.rounds_dim - 1): # -1 because of last chop-bin for actions is always empty
        assert len(self.chopped_normalized_enriched_game_inputs[complete_level]) == len(self.chopped_normalized_game_outputs[complete_level])

        idx = np.random.randint(0, self.chopped_normalized_enriched_game_inputs[complete_level].shape[0], self.batch_size)
        normalized_enriched_game_inputs_batch = self.chopped_normalized_enriched_game_inputs[complete_level][idx]
        normalized_game_outputs_batch = self.chopped_normalized_game_outputs[complete_level][idx]

        #region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
        dim_permutation = np.random.permutation(self.types_dim)
        normalized_enriched_game_inputs_batch = normalized_enriched_game_inputs_batch[:,dim_permutation]
        #endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

        loss[complete_level] = np.array(self.supermodel.train_on_batch(normalized_enriched_game_inputs_batch, normalized_game_outputs_batch))
        if loss[complete_level][0] < 2e-3 and complete_level == 0:
          print ('stange lo at CL{}: loss {} for out {}'.format(complete_level, loss[complete_level], normalized_game_outputs_batch))
        if epoch % print_interval == 0:
          #print ('{} [CL{} TRN] [mse loss: {}] [mae metric: {}]'.format(epoch, complete_level, loss[complete_level][0], loss[complete_level][1]))
          print ('{} [CL{} TRN] [CUS: {}] [MSE: {}] [MAE: {}]'.format(epoch, complete_level, loss[complete_level][0], loss[complete_level][1], loss[complete_level][2]))

      smooth_loss = loss * 0.005 + smooth_loss * 0.995




      if epoch % tb_interval == 0:
        tb_loss = np.array([loss, smooth_loss]).flatten()
        #base_loss_names = ['MSE', 'MAE']
        base_loss_names = ['CUS', 'MSE', 'MAE']
        smooth_suffix = ['', '_smooth']
        complete_level_suffix = np.arange(0, self.rounds_dim)
        #train_test = ['TRN', 'TST']
        train_test = ['TRN']
        tb_loss_names = []
        for tt in train_test:
            for ss in smooth_suffix:
                for cl in complete_level_suffix:
                    for bln in base_loss_names:
                        loss_name = '[CL{} {}] {}{}'.format(cl, tt, bln, ss)
                        tb_loss_names.append(loss_name)

        self.write_tb_log(tb_callback, tb_loss_names, tb_loss, epoch)

      if epoch % demonstrate_interval == 0:
        self.demonstrate_progress(epoch)

      if epoch % save_interval == 0:
        pass
        self.save_models()


  def save_models(self):
    pathlib.Path('./saved_model/' + self.name).mkdir(parents=True, exist_ok=True)
    self.supermodel.save_weights('./saved_model/' + self.name + '/supermodel.h5')
    self.generator.save_weights('./saved_model/' + self.name + '/generator.h5')

  def demonstrate_progress(self, epoch):
    nf_10 = 10 # norm_factor TODO denormalize explicitly
    nf_5 = 5 # norm_factor TODO denormalize explicitly
    print(' == demo at epoch {} == '.format(epoch))
    prev_printoptions = np.get_printoptions()
    #np.set_printoptions(precision=2)

    for game_num in range(0, 8):
      for is_a in (True, False):
        letter = 'A' if is_a else 'B'
        unchopped_normalized_game_inputs_one_as_dataset = np.expand_dims(self.normalized_game_inputs[game_num * 2 + (not is_a) * 1], axis = 0)
        unchopped_normalized_game_outputs_one_as_dataset = np.expand_dims(self.normalized_game_outputs[game_num * 2 + (not is_a) * 1], axis = 0)
        normalized_counts = unchopped_normalized_game_inputs_one_as_dataset[0,:,0,3]
        print('game#{} {} counts is {}, ground-truth is {}'.format(game_num, letter, normalized_counts * nf_5, unchopped_normalized_game_outputs_one_as_dataset * nf_10))

        chopped_normalized_game_inputs_one_as_dataset, chopped_normalized_game_outputs_one_as_dataset = chop_guesser_dataset(unchopped_normalized_game_inputs_one_as_dataset, unchopped_normalized_game_outputs_one_as_dataset, as_gameaction=True)

        for complete_level in range(0, self.rounds_dim - 1): # -1 because of last chop-bin for actions is always empty
          normalized_game_inputs_one_as_batch = chopped_normalized_game_inputs_one_as_dataset[complete_level]
          normalized_game_outputs_one_as_batch = chopped_normalized_game_outputs_one_as_dataset[complete_level]
          if len(normalized_game_inputs_one_as_batch) == 0:
            print ('early end of game#{}'.format(game_num))
            break
          else:
            normalized_enriched_game_inputs_one_as_batch = enrich_dataset(normalized_game_inputs_one_as_batch)
            normalized_game_predictions_one_as_batch = self.generator.predict(normalized_enriched_game_inputs_one_as_batch)


            print ('game#{} {} [CL{}] prediction: {} (chk counts {})'.format(game_num, letter, complete_level, normalized_game_predictions_one_as_batch * normalized_enriched_game_inputs_one_as_batch[0,:,0,3] * nf_5, normalized_enriched_game_inputs_one_as_batch[0,:,0,3] * nf_5))
    #np.set_printoptions(prev_printoptions)

  def load_models(self):
    self.supermodel.load_weights('./saved_model/' + self.name + '/supermodel.h5')
    self.generator.load_weights('./saved_model/' + self.name + '/generator.h5')

  def write_tb_log(self, callback, names, logs, batch_no):
    for name, value in zip(names, logs):
      summary = tf.Summary()
      summary_value = summary.value.add()
      summary_value.simple_value = value
      summary_value.tag = name
      callback.writer.add_summary(summary, batch_no)
      callback.writer.flush()

  def create_raw_dataset(self):
    tour_name = 'v3.5_chap50_tour_500k'

    normalized_game_inputs_path = 'stat_tournament/' + tour_name + '_to_score_guesser_gameaction_dataset_inputs.npy'
    normalized_game_outputs_path = 'stat_tournament/' + tour_name + '_to_score_guesser_gameaction_dataset_outputs.npy'
    self.normalized_game_inputs = np.load(normalized_game_inputs_path)
    self.normalized_game_outputs = np.load(normalized_game_outputs_path)

    #region ground truth
    self.normalized_game_outputs[:,0] = 1 # my score = 10
    self.normalized_game_outputs[:,1] = 0 # his score = 0
    #endregion ground truth

    #region blind mode
    #self.normalized_game_inputs = np.random.uniform(0, 1, self.normalized_game_inputs.shape)
    #endregion blind mode
    assert len(self.normalized_game_inputs) == len(self.normalized_game_outputs)

    self.chopped_normalized_game_inputs, self.chopped_normalized_game_outputs = chop_guesser_dataset(self.normalized_game_inputs, self.normalized_game_outputs, as_gameaction=True)


    self.chopped_normalized_enriched_game_inputs = []
    for complete_level in range(0, len(self.chopped_normalized_game_inputs) - 1): # -1 because of last chop-bin for actions is always empty
      normalized_enriched_game_inputs = enrich_dataset(self.chopped_normalized_game_inputs[complete_level])
      self.chopped_normalized_enriched_game_inputs.append(normalized_enriched_game_inputs)
  def predict(self, gameaction_dataset_element):

    layers, _ = gameaction_dataset_element.export_score_guesser_gameaction()
    normalized_one_as_inputs = np.array([layers])
    normalized_enriched_one_as_inputs = enrich_dataset(normalized_one_as_inputs)
    #TODO
    ##region extra permute augmentation to types-vise convolution be more robust and ivariant to swap
    #dim_permutation = np.random.permutation(self.types_dim)
    #normalized_enriched_game_inputs_batch = normalized_enriched_game_inputs_batch[:,dim_permutation]
    ##endregion extra permute augmentation to types-vise convolution be more robust and ivariant to swap

    return self.supermodel.predict(normalized_enriched_one_as_inputs)

if __name__ == '__main__':
  gan_for_gameaction_score_guesser = GanForGameactionScoreGuesser(name='gan_aaa_fixgo0', score_guesser_name = 'gameaction_score_guesser_caa', batch_size= 128 * 2, preload = False)
  gan_for_gameaction_score_guesser.create_raw_dataset()
  gan_for_gameaction_score_guesser.train(epochs=5*1000*1000, start_epoch = 0*1000)
