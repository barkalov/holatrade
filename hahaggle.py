import numpy as np
import matplotlib.pyplot as plt
import pathlib
import json

from gamereplay_dataset_element import GamereplayDataset
class Hahaggle:
    def __init__(self, Agents, name):
        def do_nothing(*agrs, **kwargs):
            pass
        self.Agents = Agents
        self.name = name
        self.max_rounds = 5
        self.round = 0
        self.types_dim = 3
        self.rounds_dim = 6
        self.target_total_values_sum = 10

        pathlib.Path('./stat_tournament/').mkdir(parents=True, exist_ok=True)
        log_file =  open('stat_tournament/'+ self.name+'.txt', 'w')
        self.log0 = lambda *args: print('0 |', *args, file = log_file)
        self.log1 = lambda *args: print('1 |', *args, file = log_file)
        self.log2 = lambda *args: print('2 |', *args, file = log_file)
        #self.log0 = do_nothing
        #self.log1 = do_nothing
        #self.log2 = do_nothing
        self.log3 = lambda *args: print('3 |', *args, file = log_file)
        self.log4 = lambda *args: print('4 |', *args, file = log_file)

        self.max_counts_unshuffled = np.array([4,3,2])
        self.gamereplay_dataset_a = None
        self.gamereplay_dataset_b = None

        with open('./reference_generator.json') as f:
            self.reference_generator = json.load(f)

    def generate_good_inits(self):
        countsId = np.random.randint(0, len(self.reference_generator))
        countsData = self.reference_generator[countsId]
        counts = countsData['counts']
        valuations = countsData['valuations']
        valuationsAId = np.random.randint(0, len(valuations))
        valuationsBId = np.random.randint(0, len(valuations))
        valuesA = valuations[valuationsAId]
        valuesB = valuations[valuationsBId]
        return np.array(counts), [np.array(valuesA), np.array(valuesB)]



    def play_round(self, agent_a, agent_b, current_round, current_offer_result):
        assert np.all(agent_a.counts == agent_b.counts)
        counts = agent_a.counts
        #region dirty hack
        if current_offer_result is None:
            current_offer_result = np.zeros(counts.shape)
        #endregion dirty hack

        if current_round >= self.max_rounds:
            self.log2('current_round exeed max_rounds')
            # TODO debug and check values manually!
            agent_a.timeout()
            agent_b.timeout()
            agent_a.end_of_game([0, 0])
            agent_b.end_of_game([0, 0])
            #region dirty hack
            if current_offer_result.sum() == 0:
                current_offer_result = None
            #endregion dirty hack
            return [0, 0], current_round, current_offer_result
        else:
            current_round +=1
            self.log2('== playing round {} =='.format(current_round))

            if current_round == 1:
                self.log2('[{} {}] will innitially be offered with {}'.format('A', agent_a.name, current_offer_result))

            new_offer_result = agent_a.offer(current_offer_result)
            if new_offer_result is None:
                self.log2('[{} {}] accepted offer'.format('A', agent_a.name))
                a_score = np.sum(current_offer_result * agent_a.values)
                b_score = np.sum((counts - current_offer_result) * agent_b.values)

                agent_a.acceptance()
                agent_b.acceptance()
                agent_a.end_of_game([a_score, b_score])
                agent_b.end_of_game([b_score, a_score])
                #region dirty hack
                if current_offer_result.sum() == 0:
                    current_offer_result = None
                #endregion dirty hack
                return [a_score, b_score], current_round, current_offer_result
            else:
                current_offer_result = new_offer_result
                self.log2('[{} {}] rejected offer and answered with {} which is valuable for him as {}'.format('A', agent_a.name, current_offer_result, np.sum(current_offer_result * agent_a.values)))

            current_offer_result = counts - current_offer_result

            self.log2('[{} {}] will be offered with {} which is valuable for him as {}'.format('B', agent_b.name, current_offer_result, np.sum(current_offer_result * agent_b.values)))

            new_offer_result = agent_b.offer(current_offer_result)
            if new_offer_result is None:
                self.log2('[{} {}] accepted offer'.format('B', agent_b.name))
                b_score = np.sum(current_offer_result * agent_b.values)
                a_score = np.sum((counts - current_offer_result) * agent_a.values)

                agent_a.acceptance()
                agent_b.acceptance()
                agent_a.end_of_game([a_score, b_score])
                agent_b.end_of_game([b_score, a_score])
                #region dirty hack
                if current_offer_result.sum() == 0:
                    current_offer_result = None
                #endregion dirty hack
                return [a_score, b_score], current_round, current_offer_result
            else:
                current_offer_result = new_offer_result
                self.log2('[{} {}] rejected offer and answered with {} which is valuable for him as {}'.format('B', agent_b.name, current_offer_result, np.sum(current_offer_result * agent_b.values)))

            current_offer_result = counts - current_offer_result

            self.log2('[{} {}] will be offered with {} which is valuable for him as {}'.format('A', agent_a.name, current_offer_result, np.sum(current_offer_result * agent_a.values)))
            #region dirty hack
            if current_offer_result.sum() == 0:
                current_offer_result = None
            #endregion dirty hack
            return None, current_round, current_offer_result
    def play_game(self, agent_a_id, agent_b_id, game_num):
        self.log3('== playing game {} =='.format(game_num))
        Agent_a = self.Agents[agent_a_id]
        Agent_b = self.Agents[agent_b_id]

        game_counts, [game_agent_a_values, game_agent_b_values] = self.generate_good_inits()
        game_agent_a = Agent_a(me = 0, counts = game_counts, values=game_agent_a_values, max_rounds = self.max_rounds, log = self.log1, cheat_opponent_values = game_agent_b_values, gamereplay_dataset = self.gamereplay_dataset_a, gamereplay_dataset_id = game_num)
        game_agent_b = Agent_b(me = 1, counts = game_counts, values=game_agent_b_values, max_rounds = self.max_rounds, log = self.log1, cheat_opponent_values = game_agent_a_values, gamereplay_dataset = self.gamereplay_dataset_b, gamereplay_dataset_id = game_num)
        game_agent_a.name += '#{}'.format(agent_a_id)
        game_agent_b.name += '#{}'.format(agent_b_id)
        current_round = 0
        current_offer_result = None
        current_round_result = None
        self.log3('{} vs {}'.format(game_agent_a.name, game_agent_b.name))

        stat_game_counts = game_counts
        self.log3('game_counts', game_counts)
        assert np.all(game_counts>0)

        stat_game_values = [game_agent_a_values, game_agent_b_values]
        while current_round_result is None:
            current_round_result, current_round, current_offer_result = self.play_round(game_agent_a, game_agent_b, current_round, current_offer_result)
        self.log3('end of game with scores {}'.format(current_round_result))
        stat_game_result = current_round_result

        #stat_game_gamestate_dataset_elements_pair = [game_agent_a.gamestate_dataset_element, game_agent_b.gamestate_dataset_element]
        stat_game_gamestate_dataset_elements_pair = [None, None]
        stat_game_gameaction_dataset_elements_pair = [game_agent_a.gameaction_dataset_element, game_agent_b.gameaction_dataset_element]

        stat_game = [stat_game_counts, stat_game_values, stat_game_gamestate_dataset_elements_pair, stat_game_gameaction_dataset_elements_pair, stat_game_result]
        return current_round_result, stat_game
    def play_tournament(self, games_count = 1000, hot_ids = None):
        self.log4('=== playing tournament ===')
        if hot_ids is not None:
            self.log4('hot vs all mode')

        self.gamereplay_dataset_a = GamereplayDataset(self.types_dim, self.rounds_dim, games_count)
        self.gamereplay_dataset_b = GamereplayDataset(self.types_dim, self.rounds_dim, games_count)

        tournament_score_sums = np.zeros((len(self.Agents), len(self.Agents)))
        tournament_games_played = np.zeros((len(self.Agents), len(self.Agents)))

        tournament_progress_score_means = []
        stat_tournament = []
        agent_names = np.array(list(map(lambda Agent: Agent.agent_name, self.Agents)))

        for game_num in range(0, games_count):
            if hot_ids is None:
                agent_a_id = np.random.randint(0, len(self.Agents))
                agent_b_id = np.random.randint(0, len(self.Agents))
            else:
                #region multi-id support
                if hasattr(hot_ids, "__len__") > 0:
                    one_hot_id = np.random.choice(hot_ids)
                else:
                    one_hot_id = hot_ids
                #endregion multi-id support

                if np.random.uniform() < 0.5:
                    agent_a_id = one_hot_id
                    agent_b_id = np.random.randint(0, len(self.Agents))
                else:
                    agent_a_id = np.random.randint(0, len(self.Agents))
                    agent_b_id = one_hot_id

            game_scores, stat_game = self.play_game(agent_a_id, agent_b_id, game_num)
            stat_tournament.append(stat_game)
            tournament_score_sums[agent_a_id, agent_b_id] += game_scores[0]
            tournament_score_sums[agent_b_id, agent_a_id] += game_scores[1]
            tournament_games_played[agent_a_id, agent_b_id] += 1
            tournament_games_played[agent_b_id, agent_a_id] += 1
            add_to_progress_interval = max(100, int(games_count / 100))
            if game_num % add_to_progress_interval == 0:
                tournament_progress_score_means.append(tournament_score_sums / tournament_games_played)
                self.plot_tournament_progress(tournament_progress_score_means, agent_names)



        agent_ids = np.arange(0, len(self.Agents))
        qute_table = np.array([agent_names, agent_ids, tournament_score_sums.sum(axis=1)]).T
        anti_scores_column = -qute_table[:,2].astype(float)
        qute_table = qute_table[anti_scores_column.argsort()]
        self.log4('end of tournament with scores:')
        for qute_line in qute_table:
            self.log4('{}#{}: {}'.format(qute_line[0], qute_line[1], qute_line[2]))
        # assert np.all(np.array(self.gamereplay_dataset_a.a_steps_as_a_see) == np.array(self.gamereplay_dataset_b.a_steps_as_a_see))
        # assert np.all(np.array(self.gamereplay_dataset_a.b_steps_as_b_see) == np.array(self.gamereplay_dataset_b.b_steps_as_b_see))
        # assert np.all(np.array(self.gamereplay_dataset_a.a_values) == np.array(self.gamereplay_dataset_b.a_values))
        # assert np.all(np.array(self.gamereplay_dataset_a.b_values) == np.array(self.gamereplay_dataset_b.b_values))
        # assert np.all(np.array(self.gamereplay_dataset_a.counts) == np.array(self.gamereplay_dataset_b.counts))
        # assert np.all(np.array(self.gamereplay_dataset_a.score_pair) == np.array(self.gamereplay_dataset_b.score_pair))
        return qute_table, stat_tournament, self.gamereplay_dataset_a



    def plot_tournament_progress(self, tournament_progress_score_means, agent_names):
        #plot_filename = self.name + '_tournament_progress.png'
        plot_filename = self.name + '_tournament_progress_zoomed.png'
        #fig = plt.figure(figsize=(16, 160))
        #fig = plt.figure(figsize=(16, 48 ))
        #fig = plt.figure(figsize=(16, 32 ))
        fig = plt.figure(figsize=(16, 128 ))

        line_widths = np.ones(agent_names.shape) * 0.75
        line_styles = np.array(['-'] * len(agent_names))
        line_widths[0:2] = 2
        line_widths[2] = 4
        line_styles[3] = ':'
        transposed_plots = np.array(tournament_progress_score_means).T

        ######
        for i in range(0, transposed_plots.shape[0]): # vs
            plt.subplot(transposed_plots.shape[0] + 2, 2, i * 2 + 1)
            for j in range(0, transposed_plots.shape[1]): # me
                tournament_progress_score_mean = transposed_plots[i][j]
                line_width = line_widths[j]
                line_style = line_styles[j]
                plt.plot(tournament_progress_score_mean, linewidth = line_width, linestyle = line_style)

            plt.legend(agent_names, loc='upper left', fontsize = 6, ncol=2)
            plt.title('vs name: {}, id: {}'.format(agent_names[i], i))
            plt.ylim(1, 9)

        transposed_plots_vs_all = transposed_plots.mean(axis=(0))

        plt.subplot(transposed_plots.shape[0] + 2, 2, transposed_plots.shape[0] * 2 + 1)
        for j in range(0, transposed_plots.shape[1]): # me
            tournament_progress_score_mean = transposed_plots_vs_all[j]
            line_width = line_widths[j]
            line_style = line_styles[j]
            plt.plot(tournament_progress_score_mean, linewidth = line_width, linestyle = line_style)

        plt.legend(agent_names, loc='upper left', fontsize = 6, ncol=2)
        plt.title('vs all')
        plt.ylim(1, 9)

        ###zoom###
        plt.subplot(transposed_plots.shape[0] + 2, 2, transposed_plots.shape[0] * 2 + 1 + 2)
        for j in range(0, transposed_plots.shape[1]): # me
            tournament_progress_score_mean = transposed_plots_vs_all[j]
            line_width = line_widths[j]
            line_style = line_styles[j]
            plt.plot(tournament_progress_score_mean, linewidth = line_width, linestyle = line_style)

        plt.legend(agent_names, loc='upper left', fontsize = 6, ncol=2)
        plt.title('vs all zoom')
        plt.ylim(5, 7)
        ###zoom###

        ######
        for i in range(0, transposed_plots.shape[0]): # me
            plt.subplot(transposed_plots.shape[0] + 2, 2, i * 2 + 2)
            for j in range(0, transposed_plots.shape[1]): # vs
                tournament_progress_score_mean = transposed_plots[j][i]
                line_width = line_widths[j]
                line_style = line_styles[j]
                plt.plot(tournament_progress_score_mean, linewidth = line_width, linestyle = line_style)

            plt.legend(agent_names, loc='upper left', fontsize = 6, ncol=2)
            plt.title('name: {}, id: {} vs '.format(agent_names[i], i))
            plt.ylim(1, 9)

        transposed_plots_all_vs = transposed_plots.mean(axis=(1))

        plt.subplot(transposed_plots.shape[0] + 2, 2, transposed_plots.shape[0] * 2 + 2)
        for j in range(0, transposed_plots.shape[1]): # vs
            tournament_progress_score_mean = transposed_plots_all_vs[j]
            line_width = line_widths[j]
            line_style = line_styles[j]
            plt.plot(tournament_progress_score_mean, linewidth = line_width, linestyle = line_style)

        plt.legend(agent_names, loc='upper left', fontsize = 6, ncol=2)
        plt.title('all vs')
        plt.ylim(1, 9)

        ###zoom###
        plt.subplot(transposed_plots.shape[0] + 2, 2, transposed_plots.shape[0] * 2 + 2 + 2)
        for j in range(0, transposed_plots.shape[1]): # vs
            tournament_progress_score_mean = transposed_plots_all_vs[j]
            line_width = line_widths[j]
            line_style = line_styles[j]
            plt.plot(tournament_progress_score_mean, linewidth = line_width, linestyle = line_style)

        plt.legend(agent_names, loc='upper left', fontsize = 6, ncol=2)
        plt.title('all vs zoom')
        plt.ylim(5, 7)
        ###zoom###

        ######



        plt.tight_layout()
        pathlib.Path('./images/').mkdir(parents=True, exist_ok=True)
        fig.savefig('./images/'  + plot_filename)
        plt.close()
