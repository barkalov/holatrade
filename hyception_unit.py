import os

import keras
from keras.layers import Input
from keras.layers import BatchNormalization, Activation
from keras.layers.convolutional import Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer

import numpy as np
import json

from layer_hyperizer import Hyperizer
 
class HyceptionUnit():

  def __init__(self, types_dim, rounds_dim, type_round_channels_dim, type_channels_dim, round_channels_dim, single_channels_dim):
    self.class_name = 'hyception'
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim

    self.type_round_channels_dim = type_round_channels_dim
    self.type_channels_dim = type_channels_dim
    self.round_channels_dim = round_channels_dim
    self.single_channels_dim = single_channels_dim    

    self.model = self.build_model()

  def build_model(self):
    hypermodel_type_round_data_input = Input(shape=(self.types_dim, self.rounds_dim, self.type_round_channels_dim))
    hypermodel_type_data_input = Input(shape=(self.types_dim, self.type_channels_dim))
    hypermodel_round_data_input = Input(shape=(self.rounds_dim, self.round_channels_dim))
    hypermodel_single_data_input = Input(shape=(self.single_channels_dim, ))

    x = Hyperizer(self.types_dim, self.rounds_dim, is_do_enrich = True, is_oldschool = False)([
      hypermodel_type_round_data_input, \
      hypermodel_type_data_input, \
      hypermodel_round_data_input, \
      hypermodel_single_data_input, \
    ])

    model_output = x
    model_inputs = [
      hypermodel_type_round_data_input,
      hypermodel_type_data_input,
      hypermodel_round_data_input,
      hypermodel_single_data_input,
    ]
    model = Model(model_inputs, model_output)
    print('====| hyception_unit |====')
    model.summary()
    return model

  def export_setup(self):

    setup = {
      'className': self.class_name,
      'typesDim': self.types_dim,
      'roundsDim': self.rounds_dim,
      'typeRoundChannelsDim': self.type_round_channels_dim,
      'typeChannelsDim': self.type_channels_dim,
      'roundChannelsDim': self.round_channels_dim,
      'singleChannelsDim': self.single_channels_dim,    
    }
    return setup

  def export_setup_to_json(self, json_path):
    setup = self.export_setup()
    os.makedirs(os.path.dirname(json_path), exist_ok=True)
    with open(json_path, 'w') as outfile:
      json.dump(setup, outfile)
  
  def add_to_stack(self, source, master_source):
    return self.model(source)

if __name__ == '__main__':
  test_type_round_input_batch = np.linspace(0, 1, 1 * 3 * 6 * 9)
  test_type_round_input_batch = test_type_round_input_batch.reshape((1, 3, 6, 9))

  test_type_input_batch = np.linspace(0, 1, 3 * 1 * 8)
  test_type_input_batch = test_type_input_batch.reshape((1, 3, 8))

  test_round_input_batch = np.linspace(0, 1, 1 * 6 * 7)
  test_round_input_batch = test_round_input_batch.reshape((1, 6, 7))

  test_single_input_batch = np.linspace(0, 1, 1 * 6)
  test_single_input_batch = test_single_input_batch.reshape((1, 6))

  test_inputs_batch = [
    test_type_round_input_batch,
    test_type_input_batch,
    test_round_input_batch,
    test_single_input_batch,  
  ]
  hyception_unit = HyceptionUnit(types_dim = 3, rounds_dim = 6, type_round_channels_dim = 9, type_channels_dim = 8, round_channels_dim = 7, single_channels_dim = 6)
  hyception_unit.export_setup_to_json('./exported_model/hyception_test/setup.json')
  test_pred = hyception_unit.model.predict(test_inputs_batch)
  print('test_pred ch 0', test_pred[0,:,:,0])
  print('test_pred ch 1', test_pred[0,:,:,1])
  print('test_pred ch 2', test_pred[0,:,:,2])
  print('test_pred ch 3', test_pred[0,:,:,3])
  print('test_pred shape', test_pred.shape)
  print('test_pred fl', test_pred.flatten())
  print('test_pred fl len', len(test_pred.flatten()))
  
  print('test_type_round_input_batch sum', test_type_round_input_batch.sum())
  print('test_type_input_batch sum', test_type_input_batch.sum())
  print('test_round_input_batch sum', test_round_input_batch.sum())
  print('test_single_input_batch sum', test_single_input_batch.sum())
  
  print('test_pred sum', test_pred.sum())
  print('end')
