const { Hypercube } = require('./hypermath.js');
const { UnitStack } = require('./unitStack.js');
function prettyPrintActionSource(actionSource, actionSourceName) {
  console.log('prettyprinting ' + actionSourceName);
  text = '';
  for (let channelsId = 0; channelsId < actionSource.channelsDim; channelsId++) {
    text += 'channel: ' + channelsId + '\n';
    for (let typesId = 0; typesId < actionSource.typesDim; typesId++) {
      for (let roundsId = 0; roundsId < actionSource.roundsDim; roundsId++) {
        let actionSourceId = actionSource.hyperindex(typesId, roundsId, channelsId);
        text += actionSource.rawData[actionSourceId] + ' ';
      }
      text += '\n'
    }
  }
  console.log(text);
}
function prettyPrintActionSources(actionSources) {
  prettyPrintActionSource(actionSources[0], 'typeRoundSource');
  prettyPrintActionSource(actionSources[1], 'typeSource');
  prettyPrintActionSource(actionSources[2], 'roundRoundSource');
  prettyPrintActionSource(actionSources[3], 'singleSource');
}
////
//// actionizer.js
////

class Actionizer {
  constructor(myValues, counts, isA, unitStack) {
    this.typesDim = 3;
    this.roundsDim = 6;
    this.myValues = myValues;
    this.counts = counts;
    this.isA = isA;
    this.myStepsAsISee = [];
    this.hisStepsAsISee = [];
    this.stepId = -1;
    this.unitStack = unitStack;
  }
  incomingOffer(offer) {
    this.hisStepsAsISee.push(offer);
    this.stepId++;
  }
  outgoingOffer(offer) {
    this.myStepsAsISee.push(offer);
  }

  generateAllOfferTries() {
    let countsSum = this.counts.reduce((a, b) => a + b, 0);
    let offerTries = [];
    for (let a = 0; a <= this.counts[0]; a++) {
      for (let b = 0; b <= this.counts[1]; b++) {
        for (let c = 0; c <= this.counts[2]; c++) {
          let offer = [a, b, c];
          let offerTotalValuesSum = offer[0] * this.myValues[0] + offer[1] * this.myValues[1] + offer[2] * this.myValues[2];
          if (offerTotalValuesSum > (2.5 + Math.random())) {
            offerTries.push(offer);
          }
        }
      }
    }
    return offerTries;
  }
  generateActionSourcesTry(offerTry) {
    let hisStepsAsISee = this.hisStepsAsISee;
    let myStepsAsISee = this.myStepsAsISee.slice();
    myStepsAsISee.push(offerTry);
    let typeRoundSource = new Hypercube(this.typesDim, this.roundsDim, 2);
    for (let typesId = 0; typesId < this.typesDim; typesId++) {
      for (let roundsId = 0; roundsId < this.roundsDim; roundsId++) {
        let hisStepsResultId = typeRoundSource.hyperindex(typesId, roundsId, 0);
        let myStepsResultId = typeRoundSource.hyperindex(typesId, roundsId, 1);
        // TODO
        let stepId = roundsId - (5 - this.stepId);
        if (stepId >= 0) {
          typeRoundSource.rawData[hisStepsResultId] = hisStepsAsISee[stepId][typesId] / 5;
          typeRoundSource.rawData[myStepsResultId] = myStepsAsISee[stepId][typesId] / 5;
        } else {
          typeRoundSource.rawData[hisStepsResultId] = 0 / 5;
          typeRoundSource.rawData[myStepsResultId] = this.counts[typesId] / 5;
        }
      }
    }

    let typeSource = new Hypercube(this.typesDim, 1, 2);
    for (let typesId = 0; typesId < this.typesDim; typesId++) {
      let myValuesResultId = typeSource.hyperindex(typesId, 0, 0);
      let countsResultId = typeSource.hyperindex(typesId, 0, 1);
      typeSource.rawData[myValuesResultId] = this.myValues[typesId] / 10;
      typeSource.rawData[countsResultId] = this.counts[typesId] / 5;
    }

    let roundSource = new Hypercube(1, this.roundsDim, 1);
    for (let roundsId = 0; roundsId < this.roundsDim; roundsId++) {
      // TODO
      let stepId = roundsId - (5 - this.stepId);
      let semiId = stepId * 2 + 1 + !this.isA * 1;
      semiId = Math.max(0, semiId);
      let semiIdsResultId = roundSource.hyperindex(0, roundsId, 0);
      roundSource.rawData[semiIdsResultId] = semiId / 9;
    }

    let singleSource = new Hypercube(1, 1, 1);
    let isAsResultId = singleSource.hyperindex(0, 0, 0);
    singleSource.rawData[isAsResultId] = this.isA * 1;


    let sources = [typeRoundSource, typeSource, roundSource, singleSource]
    return sources;
  }
  generateActionSourcesTries() {
    let offerTries = this.generateAllOfferTries();
    let actionSourcesTries = offerTries.map((offerTry)=>{
      return {actionSources: this.generateActionSourcesTry(offerTry), offer: offerTry}
    })
    return actionSourcesTries;
  }
  generateBestOffer() {
    let actionSourcesTries = this.generateActionSourcesTries();
    let predictions = actionSourcesTries.map((actionSourcesTry)=>{
      return {metapenalty: this.unitStack.feed(actionSourcesTry.actionSources).rawData[0], offer: actionSourcesTry.offer}
    })
    let bestPrediction = predictions.sort((a, b)=>{
      return a.metapenalty - b.metapenalty; // find min first penalty
    })[0];
    return bestPrediction.offer;
  }
}

//// end actionizer.js



module.exports = { Actionizer };


// // test
// const setups = require('../../exported_model/gameaction_metapenalty_guesser_zaa_16/setups.json');
// let unitStack = UnitStack.fromSetups(setups);
// let actionizer = new Actionizer([0,0,10], [3,2,1], true, unitStack);
// actionizer.incomingOffer([2,2,1]);
// //let offerTries = actionizer.generateAllOfferTries();
// let actionSourcesTries = actionizer.generateActionSourcesTries();



// console.log(actionSourcesTries);
// prettyPrintActionSources(actionSourcesTries[0].actionSources)
// let now = Date.now();
// let bestOffer = actionizer.generateBestOffer();
// let diff = Date.now() - now;
// console.log('diff', diff);
// console.log(bestOffer);