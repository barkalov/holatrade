'use strict';

//import { Hypercube, Hyperkernel, Hyperbias } from './hypermath.js';
const { Hypercube, Hyperkernel, Hyperbias } = require('./hypermath.js');
const { CeptionUnit } = require('./unit.js');

const setup = require('../../exported_model/ception_test/setup.json');

// run test
let source = new Hypercube(3, 6, 7);
let testInput = [] 
for (let i = 0; i < 3 * 6 * 7; i++) {
  let ni = i / (3 * 6 * 7 - 1)
  testInput[i] = ni;
}
source.rawData = new Float64Array(testInput);

let ceptionUnit = CeptionUnit.fromSetup(setup);
let result = ceptionUnit.feed(source);
console.log('result sum', result.rawData.reduce((a, b) => a + b, 0));
console.log('result length', result.rawData.length);
console.log('result', result.rawData);