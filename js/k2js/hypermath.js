////
//// hypermath.js
////

class AbstractHyper {
  constructor() {
    if (new.target === AbstractHyper) {
      throw new TypeError("Cannot construct Abstract instances directly");
    }
  }
  hyperindex(channelsId) {
    throw new Error('Not implemented');
  }
}

class Hyperbias extends AbstractHyper {
  constructor (channelsDim) {
    super();
    this.channelsDim = channelsDim;
    this.rawData = new Float64Array(this.channelsDim);
  }
  hyperindex(channelsId) {
    return channelsId;
  }
}
class Hyperkernel extends AbstractHyper {
  constructor (typesDim, roundsDim, inputChannelsDim, channelsDim) {
    super();
    this.typesDim = typesDim;
    this.roundsDim = roundsDim;
    this.inputChannelsDim = inputChannelsDim;
    this.channelsDim = channelsDim;
    this.rawData = new Float64Array(this.typesDim * this.roundsDim * this.inputChannelsDim * this.channelsDim);
  }
  hyperindex(typesId, roundsId, inputChannelsId, channelsId) {
    return typesId * this.roundsDim * this.inputChannelsDim * this.channelsDim +
           roundsId * this.inputChannelsDim * this.channelsDim +
           inputChannelsId * this.channelsDim +
           channelsId;
  }
}

class Hypercube extends AbstractHyper {
  constructor (typesDim, roundsDim, channelsDim) {
    super();
    this.typesDim = typesDim;
    this.roundsDim = roundsDim;
    this.channelsDim = channelsDim;
    this.rawData = new Float64Array(this.typesDim * this.roundsDim * this.channelsDim);
  }
  hyperindex(typesId, roundsId, channelsId) {
    return typesId * this.roundsDim * this.channelsDim +
           roundsId * this.channelsDim +
           channelsId;
  }
  static convolve(source, kernel, bias) {
    console.assert(source.channelsDim == kernel.inputChannelsDim)
    console.assert(bias.channelsDim == kernel.channelsDim)

    let kernelTypesPadding = kernel.typesDim - 1;
    let kernelRoundsPadding = kernel.roundsDim - 1;
    let result = new Hypercube(source.typesDim - kernelTypesPadding, source.roundsDim - kernelRoundsPadding, kernel.channelsDim)

    for (let typesId = 0; typesId < source.typesDim - kernelTypesPadding; typesId++) {
      for (let roundsId = 0; roundsId < source.roundsDim - kernelRoundsPadding; roundsId++) {
        for (let channelsId = 0; channelsId < kernel.channelsDim; channelsId++) {
          let resultId = result.hyperindex(typesId, roundsId, channelsId);
          let biasId = bias.hyperindex(channelsId)

          for (let inputChannelsId = 0; inputChannelsId < kernel.inputChannelsDim; inputChannelsId++) {
            for (let kernelTypesId = 0; kernelTypesId < kernel.typesDim; kernelTypesId++) {
              for (let kernelRoundsId = 0; kernelRoundsId < kernel.roundsDim; kernelRoundsId++) {
                let sourceTypesId = typesId + kernelTypesId;
                let sourceRoundsId = roundsId + kernelRoundsId;
                let sourceId = source.hyperindex(sourceTypesId, sourceRoundsId, inputChannelsId);
                let kernelId = kernel.hyperindex(kernelTypesId, kernelRoundsId, inputChannelsId, channelsId);
                result.rawData[resultId] += source.rawData[sourceId] * kernel.rawData[kernelId]
              }
            }
          }
          result.rawData[resultId] += bias.rawData[biasId]
        }
      }
    }
    return result;
  }
  static norm(source, mean, variance, beta, gamma) {
    console.assert(source.channelsDim == mean.channelsDim)
    console.assert(mean.channelsDim == variance.channelsDim)
    console.assert(variance.channelsDim == beta.channelsDim)
    console.assert(beta.channelsDim == gamma.channelsDim)
    const epsilon = 1e-3
    let result = new Hypercube(source.typesDim, source.roundsDim, source.channelsDim)
    for (let typesId = 0; typesId < source.typesDim; typesId++) {
      for (let roundsId = 0; roundsId < source.roundsDim; roundsId++) {
        for (let channelsId = 0; channelsId < source.channelsDim; channelsId++) {
          let resultId = result.hyperindex(typesId, roundsId, channelsId);
          let meanVarianceBetaGammaId = mean.hyperindex(channelsId);

          let sourceValue = source.rawData[resultId];

          let meanValue = mean.rawData[meanVarianceBetaGammaId];
          let varianceValue = variance.rawData[meanVarianceBetaGammaId];
          let betaValue = beta.rawData[meanVarianceBetaGammaId];
          let gammaValue = gamma.rawData[meanVarianceBetaGammaId];

          let resultValue = (sourceValue - meanValue) / Math.sqrt(varianceValue + epsilon) * gammaValue + betaValue;

          result.rawData[resultId] = resultValue;
        }
      }
    }
    return result;
  }
  static activate(source, fn) {
    let result = new Hypercube(source.typesDim, source.roundsDim, source.channelsDim)
    for (let typesId = 0; typesId < source.typesDim; typesId++) {
      for (let roundsId = 0; roundsId < source.roundsDim; roundsId++) {
        for (let channelsId = 0; channelsId < source.channelsDim; channelsId++) {
          let resultId = result.hyperindex(typesId, roundsId, channelsId);
          let sourceValue = source.rawData[resultId];
          result.rawData[resultId] = fn(sourceValue);
        }
      }
    }
    return result;
  }

  static upsample(source, typesDimFactor, roundsDimFactor) {
    console.assert(typesDimFactor % 1 == 0)
    console.assert(roundsDimFactor % 1 == 0)
    let result = new Hypercube(source.typesDim * typesDimFactor, source.roundsDim * roundsDimFactor, source.channelsDim)
    for (let typesId = 0; typesId < source.typesDim; typesId++) {
      for (let roundsId = 0; roundsId < source.roundsDim; roundsId++) {
        for (let channelsId = 0; channelsId < source.channelsDim; channelsId++) {
          let sourceId = source.hyperindex(typesId, roundsId, channelsId);
          for (let typesIdFactored = 0; typesIdFactored < typesDimFactor; typesIdFactored++) {
            for (let roundsIdFactored = 0; roundsIdFactored < roundsDimFactor; roundsIdFactored++) {
              let resultTypesId = typesId * typesDimFactor + typesIdFactored;
              let resultRoundsId = roundsId * roundsDimFactor + roundsIdFactored;
              let resultId = result.hyperindex(resultTypesId, resultRoundsId, channelsId);
              result.rawData[resultId] = source.rawData[sourceId]
            }
          }
        }
      }
    }
    return result;
  }
  static typesDimAverage(source) {
    let result = new Hypercube(1, source.roundsDim, source.channelsDim)
    for (let channelsId = 0; channelsId < source.channelsDim; channelsId++) {
      for (let roundsId = 0; roundsId < source.roundsDim; roundsId++) {
      let resultId = result.hyperindex(0, roundsId, channelsId);
        for (let typesId = 0; typesId < source.typesDim; typesId++) {
          let sourceId = source.hyperindex(typesId, roundsId, channelsId);
          result.rawData[resultId] += source.rawData[sourceId];
        }
        result.rawData[resultId] /= source.typesDim;
      }
    }
    return result
  }
  static concatenate(hypercubes) {
    let concatenatedChannelsDim = 0;
    let referenceHypercube = hypercubes[0];
    for (let hypercubesId = 0; hypercubesId < hypercubes.length; hypercubesId++) {
      let hypercube = hypercubes[hypercubesId];
      concatenatedChannelsDim += hypercube.channelsDim;
      console.assert(hypercube.typesDim == referenceHypercube.typesDim);
      console.assert(hypercube.roundsDim == referenceHypercube.roundsDim);
    }

    let result = new Hypercube(referenceHypercube.typesDim, referenceHypercube.roundsDim, concatenatedChannelsDim)
    let concatenatedChannelsId = 0;
    for (let hypercubesId = 0; hypercubesId < hypercubes.length; hypercubesId++) {
      let hypercube = hypercubes[hypercubesId];
      for (let channelsId = 0; channelsId < hypercube.channelsDim; channelsId++) {
        for (let typesId = 0; typesId < referenceHypercube.typesDim; typesId++) {
          for (let roundsId = 0; roundsId < referenceHypercube.roundsDim; roundsId++) {
            let hypercubeId = hypercube.hyperindex(typesId, roundsId, channelsId);
            let resultId = result.hyperindex(typesId, roundsId, concatenatedChannelsId);
            result.rawData[resultId] = hypercube.rawData[hypercubeId];
          }
        }
        concatenatedChannelsId++;
      }
    }
    return result;
  }

  static hyperize(typeRoundHypercube, typeHypercube, roundHypercube, singleHypercube) {
    console.assert(typeRoundHypercube.typesDim == typeHypercube.typesDim);
    console.assert(typeRoundHypercube.roundsDim == roundHypercube.roundsDim);

    let typesDim = typeRoundHypercube.typesDim;
    let roundsDim = typeRoundHypercube.roundsDim;

    let typeRoundChannelsDim = typeRoundHypercube.channelsDim;
    let typeChannelsDim = typeHypercube.channelsDim;
    let roundChannelsDim = roundHypercube.channelsDim;
    let singleChannelsDim = singleHypercube.channelsDim;

    let channelsDim = typeRoundChannelsDim * 3 + // * 3 beacause of enrich
                      typeChannelsDim * 3 +
                      roundChannelsDim +
                      singleChannelsDim;
    let result = new Hypercube(typesDim, roundsDim, channelsDim);

    let resultChannelsIdFilled = 0;

    // typeRoundData
    for (let channelsId = 0; channelsId < typeRoundChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let typeRoundHypercubeId = typeRoundHypercube.hyperindex(typesId, roundsId, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = typeRoundHypercube.rawData[typeRoundHypercubeId];
        }
      }
      resultChannelsIdFilled++;
    }

    // typeRoundData (enrich via sum)
    for (let channelsId = 0; channelsId < typeRoundChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let typesIdA = (typesId + 1 + typesDim) % typesDim;
          let typesIdB = (typesId - 1 + typesDim) % typesDim;

          let typeRoundHypercubeIdA = typeRoundHypercube.hyperindex(typesIdA, roundsId, channelsId);
          let typeRoundHypercubeIdB = typeRoundHypercube.hyperindex(typesIdB, roundsId, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = typeRoundHypercube.rawData[typeRoundHypercubeIdA] + typeRoundHypercube.rawData[typeRoundHypercubeIdB];
        }
      }
      resultChannelsIdFilled++;
    }

    // typeRoundData (enrich via absdiff)
    for (let channelsId = 0; channelsId < typeRoundChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let typesIdA = (typesId + 1 + typesDim) % typesDim;
          let typesIdB = (typesId - 1 + typesDim) % typesDim;

          let typeRoundHypercubeIdA = typeRoundHypercube.hyperindex(typesIdA, roundsId, channelsId);
          let typeRoundHypercubeIdB = typeRoundHypercube.hyperindex(typesIdB, roundsId, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = Math.abs(typeRoundHypercube.rawData[typeRoundHypercubeIdA] - typeRoundHypercube.rawData[typeRoundHypercubeIdB]);
        }
      }
      resultChannelsIdFilled++;
    }

    // typeData
    for (let channelsId = 0; channelsId < typeChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let typeHypercubeId = typeHypercube.hyperindex(typesId, 0, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = typeHypercube.rawData[typeHypercubeId];
        }
      }
      resultChannelsIdFilled++;
    }

    // typeData (enrich via sum)
    for (let channelsId = 0; channelsId < typeChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let typesIdA = (typesId + 1 + typesDim) % typesDim;
          let typesIdB = (typesId - 1 + typesDim) % typesDim;

          let typeHypercubeIdA = typeHypercube.hyperindex(typesIdA, 0, channelsId);
          let typeHypercubeIdB = typeHypercube.hyperindex(typesIdB, 0, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = typeHypercube.rawData[typeHypercubeIdA] + typeHypercube.rawData[typeHypercubeIdB];
        }
      }
      resultChannelsIdFilled++;
    }

    // typeData (enrich via absdiff)
    for (let channelsId = 0; channelsId < typeChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let typesIdA = (typesId + 1 + typesDim) % typesDim;
          let typesIdB = (typesId - 1 + typesDim) % typesDim;

          let typeHypercubeIdA = typeHypercube.hyperindex(typesIdA, 0, channelsId);
          let typeHypercubeIdB = typeHypercube.hyperindex(typesIdB, 0, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = Math.abs(typeHypercube.rawData[typeHypercubeIdA] - typeHypercube.rawData[typeHypercubeIdB]);
        }
      }
      resultChannelsIdFilled++;
    }


    // roundData
    for (let channelsId = 0; channelsId < roundChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let roundHypercubeId = roundHypercube.hyperindex(0, roundsId, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = roundHypercube.rawData[roundHypercubeId];
        }
      }
      resultChannelsIdFilled++;
    }

    // singleData
    for (let channelsId = 0; channelsId < singleChannelsDim; channelsId++) {
      for (let typesId = 0; typesId < typesDim; typesId++) {
        for (let roundsId = 0; roundsId < roundsDim; roundsId++) {
          let singleHypercubeId = singleHypercube.hyperindex(0, 0, channelsId);
          let resultId = result.hyperindex(typesId, roundsId, resultChannelsIdFilled);
          result.rawData[resultId] = singleHypercube.rawData[singleHypercubeId];
        }
      }
      resultChannelsIdFilled++;
    }

    return result;
  }
}
class Activations {
  static elu(sourceValue) {
    const alpha = 1;
    if (sourceValue >= 0) {
      return sourceValue
    } else {
      return alpha * (Math.exp(sourceValue) - 1)
    }
  }
}

//// end hypermath.js

module.exports = { Hypercube, Hyperkernel, Hyperbias, Activations };
