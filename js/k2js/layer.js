const { Hypercube, Hyperkernel, Hyperbias, Activations } = require('./hypermath.js');
////
//// layer.js
////

class AbstractLayer {
  constructor() {
    if (new.target === AbstractLayer) {
      throw new TypeError("Cannot construct Abstract instances directly");
    }
  }
  feed(source) {
    throw new Error('Not implemented');
  }
}

class Conv2d extends AbstractLayer {
  constructor(kernel, bias) {
    super();
    this.kernel = kernel;
    this.bias = bias;
  }
  feed(source) {
    return Hypercube.convolve(source, this.kernel, this.bias);
  }
}

class BatchNormalization extends AbstractLayer {
  constructor(mean, variance, beta, gamma) {
    super();
    this.mean = mean;
    this.variance = variance;
    this.beta = beta;
    this.gamma = gamma;
  }
  feed(source) {
    return Hypercube.norm(source, this.mean, this.variance, this.beta, this. gamma);
  }
}

class Elu extends AbstractLayer {
  constructor() {
    super();
  }
  feed(source) {
    return Hypercube.activate(source, Activations.elu);
  }
}

class Upsampling2d extends AbstractLayer {
  constructor(typesDimFactor, roundsDimFactor) {
    super();
    this.typesDimFactor = typesDimFactor;
    this.roundsDimFactor = roundsDimFactor;
  }
  feed(source) {
    return Hypercube.upsample(source, this.typesDimFactor, this.roundsDimFactor);
  }
}

class TypesDimAveragePooling2d extends AbstractLayer {
  constructor() {
    super();
  }
  feed(source) {
    return Hypercube.typesDimAverage(source);
  }
}

class Hyperizer extends AbstractLayer {
  constructor(typesDim, roundsDim) {
    super();
    this.typesDim = typesDim;
    this.roundsDim = roundsDim;
  }
  feed([typeRoundHypercube, typeHypercube, roundHypercube, singleHypercube]) {
    return Hypercube.hyperize(typeRoundHypercube, typeHypercube, roundHypercube, singleHypercube, this.typesDim, this.roundsDim);
  }
}

class Concatenate extends AbstractLayer {
  constructor() {
    super();
  }
  feed(sources) {
    return Hypercube.concatenate(sources);
  }
}

//// end layer.js

module.exports = { Conv2d, BatchNormalization, Elu, Upsampling2d, TypesDimAveragePooling2d, Concatenate, Hyperizer };
