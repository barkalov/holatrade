const { Hypercube, Hyperkernel, Hyperbias, Activations } = require('./hypermath.js');
const { Conv2d, BatchNormalization, Elu, Upsampling2d, Concatenate, TypesDimAveragePooling2d, Hyperizer } = require('./layer.js');
////
//// unit.js
////

class AbstractUnit {
  constructor() {
    if (new.target === AbstractUnit) {
      throw new TypeError("Cannot construct Abstract instances directly");
    }
  }
  feed(source) {
    throw new Error('Not implemented');
  }
  static fromSetup(setup) {
    throw new Error('Not implemented');
  }

}

class CeptionUnit extends AbstractUnit {
  constructor(kernel, bias, mean, variance, beta, gamma, upsamplingTypesDimFactor, upsamplingRoundsDimFactor) {
    super();
    console.assert(kernel.channelsDim == bias.channelsDim)
    console.assert(bias.channelsDim == mean.channelsDim);
    console.assert(mean.channelsDim == variance.channelsDim);
    console.assert(variance.channelsDim == beta.channelsDim);
    console.assert(beta.channelsDim == gamma.channelsDim);

    this.conv2d = new Conv2d(kernel, bias);
    this.batchNormalization = new BatchNormalization(mean, variance, beta, gamma);
    this.elu = new Elu();
    this.upsampling2d = new Upsampling2d(upsamplingTypesDimFactor, upsamplingRoundsDimFactor)
  }
  feed(source) {
    let x = source;
    x = this.conv2d.feed(x);
    x = this.batchNormalization.feed(x);
    x = this.elu.feed(x);
    x = this.upsampling2d.feed(x);
    return x;
  }
  static fromSetup(setup) {
    let kernel = new Hyperkernel(setup.kernelTypesDim, setup.kernelRoundsDim, setup.inputChannelsDim, setup.channelsDim);
    let bias = new Hyperbias(setup.channelsDim);

    let mean = new Hyperbias(setup.channelsDim);
    let variance = new Hyperbias(setup.channelsDim);
    let beta = new Hyperbias(setup.channelsDim);
    let gamma = new Hyperbias(setup.channelsDim);

    kernel.rawData = new Float64Array(setup.convKernel);
    bias.rawData = new Float64Array(setup.convBias);

    mean.rawData = new Float64Array(setup.bnMean);
    variance.rawData = new Float64Array(setup.bnVariance);
    beta.rawData = new Float64Array(setup.bnBeta);
    gamma.rawData = new Float64Array(setup.bnGamma);

    return new CeptionUnit(kernel, bias, mean, variance, beta, gamma, setup.upsamplingTypesDimFactor, setup.upsamplingRoundsDimFactor);
  }
}

class ReceptionUnit extends AbstractUnit {
  constructor(kernelA, biasA, meanA, varianceA, betaA, gammaA,
              kernelB, biasB, meanB, varianceB, betaB, gammaB,
              kernelC, biasC, meanC, varianceC, betaC, gammaC,
              kernelD, biasD, meanD, varianceD, betaD, gammaD,
             ) {
    super();
    console.assert(kernelA.channelsDim == biasA.channelsDim)
    console.assert(biasA.channelsDim == meanA.channelsDim);
    console.assert(meanA.channelsDim == varianceA.channelsDim);
    console.assert(varianceA.channelsDim == betaA.channelsDim);
    console.assert(betaA.channelsDim == gammaA.channelsDim);

    console.assert(kernelB.channelsDim == biasB.channelsDim)
    console.assert(biasB.channelsDim == meanB.channelsDim);
    console.assert(meanB.channelsDim == varianceB.channelsDim);
    console.assert(varianceB.channelsDim == betaB.channelsDim);
    console.assert(betaB.channelsDim == gammaB.channelsDim);

    console.assert(kernelC.channelsDim == biasC.channelsDim)
    console.assert(biasC.channelsDim == meanC.channelsDim);
    console.assert(meanC.channelsDim == varianceC.channelsDim);
    console.assert(varianceC.channelsDim == betaC.channelsDim);
    console.assert(betaC.channelsDim == gammaC.channelsDim);

    console.assert(kernelD.channelsDim == biasD.channelsDim)
    console.assert(biasD.channelsDim == meanD.channelsDim);
    console.assert(meanD.channelsDim == varianceD.channelsDim);
    console.assert(varianceD.channelsDim == betaD.channelsDim);
    console.assert(betaD.channelsDim == gammaD.channelsDim);

    this.conv2dA = new Conv2d(kernelA, biasA);
    this.batchNormalizationA = new BatchNormalization(meanA, varianceA, betaA, gammaA);
    this.eluA = new Elu();

    this.conv2dB = new Conv2d(kernelB, biasB);
    this.batchNormalizationB = new BatchNormalization(meanB, varianceB, betaB, gammaB);
    this.eluB = new Elu();
    this.upsampling2dB = new Upsampling2d(kernelB.typesDim, 1)

    this.conv2dC = new Conv2d(kernelC, biasC);
    this.batchNormalizationC = new BatchNormalization(meanC, varianceC, betaC, gammaC);
    this.eluC = new Elu();

    this.conv2dD = new Conv2d(kernelD, biasD);
    this.batchNormalizationD = new BatchNormalization(meanD, varianceD, betaD, gammaD);
    this.eluD = new Elu();
    this.upsampling2dD = new Upsampling2d(1, kernelD.roundsDim)

    this.concatenate = new Concatenate(kernelB.typesDim, kernelD.roundsDim)

  }
  feed(source) {
    let x = source;

    // a
    let x_ti = this.conv2dA.feed(x);
    x_ti = this.batchNormalizationA.feed(x_ti);
    x_ti = this.eluA.feed(x_ti);

    // b
    let x_t = this.conv2dB.feed(x_ti);
    x_t = this.batchNormalizationB.feed(x_t);
    x_t = this.eluB.feed(x_t);
    x_t = this.upsampling2dB.feed(x_t);

    // c
    let x_ri = this.conv2dC.feed(x);
    x_ri = this.batchNormalizationC.feed(x_ri);
    x_ri = this.eluC.feed(x_ri);

    // d
    let x_r = this.conv2dD.feed(x_ri);
    x_r = this.batchNormalizationD.feed(x_r);
    x_r = this.eluD.feed(x_r);
    x_r = this.upsampling2dD.feed(x_r);

    x = this.concatenate.feed([x, x_t, x_r]);

    return x;
  }
  static fromSetup(setup) {
    // a
    let channelsDimA = 8 * setup.scaleFactor;
    let kernelA = new Hyperkernel(1, 1, setup.inputChannelsDim, channelsDimA);
    let biasA = new Hyperbias(channelsDimA);

    let meanA = new Hyperbias(channelsDimA);
    let varianceA = new Hyperbias(channelsDimA);
    let betaA = new Hyperbias(channelsDimA);
    let gammaA = new Hyperbias(channelsDimA);

    kernelA.rawData = new Float64Array(setup.convKernelA);
    biasA.rawData = new Float64Array(setup.convBiasA);

    meanA.rawData = new Float64Array(setup.bnMeanA);
    varianceA.rawData = new Float64Array(setup.bnVarianceA);
    betaA.rawData = new Float64Array(setup.bnBetaA);
    gammaA.rawData = new Float64Array(setup.bnGammaA);

    // b
    let channelsDimB = 16 * setup.scaleFactor;
    let kernelB = new Hyperkernel(setup.typesDim, 1, channelsDimA, channelsDimB);
    let biasB = new Hyperbias(channelsDimB);

    let meanB = new Hyperbias(channelsDimB);
    let varianceB = new Hyperbias(channelsDimB);
    let betaB = new Hyperbias(channelsDimB);
    let gammaB = new Hyperbias(channelsDimB);

    kernelB.rawData = new Float64Array(setup.convKernelB);
    biasB.rawData = new Float64Array(setup.convBiasB);

    meanB.rawData = new Float64Array(setup.bnMeanB);
    varianceB.rawData = new Float64Array(setup.bnVarianceB);
    betaB.rawData = new Float64Array(setup.bnBetaB);
    gammaB.rawData = new Float64Array(setup.bnGammaB);

    // c
    let channelsDimC = 8 * setup.scaleFactor;
    let kernelC = new Hyperkernel(1, 1, setup.inputChannelsDim, channelsDimC);
    let biasC = new Hyperbias(channelsDimC);

    let meanC = new Hyperbias(channelsDimC);
    let varianceC = new Hyperbias(channelsDimC);
    let betaC = new Hyperbias(channelsDimC);
    let gammaC = new Hyperbias(channelsDimC);

    kernelC.rawData = new Float64Array(setup.convKernelC);
    biasC.rawData = new Float64Array(setup.convBiasC);

    meanC.rawData = new Float64Array(setup.bnMeanC);
    varianceC.rawData = new Float64Array(setup.bnVarianceC);
    betaC.rawData = new Float64Array(setup.bnBetaC);
    gammaC.rawData = new Float64Array(setup.bnGammaC);

    // d
    let channelsDimD = 16 * setup.scaleFactor;
    let kernelD = new Hyperkernel(1, setup.roundsDim, channelsDimC, channelsDimD);
    let biasD = new Hyperbias(channelsDimD);

    let meanD = new Hyperbias(channelsDimD);
    let varianceD = new Hyperbias(channelsDimD);
    let betaD = new Hyperbias(channelsDimD);
    let gammaD = new Hyperbias(channelsDimD);

    kernelD.rawData = new Float64Array(setup.convKernelD);
    biasD.rawData = new Float64Array(setup.convBiasD);

    meanD.rawData = new Float64Array(setup.bnMeanD);
    varianceD.rawData = new Float64Array(setup.bnVarianceD);
    betaD.rawData = new Float64Array(setup.bnBetaD);
    gammaD.rawData = new Float64Array(setup.bnGammaD);


    return new ReceptionUnit(kernelA, biasA, meanA, varianceA, betaA, gammaA,
                             kernelB, biasB, meanB, varianceB, betaB, gammaB,
                             kernelC, biasC, meanC, varianceC, betaC, gammaC,
                             kernelD, biasD, meanD, varianceD, betaD, gammaD);
  }
}

class FinceptionUnit extends AbstractUnit {
  constructor(kernelA, biasA, meanA, varianceA, betaA, gammaA,
              kernelB, biasB, meanB, varianceB, betaB, gammaB,
             ) {
    super();
    console.assert(kernelA.channelsDim == biasA.channelsDim)
    console.assert(biasA.channelsDim == meanA.channelsDim);
    console.assert(meanA.channelsDim == varianceA.channelsDim);
    console.assert(varianceA.channelsDim == betaA.channelsDim);
    console.assert(betaA.channelsDim == gammaA.channelsDim);

    console.assert(kernelB.channelsDim == biasB.channelsDim)
    console.assert(biasB.channelsDim == meanB.channelsDim);
    console.assert(meanB.channelsDim == varianceB.channelsDim);
    console.assert(varianceB.channelsDim == betaB.channelsDim);
    console.assert(betaB.channelsDim == gammaB.channelsDim);

    this.conv2dA = new Conv2d(kernelA, biasA);
    this.batchNormalizationA = new BatchNormalization(meanA, varianceA, betaA, gammaA);
    this.eluA = new Elu();
    this.upsampling2dA = new Upsampling2d(kernelA.typesDim, 1)

    this.concatenate = new Concatenate(kernelA.typesDim, kernelB.roundsDim)

    this.conv2dB = new Conv2d(kernelB, biasB);
    this.batchNormalizationB = new BatchNormalization(meanB, varianceB, betaB, gammaB);
    this.eluB = new Elu();

    this.typesDimAveragePooling2d = new TypesDimAveragePooling2d();

  }
  feed(source, masterSource) {
    let x = source;

    // a
    let x_t = this.conv2dA.feed(x);
    x_t = this.batchNormalizationA.feed(x_t);
    x_t = this.eluA.feed(x_t);
    x_t = this.upsampling2dA.feed(x_t);

    x = this.concatenate.feed([x, x_t, masterSource]);

    // b
    x = this.conv2dB.feed(x);
    x = this.batchNormalizationB.feed(x);
    x = this.eluB.feed(x);

    x = this.typesDimAveragePooling2d.feed(x);

    return x;
  }
  static fromSetup(setup) {

    // a
    let channelsDimA = 16 * setup.scaleFactor;
    let kernelA = new Hyperkernel(setup.typesDim, 1, setup.inputChannelsDim, channelsDimA);
    let biasA = new Hyperbias(channelsDimA);

    let meanA = new Hyperbias(channelsDimA);
    let varianceA = new Hyperbias(channelsDimA);
    let betaA = new Hyperbias(channelsDimA);
    let gammaA = new Hyperbias(channelsDimA);

    kernelA.rawData = new Float64Array(setup.convKernelA);
    biasA.rawData = new Float64Array(setup.convBiasA);

    meanA.rawData = new Float64Array(setup.bnMeanA);
    varianceA.rawData = new Float64Array(setup.bnVarianceA);
    betaA.rawData = new Float64Array(setup.bnBetaA);
    gammaA.rawData = new Float64Array(setup.bnGammaA);


    // b
    let channelsDimB = 16 * setup.scaleFactor;
    let inputChannelsDimB = channelsDimA + setup.inputChannelsDim + setup.masterInputChannelsDim;
    let kernelB = new Hyperkernel(1, setup.roundsDim, inputChannelsDimB, channelsDimB);
    let biasB = new Hyperbias(channelsDimB);

    let meanB = new Hyperbias(channelsDimB);
    let varianceB = new Hyperbias(channelsDimB);
    let betaB = new Hyperbias(channelsDimB);
    let gammaB = new Hyperbias(channelsDimB);

    kernelB.rawData = new Float64Array(setup.convKernelB);
    biasB.rawData = new Float64Array(setup.convBiasB);

    meanB.rawData = new Float64Array(setup.bnMeanB);
    varianceB.rawData = new Float64Array(setup.bnVarianceB);
    betaB.rawData = new Float64Array(setup.bnBetaB);
    gammaB.rawData = new Float64Array(setup.bnGammaB);


    return new FinceptionUnit(kernelA, biasA, meanA, varianceA, betaA, gammaA,
                              kernelB, biasB, meanB, varianceB, betaB, gammaB,
                             );
  }
}

class HyceptionUnit extends AbstractUnit {
  constructor(typesDim, roundsDim) {
    super();
    this.hyperizer = new Hyperizer(typesDim, roundsDim);

  }
  feed(flatSources) {
    let x = flatSources;
    x = this.hyperizer.feed(x);

    return x;
  }
  static fromSetup(setup) {
    return new HyceptionUnit(setup.typesDim, setup.roundsDim);
  }
}
//// end unit.js

module.exports = { CeptionUnit, ReceptionUnit, FinceptionUnit, HyceptionUnit };
