const { CeptionUnit, ReceptionUnit, FinceptionUnit, HyceptionUnit } = require('./unit.js')
////
//// unitStack.js
////

class AbstractUnitStack {
  constructor() {
    if (new.target === AbstractUnitStack) {
      throw new TypeError("Cannot construct Abstract instances directly");
    }
  }
  feed(source) {
    throw new Error('Not implemented');
  }
  static fromSetups(setups) {
    throw new Error('Not implemented');
  }

}

class UnitStack extends AbstractUnitStack {
  constructor(units) {
    super();
    this.units = units;
  }
  feed(sources) {
    let x;
    let master_source;
    this.units.forEach((unit) => {
      if (unit.hyperizer) {
        x = unit.feed(sources);
        master_source = x;
      } else {
        x = unit.feed(x, master_source);
      }
    })
    return x;
  }
  static fromSetups(setups) {
    const classes = {
      'hyception': HyceptionUnit,
      'ception': CeptionUnit,
      'reception': ReceptionUnit,
      'finception': FinceptionUnit
    }
    let units = setups.map((setup) => {
      let Unit = classes[setup.className];
      let unit = Unit.fromSetup(setup);
      return unit;
    })
    return new UnitStack(units);
  }
}
//// end unitStack.js
module.exports = { UnitStack };
