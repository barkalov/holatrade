'use strict';

//import { Hypercube, Hyperkernel, Hyperbias } from './hypermath.js';
const { Hypercube, Hyperkernel, Hyperbias } = require('./hypermath.js');
const { UnitStack } = require('./unitStack.js');

//const setups = require('../../exported_model/gameaction_metapenalty_guesser_aab_try/setups.json');
const setups = require('../../exported_model/gameaction_metapenalty_guesser_uaa/setups.json');

// run test
let typeRoundSource = new Hypercube(3, 6, 2);
let typeRoundTestInput = [];
for (let i = 0; i < 3 * 6 * 2; i++) {
  let ni = i / (3 * 6 * 2 - 1)
  typeRoundTestInput[i] = ni;
}
typeRoundSource.rawData = new Float64Array(typeRoundTestInput);

let typeSource = new Hypercube(3, 1, 2);
let typeTestInput = [];
for (let i = 0; i < 3 * 1 * 2; i++) {
  let ni = i / (3 * 1 * 2 - 1)
  typeTestInput[i] = ni;
}
typeSource.rawData = new Float64Array(typeTestInput);

let roundSource = new Hypercube(1, 6, 1);
let roundTestInput = [];
for (let i = 0; i < 1 * 6 * 1; i++) {
  let ni = i / (1 * 6 * 1 - 1)
  roundTestInput[i] = ni;
}
roundSource.rawData = new Float64Array(roundTestInput);


let singleSource = new Hypercube(1, 1, 1);
let singleTestInput = [];
for (let i = 0; i < 1 * 1 * 1; i++) {
  let ni = i / (1 * 1 * 1 - 1)
  singleTestInput[i] = ni || 0;
}
singleSource.rawData = new Float64Array(singleTestInput);


let sources = [typeRoundSource, typeSource, roundSource, singleSource]

let unitStack = UnitStack.fromSetups(setups);
let result = unitStack.feed(sources);

console.log('typeRoundSource sum', typeRoundSource.rawData.reduce((a, b) => a + b, 0));
console.log('typeSource sum', typeSource.rawData.reduce((a, b) => a + b, 0));
console.log('roundSource sum', roundSource.rawData.reduce((a, b) => a + b, 0));
console.log('singleSource sum', singleSource.rawData.reduce((a, b) => a + b, 0));

console.log('result sum', result.rawData.reduce((a, b) => a + b, 0));
console.log('result length', result.rawData.length);
console.log('result', result.rawData);



