import tensorflowjs as tfjs
name = 'gameaction_score_guesser_ba'
from gameaction_score_guesser import GameactionScoreGuesser
gameaction_score_guesser = GameactionScoreGuesser(name = 'gameaction_score_guesser_ba', preload = True)
tfjs.converters.save_keras_model(gameaction_score_guesser.supermodel, 'tfjs/' + name)