
import keras.backend as K
import tensorflow as tf
import numpy as np
from keras.engine.topology import Layer

class GameactionToGamestate(Layer):

  def __init__(self, types_dim, rounds_dim, is_no_cheat = False, **kwargs):
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim
    self.is_no_cheat = is_no_cheat
    super(GameactionToGamestate, self).__init__(**kwargs)

  def build(self, input_shapes):
    type_round_data_shape = input_shapes[0]
    type_data_shape = input_shapes[1]
    round_data_shape = input_shapes[2]
    single_data_shape = input_shapes[3]

    assert type_round_data_shape[0] == type_data_shape[0]
    assert type_data_shape[0] == round_data_shape[0]
    assert round_data_shape[0] == single_data_shape[0]

    assert type_round_data_shape[1] == self.types_dim
    assert type_round_data_shape[2] == self.rounds_dim
    assert round_data_shape[1] == self.rounds_dim
    assert type_data_shape[1] == self.types_dim

    super(GameactionToGamestate, self).build(input_shapes)

  def compute_output_shape(self, input_shapes):
    return input_shapes

  def call(self, inputs):
    type_round_data = inputs[0]
    type_data = inputs[1]
    round_data = inputs[2]
    single_data = inputs[3]

    # 1
    my_values = type_data[:,:,0:1]
    his_values = type_data[:,:,1:2]
    counts = type_data[:,:,2:3]
    remains_type_data = type_data[:,:,3:]

    #region blind mode vs cheater mode
    if self.is_no_cheat:
      his_values = K.random_normal(his_values.shape)
    #endregion blind mode vs cheater mode

    gamestatish_type_data = K.concatenate([my_values, his_values, counts, remains_type_data], axis=-1)


    # 2
    his_steps = type_round_data[:,:,:,0:1]
    my_steps = type_round_data[:,:,:,1:2]
    remains_type_round_data = type_round_data[:,:,:,2:]

    rolled_my_steps = tf.manip.roll(my_steps, shift=1, axis=1) # axis types
    #rolled_my_first_step = rolled_my_steps[:,:,0:1,:]
    counts_as_first_rolled_wrapcleared_step = K.expand_dims(counts, axis=-2)
    # [batch_dim, types_dim, 1] = > [batch_dim, types_dim, 1, 1]

    rolled_my_remains_steps = rolled_my_steps[:,:,1:,:]
    rolled_wrapcleared_my_steps = K.concatenate([counts_as_first_rolled_wrapcleared_step, rolled_my_remains_steps], axis=-2)

    gamestatish_type_round_data = K.concatenate([rolled_wrapcleared_my_steps, his_steps, remains_type_round_data], axis=-1)


    # 3
    semi_ids = round_data[:,:,0:1]
    remains_round_data = round_data[:,:,1:]
    semi_ids = K.clip(semi_ids - 1/9, 0, None)

    gamestatish_round_data = K.concatenate([semi_ids, remains_round_data], axis=-1)



    return [gamestatish_type_round_data, gamestatish_type_data, gamestatish_round_data, single_data]

