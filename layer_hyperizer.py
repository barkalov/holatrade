
import keras.backend as K
import tensorflow as tf
import numpy as np
from keras.engine.topology import Layer

class Hyperizer(Layer):

  def __init__(self, types_dim, rounds_dim, is_do_enrich = True, is_oldschool = False, **kwargs):
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim
    self.is_do_enrich = is_do_enrich
    self.is_oldschool = is_oldschool
    super(Hyperizer, self).__init__(**kwargs)

  def build(self, input_shapes):
    type_round_data_shape = input_shapes[0]
    type_data_shape = input_shapes[1]
    round_data_shape = input_shapes[2]
    single_data_shape = input_shapes[3]

    assert type_round_data_shape[0] == type_data_shape[0]
    assert type_data_shape[0] == round_data_shape[0]
    assert round_data_shape[0] == single_data_shape[0]

    assert type_round_data_shape[1] == self.types_dim
    assert type_round_data_shape[2] == self.rounds_dim
    assert round_data_shape[1] == self.rounds_dim
    assert type_data_shape[1] == self.types_dim

    super(Hyperizer, self).build(input_shapes)

  def compute_output_shape(self, input_shapes):
    type_round_data_shape = input_shapes[0]
    type_data_shape = input_shapes[1]
    round_data_shape = input_shapes[2]
    single_data_shape = input_shapes[3]

    batch_dim = type_round_data_shape[0]

    type_round_data_channels_dim = type_round_data_shape[-1]
    type_data_channels_dim = type_data_shape[-1]
    round_data_channels_dim = round_data_shape[-1]
    single_data_channels_dim = single_data_shape[-1]

    if self.is_do_enrich:
      channels_dim = type_round_data_channels_dim * 3 + type_data_channels_dim * 3 + round_data_channels_dim + single_data_channels_dim
    else:
      channels_dim = type_round_data_channels_dim + type_data_channels_dim + round_data_channels_dim + single_data_channels_dim

    return (batch_dim, self.types_dim, self.rounds_dim, channels_dim)

  def enrich_via_tf(self, data, oldschool_poor_data = None):
    
    
    data_a = K.concatenate([data[:,1:2],data[:,2:3], data[:,0:1]], axis = 1)
    data_b = K.concatenate([data[:,2:3],data[:,0:1], data[:,1:2]], axis = 1)


    #data_a = tf.manip.roll(data, shift=1, axis=1) # axis types
    #data_b = tf.manip.roll(data, shift=-1, axis=1) # axis types
    data_sum = (data_a + data_b)
    data_absdiff = K.abs(data_a - data_b)

    if oldschool_poor_data is not None: #TODO: remove oldschool stuff after all goes ok
      data = oldschool_poor_data

    enriched_data = K.concatenate([data, data_sum, data_absdiff], axis = -1)
    return enriched_data

  def call(self, inputs):
    type_round_data = inputs[0]
    type_data = inputs[1]
    round_data = inputs[2]
    single_data = inputs[3]
    # orch means original channel

    #region type
    hyperized_type_data = K.expand_dims(type_data, axis=-2)
    # [batch_dim, types_dim, orch_dim] = > [batch_dim, types_dim, 1, orch_dim]
    hyperized_type_data = K.repeat_elements(hyperized_type_data, rep=self.rounds_dim, axis=-2)
    # [batch_dim, types_dim, 1, orch_dim] = > [batch_dim, types_dim, rounds_dim, orch_dim]
    #endregion type

    #region round
    hyperized_round_data = K.expand_dims(round_data, axis=-3)
    # [batch_dim, rounds_dim, orch_dim] = > [batch_dim, 1, rounds_dim, orch_dim]
    hyperized_round_data = K.repeat_elements(hyperized_round_data, rep=self.types_dim, axis=-3)
    # [batch_dim, 1, rounds_dim, orch_dim] = > [batch_dim, types_dim, rounds_dim, orch_dim]
    #endregion round

    #region single
    hyperized_single_data = K.expand_dims(single_data, axis=-2)
    # [batch_dim, orch_dim] = > [batch_dim, 1, orch_dim]
    hyperized_single_data = K.repeat_elements(hyperized_single_data, rep=self.rounds_dim, axis=-2)
    # [batch_dim, 1, orch_dim] = > [batch_dim, rounds_dim, orch_dim]
    hyperized_single_data = K.expand_dims(hyperized_single_data, axis=-3)
    # [batch_dim, rounds_dim, orch_dim] = > [batch_dim, 1, rounds_dim, orch_dim]
    hyperized_single_data = K.repeat_elements(hyperized_single_data, rep=self.types_dim, axis=-3)
    # [batch_dim, 1, rounds_dim, orch_dim] = > [batch_dim, types_dim, singles_dim, orch_dim]
    #endregion single

    #region type&round
    hyperized_type_round_data = type_round_data
    # already [batch_dim, types_dim, singles_dim, orch_dim]
    #endregion type&round

    if self.is_oldschool: #TODO: remove oldschool stuff after all goes ok
      poor_data = K.concatenate([hyperized_type_round_data, hyperized_type_data, hyperized_round_data, hyperized_single_data], axis = -1)
      enrichable_data = K.concatenate([hyperized_type_round_data, hyperized_type_data], axis = -1)
      if self.is_do_enrich:
        oldschool_enriched_data = self.enrich_via_tf(enrichable_data, oldschool_poor_data = poor_data)
      return oldschool_enriched_data
    else:
      if self.is_do_enrich:
        hyperized_type_round_data = self.enrich_via_tf(hyperized_type_round_data)
        hyperized_type_data = self.enrich_via_tf(hyperized_type_data)
      return K.concatenate([hyperized_type_round_data, hyperized_type_data, hyperized_round_data, hyperized_single_data], axis = -1)

