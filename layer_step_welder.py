
import keras.backend as K
import tensorflow as tf
import numpy as np
from keras.engine.topology import Layer

class StepWelder(Layer):

  def __init__(self, types_dim, rounds_dim, **kwargs):
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim
    super(StepWelder, self).__init__(**kwargs)

  def build(self, input_shapes):
    type_round_data_shape = input_shapes[0]
    type_data_shape = input_shapes[1]
    sigmoid_welding_step_data_shape = input_shapes[2]

    assert type_round_data_shape[0] == type_data_shape[0]
    assert type_data_shape[0] == sigmoid_welding_step_data_shape[0]

    assert type_round_data_shape[1] == self.types_dim
    assert type_round_data_shape[2] == self.rounds_dim
    assert type_data_shape[1] == self.types_dim
    assert sigmoid_welding_step_data_shape[1] == self.types_dim

    super(StepWelder, self).build(input_shapes)

  def compute_output_shape(self, input_shapes):
    type_round_data_shape = input_shapes[0]

    return type_round_data_shape

  def call(self, inputs):
    #gamestate oraculer mode
    type_round_data = inputs[0]
    type_data = inputs[1]
    sigmoid_welding_step_data = inputs[2]

    counts = type_data[:,:,2:3]
    counts = K.expand_dims(counts, axis=-1)
    # [batch_dim, types_dim, 1] = > [batch_dim, types_dim, 1, 1]

    my_steps = type_round_data[:,:,:,0:1]
    his_steps = type_round_data[:,:,:,1:2]
    remains_type_round_data = type_round_data[:,:,:,2:]

    sigmoid_welding_step = K.expand_dims(sigmoid_welding_step_data, axis=-1)
    # [batch_dim, types_dim] = > [batch_dim, types_dim, 1]
    sigmoid_welding_step = K.expand_dims(sigmoid_welding_step, axis=-1)
    # [batch_dim, types_dim, 1] = > [batch_dim, types_dim, 1, 1]
    welding_step = sigmoid_welding_step * counts
    remains_steps = my_steps[:,:,:-1,:]
    my_steps = K.concatenate([remains_steps, welding_step], axis=-2)

    welded_type_round_data = K.concatenate([my_steps, his_steps, remains_type_round_data], axis=-1)

    return welded_type_round_data

