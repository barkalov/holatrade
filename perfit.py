import numpy as np
import json
def perfit(values_a, values_b, counts, avidity_log = 0, epsilon = 1e-2):
    tweaked_values_a = pow(2, avidity_log) * values_a
    tweaked_values_b = pow(2, -avidity_log) * values_b
    perfit_ratio_a = (tweaked_values_a + epsilon) / (tweaked_values_a + values_b + epsilon)
    perfit_ratio_b = (tweaked_values_b + epsilon) / (values_a + tweaked_values_b + epsilon)

    #region renorm (to remove epsilon shift)
    perfit_ratio_sum = perfit_ratio_a + perfit_ratio_b
    perfit_ratio_a /= perfit_ratio_sum
    perfit_ratio_b /= perfit_ratio_sum
    #endregion renorm (to remove epsilon shift)

    return perfit_ratio_a, perfit_ratio_b

def stocho_perfit(values_a, values_mean_b, values_std_1ctn_b, counts, avidity_log = 0, epsilon = 1e-2):
    perfit_ratio_running_sum_a = np.zeros(counts.shape)
    perfit_ratio_running_sum_b = np.zeros(counts.shape)
    iters_count = 33
    for i in range(0, iters_count):
        values_random_b = np.random.normal(values_mean_b, values_std_1ctn_b / counts)

        values_random_b = np.clip(values_random_b, 0, 10 / counts)
        perfit_ratio_a, perfit_ratio_b = perfit(values_a, values_random_b, counts, avidity_log = avidity_log, epsilon = epsilon)
        perfit_ratio_running_sum_a += perfit_ratio_a
        perfit_ratio_running_sum_b += perfit_ratio_b

    perfit_ratio_running_mean_a = perfit_ratio_running_sum_a / iters_count
    perfit_ratio_running_mean_b = perfit_ratio_running_sum_b / iters_count

    return perfit_ratio_running_mean_a, perfit_ratio_running_mean_b




def perfit_ultimate(offer_variants, values_a, values_b, counts, bias_ratio = 0.5, is_always_win_mode = False, stocho_level = 0):
    offer_variants_total_values_sum_a = np.sum(offer_variants * values_a, axis = 1)
    offer_variants_total_values_sum_b = np.sum((counts - offer_variants) * values_b, axis = 1)

    offer_variants_total_values_ratio_a = offer_variants_total_values_sum_a / 10
    offer_variants_total_values_ratio_b = offer_variants_total_values_sum_b / 10

    offer_variants_mark1 = offer_variants_total_values_ratio_a * bias_ratio + offer_variants_total_values_ratio_b * (1 - bias_ratio)
    offer_variants_mark2 = (offer_variants_total_values_ratio_a * offer_variants_total_values_ratio_b) ** (1/2)
    offer_variants_mark = offer_variants_mark1 * offer_variants_mark2
    offer_variants_mask = 1
    if is_always_win_mode:
        if bias_ratio < 0.5:
            offer_variants_mask = (offer_variants_total_values_sum_a >= offer_variants_total_values_sum_b) * 1
        elif bias_ratio > 0.5:
            offer_variants_mask = (offer_variants_total_values_sum_a <= offer_variants_total_values_sum_b) * 1
        else:
            print('Warning! Bias is strictly 0.5, while in always win mode. Who must always win?')

    offer_variants_mark = offer_variants_mark * offer_variants_mask
    if stocho_level > 0:
        offer_variants_mark = np.random.normal(offer_variants_mark, stocho_level)

    best_offer_id = np.argmax(offer_variants_mark * offer_variants_mask)
    perfit_a = offer_variants[best_offer_id]
    perfit_b = counts - offer_variants[best_offer_id]
    return perfit_a, perfit_b

def build_offer_variants(counts):
    offer_variants_list = []
    counts_left = counts * 1
    build_offer_variants_recursive(offer_variants_list, counts_left)
    offer_variants = np.unique(offer_variants_list, axis=0)
    return offer_variants

def build_offer_variants_recursive(offer_variants_list, counts_left):
    offer_variants_list.append(counts_left)
    if counts_left.sum() == 0:
        return
    else:
        for typesId in range(0, 3):
            if counts_left[typesId] > 0:
                counts_left_clone = counts_left * 1
                counts_left_clone[typesId] -= 1
                build_offer_variants_recursive(offer_variants_list, counts_left_clone)

######
###### test
######

counts = np.array([1,4,2])
values_a = np.array([1,1,1])
values_b = np.array([1,1,1])

print('ULTIMATE')
offer_variants = build_offer_variants(counts)
keep_a, keep_b = perfit_ultimate(offer_variants, values_a, values_b, counts, bias_ratio=0.5, stocho_level=0.05)
print('counts {}\n'.format(counts))
print('values [a] {}\nvalues [b] {}\n'.format(values_a, values_b))
print('keep [a] {} {}\nkeep [b] {} {}\n'.format(np.round(keep_a), keep_a, np.round(keep_b), keep_b))
print('keep tot_val_sum [a] {} {}\nkeep tot_val_sum [b] {} {}\n'.format((np.round(keep_a) * values_a).sum(), (keep_a * values_a).sum(), (np.round(keep_b) * values_b).sum(), (keep_b * values_b).sum()))
print('check keep  [a+b] {}\n'.format(keep_a + keep_b))

with open('./reference_generator.json') as f:
    reference_generator = json.load(f)

for generator_element in reference_generator:
    counts = np.array(generator_element['counts'])
    offer_variants = build_offer_variants(counts)
    generator_element['offer_variants'] = offer_variants.tolist()

with open('./reference_generator_with_offer_variants.json', 'w') as outfile:
    json.dump(reference_generator, outfile)
