import os

import keras
from keras.layers import Input, Concatenate
from keras.layers import BatchNormalization, Activation
from keras.layers.convolutional import Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.layers import Layer

import numpy as np
import json

class ReceptionUnit():

  def __init__(self, types_dim, rounds_dim, input_channels_dim, scale_factor, bn_training):
    self.class_name = 'reception'
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim
    self.input_channels_dim = input_channels_dim
    self.scale_factor = scale_factor
    self.bn_training = bn_training

    self.model = self.build_model()

  def build_model(self):
    model_input = Input(shape=(self.types_dim, self.rounds_dim, self.input_channels_dim))
    x = model_input

    # a
    x_ti = Conv2D(8 * self.scale_factor, kernel_size=(1, 1), name='reception_unit_conv_a')(x)
    x_ti = BatchNormalization(name='reception_unit_bn_a')(x_ti, training = self.bn_training)
    x_ti = Activation('elu')(x_ti)

    # b
    x_t = Conv2D(16 * self.scale_factor, kernel_size=(self.types_dim, 1), padding='valid', name='reception_unit_conv_b')(x_ti)
    x_t = BatchNormalization(name='reception_unit_bn_b')(x_t, training = self.bn_training)
    x_t = Activation('elu')(x_t)
    x_t = UpSampling2D(size=(self.types_dim, 1))(x_t)

    # c
    x_ri = Conv2D(8 * self.scale_factor, kernel_size=(1, 1), name='reception_unit_conv_c')(x)
    x_ri = BatchNormalization(name='reception_unit_bn_c')(x_ri, training = self.bn_training)
    x_ri = Activation('elu')(x_ri)

    # d
    x_r = Conv2D(16 * self.scale_factor, kernel_size=(1, self.rounds_dim), padding='valid', name='reception_unit_conv_d')(x_ri)
    x_r = BatchNormalization(name='reception_unit_bn_d')(x_r, training = self.bn_training)
    x_r = Activation('elu')(x_r)
    x_r = UpSampling2D(size=(1, self.rounds_dim))(x_r)

    x = Concatenate()([x, x_t, x_r])

    model_output = x
    model = Model(model_input, model_output)
    print('====| reception_unit |====')
    model.summary()
    return model

  def export_setup(self):
    conv_a_weights = self.model.layers[1].get_weights()
    bn_a_weights = self.model.layers[3].get_weights()

    conv_b_weights = self.model.layers[7].get_weights()
    bn_b_weights = self.model.layers[9].get_weights()

    conv_c_weights = self.model.layers[2].get_weights()
    bn_c_weights = self.model.layers[4].get_weights()

    conv_d_weights = self.model.layers[8].get_weights()
    bn_d_weights = self.model.layers[10].get_weights()

    # a
    conv_a_kernel = conv_a_weights[0]
    conv_a_bias = conv_a_weights[1]
    bn_a_gamma = bn_a_weights[0]
    bn_a_beta = bn_a_weights[1]
    bn_a_mean = bn_a_weights[2]
    bn_a_variance = bn_a_weights[3]

    # b
    conv_b_kernel = conv_b_weights[0]
    conv_b_bias = conv_b_weights[1]
    bn_b_gamma = bn_b_weights[0]
    bn_b_beta = bn_b_weights[1]
    bn_b_mean = bn_b_weights[2]
    bn_b_variance = bn_b_weights[3]

    # c
    conv_c_kernel = conv_c_weights[0]
    conv_c_bias = conv_c_weights[1]
    bn_c_gamma = bn_c_weights[0]
    bn_c_beta = bn_c_weights[1]
    bn_c_mean = bn_c_weights[2]
    bn_c_variance = bn_c_weights[3]

    # d
    conv_d_kernel = conv_d_weights[0]
    conv_d_bias = conv_d_weights[1]
    bn_d_gamma = bn_d_weights[0]
    bn_d_beta = bn_d_weights[1]
    bn_d_mean = bn_d_weights[2]
    bn_d_variance = bn_d_weights[3]

    setup = {
      'className': self.class_name,
      'className': self.class_name,
      'typesDim': self.types_dim,
      'roundsDim': self.rounds_dim,
      'inputChannelsDim': self.input_channels_dim,
      'scaleFactor': self.scale_factor,

      #a
      'convKernelA': conv_a_kernel.flatten().tolist(),
      'convBiasA': conv_a_bias.flatten().tolist(),
      'bnMeanA': bn_a_mean.flatten().tolist(),
      'bnVarianceA': bn_a_variance.flatten().tolist(),
      'bnBetaA': bn_a_beta.flatten().tolist(),
      'bnGammaA': bn_a_gamma.flatten().tolist(),

      #b
      'convKernelB': conv_b_kernel.flatten().tolist(),
      'convBiasB': conv_b_bias.flatten().tolist(),
      'bnMeanB': bn_b_mean.flatten().tolist(),
      'bnVarianceB': bn_b_variance.flatten().tolist(),
      'bnBetaB': bn_b_beta.flatten().tolist(),
      'bnGammaB': bn_b_gamma.flatten().tolist(),

      #c
      'convKernelC': conv_c_kernel.flatten().tolist(),
      'convBiasC': conv_c_bias.flatten().tolist(),
      'bnMeanC': bn_c_mean.flatten().tolist(),
      'bnVarianceC': bn_c_variance.flatten().tolist(),
      'bnBetaC': bn_c_beta.flatten().tolist(),
      'bnGammaC': bn_c_gamma.flatten().tolist(),

      #d
      'convKernelD': conv_d_kernel.flatten().tolist(),
      'convBiasD': conv_d_bias.flatten().tolist(),
      'bnMeanD': bn_d_mean.flatten().tolist(),
      'bnVarianceD': bn_d_variance.flatten().tolist(),
      'bnBetaD': bn_d_beta.flatten().tolist(),
      'bnGammaD': bn_d_gamma.flatten().tolist(),
    }
    return setup

  def export_setup_to_json(self, json_path):
    os.makedirs(os.path.dirname(json_path), exist_ok=True)
    setup = self.export_setup()
    with open(json_path, 'w') as outfile:
      json.dump(setup, outfile)

  def add_to_stack(self, source, master_source):
    return self.model(source)

if __name__ == '__main__':
  test_input_batch = np.linspace(0, 1, 1 * 3 * 6 * 7)
  test_input_batch = test_input_batch.reshape((1, 3, 6, 7))
  reception_unit = ReceptionUnit(3, 6, 7, 3, None)
  reception_unit.export_setup_to_json('./exported_model/reception_test/setup.json')
  test_pred = reception_unit.model.predict(test_input_batch)
  print('test_pred ch 0', test_pred[0,:,:,0])
  print('test_pred ch 1', test_pred[0,:,:,1])
  print('test_pred ch 2', test_pred[0,:,:,2])
  print('test_pred ch 3', test_pred[0,:,:,3])
  print('test_pred shape', test_pred.shape)
  print('test_pred fl', test_pred.flatten())
  print('test_pred fl len', len(test_pred.flatten()))
  print('test_pred sum', test_pred.sum())
  print('end')
