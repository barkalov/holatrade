from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-tn", "--tour-name", dest='tour_name',help="tour name")
parser.add_argument("-k", "--kilos", dest='kilos',help="1k of games per one kilo")
args = parser.parse_args()
assert(args.tour_name)
assert(args.kilos)

import numpy as np
from hahaggle import Hahaggle
from agents import FairCheaterAgent, CountieAgent, RefCheaterAgent, FairNoisedCheaterAgent, RandieAgent, SimpleAgent, GreedieAgent, Greedie2Agent, Greedie3Agent, LoosieAgent, RobberAgent, LinishAgent, SmartAgent, RaiserAgent
from superagents import BrandieSuperagent
from agents_avidity_factory import AvidityAgentFactory
AvidityAgent = AvidityAgentFactory()

#from chappie_superagents import ChappieMGZAASuperagent
from chappie_superagents import ChappieMGXAASuperagent
# from chappie_superagents import ChappieMGXABSuperagent
#from chappie_superagents import ChappieMGXACSuperagent
# from chappie_superagents import ChappieMGSAASuperagent
# from chappie_superagents import ChappieMGXBASuperagent
# from chappie_superagents import ChappieMGXCASuperagent
# from chappie_superagents import ChappieMGXDASuperagent
# uac skip
#from chappie_superagents import ChappieMGUAASuperagent
from chappie_superagents import ChappieMGUBDSuperagent
from chappie_superagents import ChappieMGWAASuperagent

#tour_name = 'tour_sigma10_xparty_level01_run01_100k'
tour_name = args.tour_name + '_'+ args.kilos + 'k'
print('tour_name', tour_name)



hahaggle = Hahaggle([
  #BrandieSuperagent, BrandieSuperagent, RandieAgent, FairNoisedCheaterAgent
  #FairCheaterAgent,
  ChappieMGXAASuperagent,
  ChappieMGUBDSuperagent,
  ChappieMGWAASuperagent,
  FairCheaterAgent, FairNoisedCheaterAgent, RefCheaterAgent, AvidityAgent, CountieAgent, RandieAgent, SimpleAgent, GreedieAgent, Greedie2Agent, Greedie3Agent, LoosieAgent, RobberAgent, LinishAgent, SmartAgent, RaiserAgent,
  BrandieSuperagent,
], name = tour_name)
qute_table, stat_tournament, gamereplay_dataset = hahaggle.play_tournament(games_count = int(args.kilos) * 1000)
np.save('stat_tournament/'+ tour_name + '.npy', stat_tournament)

