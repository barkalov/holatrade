from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-tn", "--tour-name", dest='tour_name',help="tour name")
args = parser.parse_args()
#args.tour_name = 'tour_sigma25_randieparty_refgen0_500k'
assert(args.tour_name)

import numpy as np
from gameaction_dataset_element import GameactionDatasetElement
tour_name = args.tour_name


print('tour_name', tour_name)
stat_tournament = np.load('./stat_tournament/' + tour_name + '.npy')
print('stat_tournament.shape', stat_tournament.shape)

def stat_tournament_to_various_dataset_ios(stat_tournament, from_id = 0, to_id = None, is_verbose = False):
    if to_id is None: to_id = stat_tournament.shape[0]

    assert from_id >= 0
    assert to_id > from_id
    assert to_id <= stat_tournament.shape[0]

    if is_verbose:
        verbose = print
    else:
        verbose = lambda *args: None

    metapenalty_oraculer_gameaction_outputs = []

    for game_num in range(from_id, to_id):

        ## building one game dataset
        stat_counts = stat_tournament[game_num,0]
        verbose('counts: {}'.format(stat_counts))
        stat_values_pair = stat_tournament[game_num,1]
        stat_values_a = stat_values_pair[0]
        stat_values_b = stat_values_pair[1]
        verbose('values: A: {} B: {}'.format(stat_values_a, stat_values_b))

        gameaction_dataset_elements_pair  = stat_tournament[game_num, 3]
        gameaction_dataset_element_a = gameaction_dataset_elements_pair[0]
        gameaction_dataset_element_b = gameaction_dataset_elements_pair[1]

        stat_score_pair = stat_tournament[game_num,4]
        stat_score_a = stat_score_pair[0]
        stat_score_b = stat_score_pair[1]
        verbose('stat_score: A: {} B: {}'.format(stat_score_a, stat_score_b))


        #region a
        verbose('== vay it looks for a ==')

        verbose('= gameaction =')

        metapenalty_oraculer_gameaction_target = gameaction_dataset_element_a.export_metapenalty_oraculer_gameaction_outputs()
        metapenalty_oraculer_gameaction_outputs.append(metapenalty_oraculer_gameaction_target)


        verbose('(his) b_steps_as_a_see', gameaction_dataset_element_a.his_steps_as_i_see)
        verbose('(me) a_steps_as_a_see', gameaction_dataset_element_a.my_steps_as_i_see)
        verbose('semi_ids', gameaction_dataset_element_a.semi_ids)
        verbose('my_values', gameaction_dataset_element_a.my_values)
        verbose('ground truth score_pair', gameaction_dataset_element_a.score_pair)
        verbose('is_a', gameaction_dataset_element_a.is_a)
        verbose('counts', gameaction_dataset_element_a.counts)
        #endregion a

        #region b
        verbose('== vay it looks for b ==')


        verbose('= gameaction =')

        metapenalty_oraculer_gameaction_target = gameaction_dataset_element_b.export_metapenalty_oraculer_gameaction_outputs()
        metapenalty_oraculer_gameaction_outputs.append(metapenalty_oraculer_gameaction_target)


        verbose('(his) a_steps_ab_b_see', gameaction_dataset_element_b.his_steps_as_i_see)
        verbose('(me) b_steps_as_b_see', gameaction_dataset_element_b.my_steps_as_i_see)
        verbose('semi_ids', gameaction_dataset_element_b.semi_ids)
        verbose('my_values', gameaction_dataset_element_b.my_values)
        verbose('ground truth score_pair', gameaction_dataset_element_b.score_pair)
        verbose('is_a', gameaction_dataset_element_b.is_a)
        verbose('counts', gameaction_dataset_element_b.counts)
        #endregion b


    metapenalty_oraculer_gameaction_outputs = np.array(metapenalty_oraculer_gameaction_outputs)

    return metapenalty_oraculer_gameaction_outputs

##
## Entrypoint
##

#### builiding unnormalized inputs and outputs
normalized_metapenalty_oraculer_gameaction_outputs = stat_tournament_to_various_dataset_ios(stat_tournament)


#########
######### gameaction
#########
print('=== gameaction ===')




######
###### metapenalty_oraculer
######

#### printing one sample to checkout
sample_id = 6
print('== denormalized metapenalty_oraculer sample ==', sample_id)
normalized_target = normalized_metapenalty_oraculer_gameaction_outputs[sample_id]
denormalized_target = GameactionDatasetElement.denormalize_metapenalty_oraculer_gameaction_outputs(normalized_target)

print('denormalized_target:')
print(denormalized_target)

print('== normalization stat ==')
print('out min:', normalized_metapenalty_oraculer_gameaction_outputs.min())
print('out max:', normalized_metapenalty_oraculer_gameaction_outputs.max())
print('out mean:', normalized_metapenalty_oraculer_gameaction_outputs.mean())
print('out std:', normalized_metapenalty_oraculer_gameaction_outputs.std())

#### save prepared dataset
#np.save('./stat_tournament/' + tour_name + '_to_metapenalty_m1_oraculer_gameaction_dataset_inputs.npy', normalized_metapenalty_oraculer_gameaction_inputs)
#np.save('./stat_tournament/' + tour_name + '_to_metapenalty_m2_oraculer_gameaction_dataset_outputs.npy', normalized_metapenalty_oraculer_gameaction_outputs)
np.save('./stat_tournament/' + tour_name + '_to_metapenalty_m3_oraculer_gameaction_dataset_outputs.npy', normalized_metapenalty_oraculer_gameaction_outputs)





