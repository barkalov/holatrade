from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-tn", "--tour-name", dest='tour_name',help="tour name")
args = parser.parse_args()
assert(args.tour_name)

import numpy as np
from gameaction_dataset_element import GameactionDatasetElement
#tour_name = 'tour_sigma99_nochappies0_500k'
#tour_name = 'tour_sigma25_randieparty_refgen4_500k'
#tour_name = 'tour_sigma25_xparty_run1_100k'
#tour_name = 'tour_sigma10_xparty_level01_comp00_100k'
tour_name = args.tour_name


print('tour_name', tour_name)
#### load tournament stat data, generated by hahaggle.play_tournament()
stat_tournament = np.load('./stat_tournament/' + tour_name + '.npy')
print('stat_tournament.shape', stat_tournament.shape)


##
## Remark:
## We will capture even a final state, where are no place for further decision, game is done at that point,
## but it's still an eatable portion of learning data
## with (never reachable in wildlife) states of offers: converged offers (extreme narrow) or timeouted offers (mostly wide)
##
## Maybe learning that dead ends it's not a good idea. But as a first assumtion - let's try to use them!
##

#### builiding unnormalized inputs and outputs
def stat_tournament_to_various_dataset_ios(stat_tournament, from_id = 0, to_id = None, is_verbose = False):
    if to_id is None: to_id = stat_tournament.shape[0]

    assert from_id >= 0
    assert to_id > from_id
    assert to_id <= stat_tournament.shape[0]

    if is_verbose:
        verbose = print
    else:
        verbose = lambda *args: None

    score_guesser_gameaction_inputs = []
    score_guesser_gameaction_outputs = []

    metapenalty_oraculer_gameaction_inputs = []
    metapenalty_oraculer_gameaction_outputs = []

    for game_num in range(from_id, to_id):

        ## building one game dataset
        stat_counts = stat_tournament[game_num,0]
        verbose('counts: {}'.format(stat_counts))
        stat_values_pair = stat_tournament[game_num,1]
        stat_values_a = stat_values_pair[0]
        stat_values_b = stat_values_pair[1]
        verbose('values: A: {} B: {}'.format(stat_values_a, stat_values_b))

        gameaction_dataset_elements_pair  = stat_tournament[game_num, 3]
        gameaction_dataset_element_a = gameaction_dataset_elements_pair[0]
        gameaction_dataset_element_b = gameaction_dataset_elements_pair[1]

        stat_score_pair = stat_tournament[game_num,4]
        stat_score_a = stat_score_pair[0]
        stat_score_b = stat_score_pair[1]
        verbose('stat_score: A: {} B: {}'.format(stat_score_a, stat_score_b))


        #region a
        verbose('== vay it looks for a ==')

        verbose('= gameaction =')

        score_guesser_gameaction_layers, score_guesser_gameaction_target = gameaction_dataset_element_a.export_score_guesser_gameaction()
        score_guesser_gameaction_inputs.append(score_guesser_gameaction_layers)
        score_guesser_gameaction_outputs.append(score_guesser_gameaction_target)

        metapenalty_oraculer_gameaction_layers, metapenalty_oraculer_gameaction_target = gameaction_dataset_element_a.export_metapenalty_oraculer_gameaction()
        metapenalty_oraculer_gameaction_inputs.append(metapenalty_oraculer_gameaction_layers)
        metapenalty_oraculer_gameaction_outputs.append(metapenalty_oraculer_gameaction_target)


        verbose('(his) b_steps_as_a_see', gameaction_dataset_element_a.his_steps_as_i_see)
        verbose('(me) a_steps_as_a_see', gameaction_dataset_element_a.my_steps_as_i_see)
        verbose('semi_ids', gameaction_dataset_element_a.semi_ids)
        verbose('my_values', gameaction_dataset_element_a.my_values)
        verbose('ground truth score_pair', gameaction_dataset_element_a.score_pair)
        verbose('is_a', gameaction_dataset_element_a.is_a)
        verbose('counts', gameaction_dataset_element_a.counts)
        #endregion a

        #region b
        verbose('== vay it looks for b ==')


        verbose('= gameaction =')

        score_guesser_gameaction_layers, score_guesser_gameaction_target = gameaction_dataset_element_b.export_score_guesser_gameaction()
        score_guesser_gameaction_inputs.append(score_guesser_gameaction_layers)
        score_guesser_gameaction_outputs.append(score_guesser_gameaction_target)

        metapenalty_oraculer_gameaction_layers, metapenalty_oraculer_gameaction_target = gameaction_dataset_element_b.export_metapenalty_oraculer_gameaction()
        metapenalty_oraculer_gameaction_inputs.append(metapenalty_oraculer_gameaction_layers)
        metapenalty_oraculer_gameaction_outputs.append(metapenalty_oraculer_gameaction_target)


        verbose('(his) a_steps_ab_b_see', gameaction_dataset_element_b.his_steps_as_i_see)
        verbose('(me) b_steps_as_b_see', gameaction_dataset_element_b.my_steps_as_i_see)
        verbose('semi_ids', gameaction_dataset_element_b.semi_ids)
        verbose('my_values', gameaction_dataset_element_b.my_values)
        verbose('ground truth score_pair', gameaction_dataset_element_b.score_pair)
        verbose('is_a', gameaction_dataset_element_b.is_a)
        verbose('counts', gameaction_dataset_element_b.counts)
        #endregion b


    score_guesser_gameaction_inputs = np.array(score_guesser_gameaction_inputs)
    score_guesser_gameaction_outputs = np.array(score_guesser_gameaction_outputs)

    metapenalty_oraculer_gameaction_inputs = np.array(metapenalty_oraculer_gameaction_inputs)
    metapenalty_oraculer_gameaction_outputs = np.array(metapenalty_oraculer_gameaction_outputs)

    return score_guesser_gameaction_inputs, score_guesser_gameaction_outputs, metapenalty_oraculer_gameaction_inputs, metapenalty_oraculer_gameaction_outputs

##
## Entrypoint
##

#### builiding unnormalized inputs and outputs
normalized_score_guesser_gameaction_inputs, normalized_score_guesser_gameaction_outputs, normalized_metapenalty_oraculer_gameaction_inputs, normalized_metapenalty_oraculer_gameaction_outputs = stat_tournament_to_various_dataset_ios(stat_tournament)


#########
######### gameaction
#########
print('=== gameaction ===')




######
###### score_guesser
######

#### printing one sample to checkout
sample_id = 6
print('== denormalized score_guesser sample ==', sample_id)
normalized_layers = normalized_score_guesser_gameaction_inputs[sample_id]
normalized_target = normalized_score_guesser_gameaction_outputs[sample_id]
denormalized_layers = GameactionDatasetElement.denormalize_score_guesser_gameaction_inputs(normalized_layers)
denormalized_target = GameactionDatasetElement.denormalize_score_guesser_gameaction_outputs(normalized_target)

print('denormalized_his_steps_as_i_see:')
print(denormalized_layers[:, :, 0])
print('denormalized_my_steps_as_i_see:')
print(denormalized_layers[:, :, 1])
print('denormalized_my_values:')
print(denormalized_layers[:, :, 2])
print('denormalized_counts:')
print(denormalized_layers[:, :, 3])
print('denormalized_semi_ids:')
print(denormalized_layers[:, :, 4])
print('denormalized_is_a:')
print(denormalized_layers[:, :, 5])
print('denormalized_target:')
print(denormalized_target)

print('== normalization stat ==')
print('in min per chan:', normalized_score_guesser_gameaction_inputs.min(axis=(0,1,2)))
print('in max per chan:', normalized_score_guesser_gameaction_inputs.max(axis=(0,1,2)))
print('in mean per chan:', normalized_score_guesser_gameaction_inputs.mean(axis=(0,1,2)))
print('in std per chan:', normalized_score_guesser_gameaction_inputs.std(axis=(0,1,2)))

print('out min:', normalized_score_guesser_gameaction_outputs.min())
print('out max:', normalized_score_guesser_gameaction_outputs.max())
print('out mean:', normalized_score_guesser_gameaction_outputs.mean())
print('out std:', normalized_score_guesser_gameaction_outputs.std())

#### save prepared dataset
np.save('./stat_tournament/' + tour_name + '_to_score_guesser_gameaction_dataset_inputs.npy', normalized_score_guesser_gameaction_inputs)


######
###### metapenalty_oraculer
######

#### printing one sample to checkout
sample_id = 6
print('== denormalized metapenalty_oraculer sample ==', sample_id)
normalized_layers = normalized_metapenalty_oraculer_gameaction_inputs[sample_id]
normalized_target = normalized_metapenalty_oraculer_gameaction_outputs[sample_id]
denormalized_layers = GameactionDatasetElement.denormalize_metapenalty_oraculer_gameaction_inputs(normalized_layers)
denormalized_target = GameactionDatasetElement.denormalize_metapenalty_oraculer_gameaction_outputs(normalized_target)

print('denormalized_his_steps_as_i_see:')
print(denormalized_layers[:, :, 0])
print('denormalized_my_steps_as_i_see:')
print(denormalized_layers[:, :, 1])
print('denormalized_my_values:')
print(denormalized_layers[:, :, 2])
print('denormalized_his_(cheated)_values:')
print(denormalized_layers[:, :, 3])
print('denormalized_counts:')
print(denormalized_layers[:, :, 4])
print('denormalized_semi_ids:')
print(denormalized_layers[:, :, 5])
print('denormalized_is_a:')
print(denormalized_layers[:, :, 6])
print('denormalized_target:')
print(denormalized_target)

print('== normalization stat ==')
print('in min per chan:', normalized_metapenalty_oraculer_gameaction_inputs.min(axis=(0,1,2)))
print('in max per chan:', normalized_metapenalty_oraculer_gameaction_inputs.max(axis=(0,1,2)))
print('in mean per chan:', normalized_metapenalty_oraculer_gameaction_inputs.mean(axis=(0,1,2)))
print('in std per chan:', normalized_metapenalty_oraculer_gameaction_inputs.std(axis=(0,1,2)))

print('out min:', normalized_metapenalty_oraculer_gameaction_outputs.min())
print('out max:', normalized_metapenalty_oraculer_gameaction_outputs.max())
print('out mean:', normalized_metapenalty_oraculer_gameaction_outputs.mean())
print('out std:', normalized_metapenalty_oraculer_gameaction_outputs.std())

#### save prepared dataset
np.save('./stat_tournament/' + tour_name + '_to_metapenalty_m1_oraculer_gameaction_dataset_outputs.npy', normalized_metapenalty_oraculer_gameaction_outputs)
#np.save('./stat_tournament/' + tour_name + '_to_metapenalty_m2_oraculer_gameaction_dataset_outputs.npy', normalized_metapenalty_oraculer_gameaction_outputs)
#np.save('./stat_tournament/' + tour_name + '_to_metapenalty_m3_oraculer_gameaction_dataset_outputs.npy', normalized_metapenalty_oraculer_gameaction_outputs)





