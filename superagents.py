import numpy as np
import math
import json
with open('./reference_generator_with_offer_variants.json') as f:
    reference_generator_with_offer_variants = json.load(f)

from agents import AbstractStateactionedAgent
from perfit import perfit_ultimate

class BrandieSuperagent(AbstractStateactionedAgent):
  agent_name = 'BrandieSuperagent'

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.base_acceptance_propability = 0.8
    self.base_acceptance_sweetspot_sum = 6.3
    self.base_harden_factor = 2
    counts_as_list = self.counts.tolist()
    self.offer_variants = np.array([x for x in reference_generator_with_offer_variants if x['counts'] == counts_as_list][0]['offer_variants'])


  def calc_incoming_offer_accept_propability(self, incoming_offer_sum, rounds_left):
    if incoming_offer_sum == 0:
      return 0.0
    propability_for_game_exp = math.exp((incoming_offer_sum - self.base_acceptance_sweetspot_sum) * self.base_harden_factor)
    propability_for_game = ( propability_for_game_exp ) / ( propability_for_game_exp + 1 ) #pure sigmoid, wolfram: Plot[(e^(x - 5))/((e^(x - 5))+1), {x, 0, 10}]
    propability_for_game_base_aceptance_power = math.log(self.base_acceptance_propability, 0.5)
    propability_for_game_based = propability_for_game ** propability_for_game_base_aceptance_power
    propability_for_round_based = propability_for_game_based ** rounds_left
    return propability_for_round_based

  def calc_outgoing_offer(self, rounds_left):
    values_epsilon = 0.1
    uniform3 = np.random.uniform(0, 1, 3)
    giveaway_propability_for_game = uniform3 ** (self.values + values_epsilon)
    giveaway_propability_for_game_base_aceptance_power = math.log(self.base_acceptance_propability, 0.5)
    giveaway_propability_for_game_based = giveaway_propability_for_game ** giveaway_propability_for_game_base_aceptance_power
    giveaway_propability_for_round_based = giveaway_propability_for_game_based ** (rounds_left + 1)
    outgoing_random_offer = self.counts - giveaway_propability_for_round_based * self.counts
    outgoing_perfit_offer, _ = perfit_ultimate(self.offer_variants, self.values, self.cheat_opponent_values, self.counts, bias_ratio=0.49 ** rounds_left, is_always_win_mode=True, stocho_level=0.1)

    mix_ratio = np.random.uniform(0, 1, 3)
    outgoing_offer = outgoing_random_offer * mix_ratio + outgoing_perfit_offer * (1 - mix_ratio)

    #print('offer generated sum: ', np.sum(outgoing_offer * self.values))
    return np.round(outgoing_offer)

  def internal_offer_implementation(self, o):
    incoming_offer_sum = np.sum(o * self.values)
    rounds_left =  self.last_round - self.current_round + 1
    propability_for_round = self.calc_incoming_offer_accept_propability(incoming_offer_sum, rounds_left)
    if np.random.uniform() < propability_for_round:
      return o * 1
    else:
      outgoing_offer = self.calc_outgoing_offer(rounds_left)
      if outgoing_offer.sum() == 0:
        outgoing_offer = np.zeros(self.values.shape)
        outgoing_offer[self.values.argmax()] = 1
      return outgoing_offer


