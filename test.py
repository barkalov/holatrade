import numpy as np


def dbg_guesser_hypecube_to_multiple_inputs(hypercube):
  type_round_data = hypercube[:, :, 0:2]
  type_data = hypercube[:, 0, 2:4]
  round_data = hypercube[0, :, 4:5]
  single_data = hypercube[0, 0, 5:6]
  return type_round_data, type_data, round_data, single_data

def dbg_oraculer_hypecube_to_multiple_inputs(hypercube):
  type_round_data = hypercube[:, :, 0:2]
  type_data = hypercube[:, 0, 2:5]
  round_data = hypercube[0, :, 5:6]
  single_data = hypercube[0, 0, 6:7]
  return type_round_data, type_data, round_data, single_data

def test_guesser(old_state_hypercube, new_state_inputs):
  print('testing guesser...')
  old_state_type_round_data, \
  old_state_type_data, \
  old_state_round_data, \
  old_state_single_data = dbg_guesser_hypecube_to_multiple_inputs(old_state_hypercube)

  new_state_type_round_data, \
  new_state_type_data, \
  new_state_round_data, \
  new_state_single_data = new_state_inputs

  assert np.all(old_state_type_round_data == new_state_type_round_data)
  assert np.all(old_state_type_data == new_state_type_data)
  assert np.all(old_state_round_data == new_state_round_data)
  assert np.all(old_state_single_data == new_state_single_data)

  print('guesser tested and matched perfectly...')
  return True

def test_oraculer(old_state_hypercube, new_state_inputs):
  print('testing oraculer...')
  old_state_type_round_data, \
  old_state_type_data, \
  old_state_round_data, \
  old_state_single_data = dbg_oraculer_hypecube_to_multiple_inputs(old_state_hypercube)

  new_state_type_round_data, \
  new_state_type_data, \
  new_state_round_data, \
  new_state_single_data = new_state_inputs

  assert np.all(old_state_type_round_data == new_state_type_round_data)
  assert np.all(old_state_type_data == new_state_type_data)
  assert np.all(old_state_round_data == new_state_round_data)
  assert np.all(old_state_single_data == new_state_single_data)

  print('oraculer tested and matched perfectly...')
  return True