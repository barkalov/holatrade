import os

import keras
from keras.layers import Input
from keras.models import Model

import numpy as np
import json
from hyception_unit import HyceptionUnit
from ception_unit import CeptionUnit
from reception_unit import ReceptionUnit
from finception_unit import FinceptionUnit

class UnitStack():

  def __init__(self, units, types_dim, rounds_dim, type_round_channels_dim, type_channels_dim, round_channels_dim, single_channels_dim):
    self.units = units
    self.types_dim = types_dim
    self.rounds_dim = rounds_dim

    self.type_round_channels_dim = type_round_channels_dim
    self.type_channels_dim = type_channels_dim
    self.round_channels_dim = round_channels_dim
    self.single_channels_dim = single_channels_dim

    self.model = self.build_model()

  def build_model(self):
    hypermodel_type_round_data_input = Input(shape=(self.types_dim, self.rounds_dim, self.type_round_channels_dim))
    hypermodel_type_data_input = Input(shape=(self.types_dim, self.type_channels_dim))
    hypermodel_round_data_input = Input(shape=(self.rounds_dim, self.round_channels_dim))
    hypermodel_single_data_input = Input(shape=(self.single_channels_dim, ))
    model_inputs = [
      hypermodel_type_round_data_input,
      hypermodel_type_data_input,
      hypermodel_round_data_input,
      hypermodel_single_data_input,
    ]

    master_source = None
    x = None
    for unit in self.units:
      if unit.class_name == 'hyception':
        master_source = unit.add_to_stack(model_inputs, None)
        x = master_source
      else:
        x = unit.add_to_stack(x, master_source)

    model_output = x
    model = Model(model_inputs, model_output)
    print('====| unit_stack |====')
    model.summary()
    return model

  def export_setups(self):
    setups = []
    for unit in self.units:
      setup = unit.export_setup()
      setups.append(setup)
    return setups

  def export_setups_to_json(self, json_path):
    os.makedirs(os.path.dirname(json_path), exist_ok=True)
    setups = self.export_setups()
    with open(json_path, 'w') as outfile:
      json.dump(setups, outfile)


if __name__ == '__main__':
  test_type_round_input_batch = np.linspace(0, 1, 1 * 3 * 6 * 9)
  test_type_round_input_batch = test_type_round_input_batch.reshape((1, 3, 6, 9))

  test_type_input_batch = np.linspace(0, 1, 3 * 1 * 8)
  test_type_input_batch = test_type_input_batch.reshape((1, 3, 8))

  test_round_input_batch = np.linspace(0, 1, 1 * 6 * 7)
  test_round_input_batch = test_round_input_batch.reshape((1, 6, 7))

  test_single_input_batch = np.linspace(0, 1, 1 * 6)
  test_single_input_batch = test_single_input_batch.reshape((1, 6))

  test_inputs_batch = [
    test_type_round_input_batch,
    test_type_input_batch,
    test_round_input_batch,
    test_single_input_batch,
  ]

  units = [
    HyceptionUnit(types_dim = 3, rounds_dim = 6, type_round_channels_dim = 9, type_channels_dim = 8, round_channels_dim = 7, single_channels_dim = 6),
    CeptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 9 * 3 + 8 * 3 + 7 + 6, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 13, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = None),
    ReceptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 13, scale_factor = 3, bn_training = None),
    CeptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 16 * 3 * 2 + 13, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 13, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = None),
    ReceptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 13, scale_factor = 3, bn_training = None),
    CeptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 16 * 3 * 2 + 13, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 13, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = None),
    FinceptionUnit(types_dim = 3, rounds_dim = 6, input_channels_dim = 13, master_input_channels_dim = 9 * 3 + 8 * 3 + 7 + 6, scale_factor = 3, bn_training = None),
    CeptionUnit(types_dim = 1, rounds_dim = 1, input_channels_dim = 16 * 3, kernel_types_dim = 1, kernel_rounds_dim = 1, channels_dim = 5, upsampling_types_dim_factor = 1, upsampling_rounds_dim_factor = 1, bn_training = None),
  ]

  unit_stack = UnitStack(units, types_dim = 3, rounds_dim = 6, type_round_channels_dim = 9, type_channels_dim = 8, round_channels_dim = 7, single_channels_dim = 6)
  unit_stack.export_setups_to_json('./exported_model/unit_stack_test/setups.json')
  test_pred = unit_stack.model.predict(test_inputs_batch)
  print('test_pred ch 0', test_pred[0,:,:,0])
  print('test_pred ch 1', test_pred[0,:,:,1])
  print('test_pred ch 2', test_pred[0,:,:,2])
  print('test_pred ch 3', test_pred[0,:,:,3])
  print('test_pred shape', test_pred.shape)
  print('test_pred fl', test_pred.flatten())

  print('test_type_round_input_batch sum', test_type_round_input_batch.sum())
  print('test_type_input_batch sum', test_type_input_batch.sum())
  print('test_round_input_batch sum', test_round_input_batch.sum())
  print('test_single_input_batch sum', test_single_input_batch.sum())


  print('test_pred fl len', len(test_pred.flatten()))
  print('test_pred sum', test_pred.sum())
  print('end')
